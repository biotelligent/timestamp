{ Handles the formatting to / from internal timestamp record to dataset
}
{              StartTime  : TDateTime;
               EndTime    : TDateTime;
               DiffTime   : double;       // Initial calculation is End-Start; thereafter timed.
               WorkTime   : double;       // DiffTime - SlackTime
               SlackTime  : double;       // Timed
               DayCumulativeTime : double;
               OverTime   : double;       // *New* 20071202 -- Worktime >= 8 hrs/day or Sat or Sun
               HourlyRate : double;       // Per record
               Task_Num   : double;       // not used
               Unused2    : double;       // custom field 1
               Unused3    : double;       // custom field 2
               Notes      : string[255];
               EndNotes   : string[255];  // Always the same for every item
               Unused5    : string[255];  // custom field 3
               Unused6    : string[255];  // custom field 4
               pkGUID     : string;       // unique id; runtime only
               ProjectCode: string[50];   // project code within project group

               DateNumber    : Trunc( Double( StartTime )); // Used for sameday groupings
               WeekNumber    : Weeks elapsed since start of project / or start of year
               WorkTimeCost  : Calculated( WorkTime * HourlyRate )
               SlackTimeCost : Calculated( SlackTime * HourlyRate )

  Slacktime is assumed to be incremented via a counter
  DiffTime can be re-calculated as EndTime - StartTime, but is timer based; because you can "continue" retiming a task.
  WorkTime can always be calculated as DiffTime - SlackTime;
  WorkTimeCost can always be calculated (WorkTime * 24) * CurrentRow.HourlyRate;

  Aggregates:
    TotalWorkTimeCost    := SUM( WorkTimeCost )
    TotalSlackTimeCost   := SUM( SlackTime * 24 * HourlyRate );

    TotalWorkTimeElapsed  := SUM( WorkTime );
    TotalSlackTimeElapsed := SUM( SlackTime);
    TotalOverTimeElapsed  := SUM( OverTime);

    GroupDayWorktime      := SUM( WorkTime ) in a given day (ie. cumulative worktime across projects within a day)

    TotalTimeElapsed      := TotalWorkTimeElapsed + TotalSlackTimeElapsed; // Set this value on aggregate gettext;
    AverageHourlyRate
}

{ TODO -oJUH -chandling : Need a "data is changed" event for updating recalcs and notifying for save. }
{ TODO -oJUH -chandling : Need a "view is changed" event for updating aggregates }
//{$IFDEF WS}    webservice saves
//WS.SetEmployees(ADODataSet.DataTable.DataSet.GetChanges);
//ADODataSet.MergeChangeLog; // accept changes;
//{$ELSE}
//SQLCB := sqlCommandBuilder.Create(SQLAdap);
//SQLAdap.UpdateCommand := SQLCB.GetUpdateCommand;
//ADODataSet.ApplyUpdates(SQLAdap)
//{$ENDIF}

unit TMDataModule;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, System.ComponentModel, Borland.Vcl.Db, Borland.Vcl.ADODB,
  Contnrs,
  System.Data,            // DataColumn
//  System.Data.SqlClient,  // sqlXXX
  ADONETDb,                 // TADONETConnector
  TMClientDataTable,
  TMUtils;

type
  TWorktimeAggregate = class
  private
  protected
    FWorktime: Double;
  public
    procedure AddWorktime(Worktime: Double);
    function  WorktimeDelta: String;
  end;

  TWorktimeAggregateList = class(TStringList)
  private
    function GetAggregate(Field: string): TWorktimeAggregate;
  public
    function AddWorkTime(Field: String; Worktime: Double): Integer;
    procedure Clear; override;
    constructor Create;
    destructor Destroy; override;
    function GetWorkTime(Field: string): Double;
    property Aggregate[Field: string]: TWorktimeAggregate read GetAggregate; default;
    property Worktime[Field: string]: Double read GetWorkTime;
  end;

  TTsData = class(TDataModule)
    procedure DataModuleCreate(Sender: TObject);
  private
    FDatasetInitialised: Boolean;
    FInsertAllowed              : Boolean;  // Is this an internally generated insert
    iExceptionCount             : Integer;
    FUpdateCount                : Integer;
    DsGrid: TDataSource;
    FPreviousCursorPosition: Integer;                    // Linked to the grid datasource so we can en/disable from here
    FActiveRecordBuffer: TimeRecord_SA2;
    procedure CreateCDT;
    procedure ConnectToDataset;
    procedure AddToAggregate(var ts2: TimeRecord_SA2);
    procedure ClearAggregates;
    procedure CreateAggregates;
    procedure DoAfterScroll(DataSet: TDataSet);
    procedure DoAfterPost(DataSet: TDataSet);
    procedure DoBeforeDelete(DataSet: TDataSet);
    procedure AdjustAggregate(TS2Add, TS2Subtract: TimeRecord_SA2);
    procedure DeleteFromAggregate(var ts2: TimeRecord_SA2);
  public
    CDT: TClientDataTable;                  // ClientDataTable - we're trying to repro everything the client dataset does
    ADODataSet: TADONETConnector;
    ProjectAggregate: TWorktimeAggregateList;
    DayAggregate: TWorktimeAggregateList;
    DayProjectAggregate: TWorktimeAggregateList;
    TotalWorkTimeAggregate: TWorktimeAggregateList;
    procedure Append(var ts2: TimeRecord_SA2);
    procedure BeginUpdate(StoreCurrentPosition: Boolean = False);
    procedure EndUpdate;
    procedure ClearDataset;
    procedure InitialiseDataset(DataSource: TDataSource);
    function  HasData: Boolean;
    procedure SetIndexName(const Value: String);
    procedure ExportToXml(const FileName: String);
    procedure First;
    procedure Prior;
    procedure Next;
    function  Bof: Boolean;
    function  Eof: Boolean;
    function  IsDisplayedRecord: Boolean;
    procedure ClearFilter;
    procedure RecalculateAggregates;
    procedure LoadXMLFile(FileName: String);
    procedure Save(FileName: String);
    function  GetIsUpdating: Boolean;
    function  GetActiveRecordSA2: TimeRecord_SA2;

    property  ActiveRecordSA2: TimeRecord_SA2 read GetActiveRecordSA2;
    property  DatasetInitialised: Boolean read FDatasetInitialised write FDatasetInitialised;
    property  IndexName      : String write SetIndexName;
    property  UpdateCount: Integer read FUpdateCount;
    property  IsUpdating: Boolean read GetIsUpdating;
    property  InsertAllowed: Boolean read FInsertAllowed write FInsertAllowed;
  end;

var
  TsData: TTsData;

implementation

uses
  DateUtils,
  //
  TMGridFrme, TMMain;

{$R *.nfm}

{ TTsData }

procedure TTsData.Append(var ts2: TimeRecord_SA2);
{ Append a timerecord to the end of the dataset
}
begin
  if not FDatasetInitialised then
    raise Exception.Create('Append - Dataset not initialised');

  try
    InsertAllowed := True;
    AdoDataset.AppendRecord([ ts2.StartTime, ts2.EndTime,
                                ts2.DiffTime, ts2.WorkTime,
                                ts2.SlackTime, ts2.Overtime,
                                ts2.HourlyRate,
                                Trunc( ts2.Task_Num ), Trunc( ts2.Unused2 ),
                                ts2.Unused3,
                                String( ts2.Notes ), String( ts2.EndNotes ),
                                String( ts2.Unused5 ), String( ts2.Unused6 ),
                                String( ts2.pkGUID ),
                                String( ts2.ProjectCode ),
                                Trunc( Double( ts2.StartTime )),
                                (ts2.WorkTime * 24)  * ts2.HourlyRate ] );
     AddToAggregate(ts2);

  except on E:Exception do
    begin
      inc( iExceptionCount );
      if iExceptionCount <= 1 then
        ShowMessage(IntToStr(CDT.Columns.Count) + ' AppendRecord.'+ E.Message);
    end;
  end;
end;

procedure TTsData.BeginUpdate(StoreCurrentPosition: Boolean = False);
{ Disable screen updates and dataset events whenever performing batch operations
}
begin
  inc( FUpdateCount );

  // Store the cursor position if requested
  if (FUpdateCount = 1) then
  begin
    if StoreCurrentPosition then
      FPreviousCursorPosition := AdoDataset.RecNo
    else
      FPreviousCursorPosition := -1;
  end;

  DsGrid.Enabled        := ( FUpdateCount <= 0 );
  CDT.AggregatesActive  := ( FUpdateCount <= 0 );
  if ( FUpdateCount > 0 ) then
  begin
    CDT.BeginLoadData;
    AdoDataset.BeforeDelete := nil;
    AdoDataset.BeforePost   := nil;
    AdoDataset.AfterPost    := nil;
    AdoDataset.AfterScroll  := nil;
  end;
  iExceptionCount := 0;
end;

function TTsData.Bof: Boolean;
begin
  Result := AdoDataset.Bof;
end;

procedure TTsData.ClearDataset;
begin
  FPreviousCursorPosition := -1;
  if DatasetInitialised and (AdoDataset.RecordCount > 0) then
  begin
    AdoDataset.Active := True;
    OutputDebugString( 'TsData.ClearDataset clearing ' + IntToStr(AdoDataset.RecordCount) + ' records');
    CDT.Clear;
    AdoDataset.Close;
    AdoDataset.Open;
    OutputDebugString( 'TsData.ClearDataset reset to ' + IntToStr(AdoDataset.Fields.Count) + ' fields');
    OutputDebugString( 'TsData.ClearDataset reset to ' + IntToStr(AdoDataset.RecordCount) + ' records');

    ClearAggregates;
  end;
end;

procedure TTsData.ClearFilter;
begin
  AdoDataset.Filtered := False;
end;

procedure TTsData.ConnectToDataset;
begin
  OutputDebugString( 'Sequence 2: TTsData.ConnectToDataset' );
  ADODataSet.Active := False;
  ADODataSet.DataTable := CDT;
  ADODataSet.Active := True;
  AdoDataset.LogChanges := False;
  AdoDataset.OnNewRecord  := TSMainForm.TSGrid.TSClientDatasetNewRecord;
end;

procedure TTsData.CreateCDT;
begin
  CDT := TClientDataTable.Create;
  CDT.Active := False;
  FDatasetInitialised := False;
end;

procedure TTsData.DataModuleCreate(Sender: TObject);
begin
  OutputDebugString('Initialising datamodule: TTsData.DataModuleCreate');
  CreateCDT;
  CreateAggregates;
  ADODataSet := TADONETConnector.Create(self);
end;

procedure TTsData.DoBeforeDelete(DataSet: TDataSet);
begin
  if FActiveRecordBuffer.IsValid then
    DeleteFromAggregate(FActiveRecordBuffer);

  TSMainForm.TSGrid.TSClientDatasetBeforeDelete(DataSet);
end;

procedure TTsData.DoAfterPost(DataSet: TDataSet);
var
  TS2: TimeRecord_SA2;
begin
  if FActiveRecordBuffer.IsValid then
  begin
    TS2 := ActiveRecordSA2;
    if TS2.IsValid then
    begin
      if (FActiveRecordBuffer.pkGUID = TS2.pkGUID) then
      begin
        // Edited, subtract old then add new values, because day or project and thus indexes may have changed
        DeleteFromAggregate(FActiveRecordBuffer);
        AddToAggregate(TS2);
      end
      else
        AddToAggregate(TS2);
    end;
  end;
  TSMainForm.TSGrid.TSClientDatasetAfterPost(DataSet);
end;

procedure TTsData.DoAfterScroll(DataSet: TDataSet);
begin
  FActiveRecordBuffer := GetActiveRecordSA2;
  TSMainForm.TSGrid.TSClientDatasetAfterScroll(DataSet);
end;

procedure TTsData.EndUpdate;
{ Re-enable screen updates and dataset events after batch operations
}
begin
  if ( FUpdateCount > 0 ) then
    dec( FUpdateCount );

  if (FUpdateCount = 0) then
  begin
    // Restore cursor if we'd set it during BeginUpdate
    if (FPreviousCursorPosition > -1) and (FPreviousCursorPosition < AdoDataset.RecordCount) then
      AdoDataset.RecNo := FPreviousCursorPosition;

    CDT.EndLoadData;
    AdoDataset.OnNewRecord  := TSMainForm.TSGrid.TSClientDatasetNewRecord;
    AdoDataset.BeforePost   := TSMainForm.TSGrid.TSClientDatasetBeforePost;
    AdoDataset.AfterPost    := DoAfterPost;
    AdoDataset.AfterScroll  := DoAfterScroll;
    AdoDataset.BeforeDelete := DoBeforeDelete;
    iExceptionCount         := 0;
  end;

  CDT.AggregatesActive := ( FUpdateCount = 0 );
  DsGrid.Enabled       := ( FUpdateCount = 0 );
end;

function TTsData.Eof: Boolean;
begin
  Result := AdoDataset.Eof;
end;

procedure TTsData.ExportToXml(const FileName: String);
begin
  CDT.AcceptChanges;
  CDT.SaveToFile(FileName);//, System.Data.XMLWriteMode.WriteSchema);
//  AdoDataset.SaveToFile(FileName, System.Data.XMLWriteMode.WriteSchema);
end;

procedure TTsData.First;
{ AdoDataset.First does not reliably work
}
begin
  if HasData then
    AdoDataset.RecNo := 0;
  //DebugMsg('First recno=%d', [AdoDataset.RecNo]);
end;

function TTsData.GetActiveRecordSA2: TimeRecord_SA2;
{ Return the current TSClientDataset record as a TimeRecord_SA2 structure
}
var
  EmptyRecord: TimeRecord_SA2;
begin
  if HasData then
  begin
    //if (FActiveRecordBuffer.pkGUID = AdoDataset.FieldByName( 'pkGUID' ).AsString) then
    //  Result := Oops, can't do, the field values may be changing
    Result.StartTime  := AdoDataset.FieldByName( 'StartTime' ).AsDateTime;
    Result.EndTime    := AdoDataset.FieldByName( 'EndTime' ).AsDateTime;
    Result.DiffTime   := AdoDataset.FieldByName( 'DiffTime' ).AsFloat;
    Result.WorkTime   := AdoDataset.FieldByName( 'WorkTime' ).AsFloat;
    Result.SlackTime  := AdoDataset.FieldByName( 'SlackTime' ).AsFloat;
    Result.OverTime   := AdoDataset.FieldByName( 'OverTime' ).AsFloat;

    Result.HourlyRate := AdoDataset.FieldByName( 'HourlyRate' ).AsFloat;
    Result.Task_Num   := AdoDataset.FieldByName( 'Task_Num' ).AsFloat;
    Result.Unused2    := AdoDataset.FieldByName( 'Unused2' ).AsFloat;
    Result.Unused3    := AdoDataset.FieldByName( 'Unused3' ).AsFloat;

    Result.Notes      := AdoDataset.FieldByName( 'Notes' ).AsString;
    Result.EndNotes   := AdoDataset.FieldByName( 'EndNotes' ).AsString;
    Result.Unused5    := AdoDataset.FieldByName( 'Unused5' ).AsString;
    Result.Unused6    := AdoDataset.FieldByName( 'Unused6' ).AsString;
    Result.pkGUID     := AdoDataset.FieldByName( 'pkGUID' ).AsString;
    Result.ProjectCode:= AdoDataset.FieldByName( 'ProjectCode' ).AsString;
  end
  else
    Result := EmptyRecord;

  // Update the quick lookup record for the timing row
  //if IsTimingRow then
  //  FTimingTask := Result;
end;

function TTsData.GetIsUpdating: Boolean;
begin
  Result := FUpdateCount > 0;
end;

function TTsData.HasData: Boolean;
begin
  Result := DatasetInitialised and AdoDataset.Active and (AdoDataset.RecordCount > 0);
end;

procedure TTsData.InitialiseDataset(Datasource: TDataSource);
{ Create the clientdataset structure definitions.
  Fields, Aggregates, and Indices...
}
begin
  if DatasetInitialised then
    Exit;

  OutputDebugString( 'Sequence 1: TTsData.InitialiseDataset metadata' );
  FPreviousCursorPosition := -1;

  if not CDT.SchemaCreated then
    CDT.CreateSchema;

  FDatasetInitialised    := True;
  CDT.Active := True;
  ConnectToDataset;       // *Must* happen after CDT metadata initialised

  DsGrid := DataSource;
  DsGrid.DataSet := AdoDataset;
end;

function TTsData.IsDisplayedRecord: Boolean;
// Only display/calculate the record if in the last 6 months
begin
  Result := HasData and (DaysBetween(now, AdoDataset.FieldByName( 'StartTime' ).AsDateTime) < 180);
end;

procedure TTsData.Next;
begin
  AdoDataset.Next;
end;

procedure TTsData.Prior;
begin
  AdoDataset.Prior;
end;

procedure TTsData.RecalculateAggregates;
var
  ts2: TimeRecord_SA2;
begin
  ClearAggregates;
  if not HasData then
    Exit;

  BeginUpdate(True);
  try
    First;
    while not Eof do
    begin
      if IsDisplayedRecord then
      begin
        ts2 := ActiveRecordSA2;
        AddToAggregate(ts2);
      end;
      Next;
    end;
  finally
    EndUpdate;
  end;
end;

//procedure TTsData.SetActiveRow(const Value: Integer);
//{ Set the selected row to the record number passed
//}
//begin
//  if AdoDataset.Active then
//    AdoDataset.RecNo := Value;
//end;
//

procedure TTsData.LoadXMLFile(FileName: String);
begin
  BeginUpdate(False);
  try
    if DatasetInitialised then
    begin
      ClearDataSet;
      FDatasetInitialised := False; // *AFTER* ClearDataset
    end;

    if FileExists(FileName) then
    begin
      //  if the XML file was saved w/o schema, then
      //  if not CDT.SchemaCreated then CDT.CreateSchema;
      CDT.Active := False;
      CDT.LoadFromFile(FileName);
      FDatasetInitialised := True;
      ConnectToDataset;       // *Must* happen after CDT metadata initialised
      RecalculateAggregates;
      TSMainForm.TSGrid.dsCdt.Dataset := AdoDataset;
    end;
  finally
    EndUpdate;
  end;
end;

procedure TTsData.Save(FileName: String);
begin
  CDT.AcceptChanges;
  CDT.SaveToFile(FileName);
end;

procedure TTsData.SetIndexName(const Value: String);
begin
  AdoDataset.IndexFieldNames := Value;
end;

{ TProjectAggregateList }

function TWorktimeAggregateList.AddWorkTime(Field : String; Worktime: Double): Integer;
begin
  if not Find(Field, Result) then
    Result := AddObject(Field, TWorktimeAggregate.Create);

  Aggregate[Field].AddWorktime(Worktime);
end;

procedure TWorktimeAggregateList.Clear;
var
  i: Integer;
begin
  for i := Count - 1 downto 0 do
    Objects[i].Free;
  inherited;
end;

constructor TWorktimeAggregateList.Create;
begin
  inherited Create;
  Duplicates := dupError;
  Sorted := True;
end;

destructor TWorktimeAggregateList.Destroy;
begin
  Clear;
  inherited;
end;

function TWorktimeAggregateList.GetAggregate(Field: string): TWorktimeAggregate;
var
  Index: Integer;
begin
  if not Find(Field, Index) then
    Index := AddWorktime(Field, 0);

  Result := TWorktimeAggregate(Objects[Index]);
end;

function TWorktimeAggregateList.GetWorkTime(Field: string): Double;
begin
  if Assigned(Aggregate[Field]) then
    Result := Aggregate[Field].FWorktime
  else
    Result := 0;
end;

{ TProjectAggregate }

procedure TWorktimeAggregate.AddWorktime(Worktime: Double);
begin
  FWorktime := FWorktime + Worktime;
end;

function TWorktimeAggregate.WorktimeDelta: String;
begin
  Result := TMUtils.FormatSummaryTime(FWorktime, False);
end;

procedure TTsData.CreateAggregates;
begin
  ProjectAggregate := TWorktimeAggregateList.Create;
  DayAggregate := TWorktimeAggregateList.Create;
  DayProjectAggregate := TWorktimeAggregateList.Create;
  TotalWorkTimeAggregate := TWorktimeAggregateList.Create;
end;

procedure TTsData.ClearAggregates;
begin
  ProjectAggregate.Clear;
  DayAggregate.Clear;
  DayProjectAggregate.Clear;
  TotalWorkTimeAggregate.Clear;
end;

procedure TTsData.AddToAggregate(var ts2: TimeRecord_SA2);
var
  ZeroRecord: TimeRecord_SA2;
begin
  AdjustAggregate(TS2, ZeroRecord);
end;

procedure TTsData.DeleteFromAggregate(var ts2: TimeRecord_SA2);
var
  ZeroRecord: TimeRecord_SA2;
begin
  AdjustAggregate(ZeroRecord, TS2);
end;

procedure TTsData.AdjustAggregate(TS2Add, TS2Subtract: TimeRecord_SA2);
begin
  ProjectAggregate.AddWorkTime(Ts2Add.ProjectCode, Ts2Add.WorkTime - TS2Subtract.WorkTime);
  DayAggregate.AddWorkTime(Ts2Add.DayNumberStr, Ts2Add.WorkTime - TS2Subtract.WorkTime);
  DayProjectAggregate.AddWorkTime(Ts2Add.DayProject, Ts2Add.WorkTime - TS2Subtract.WorkTime);
  TotalWorkTimeAggregate.AddWorkTime('', Ts2Add.WorkTime - TS2Subtract.WorkTime);
end;

end.
