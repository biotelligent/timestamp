{
Application:    Time Stamp, Copyright 1997 William Rouck
Module:         TMPref.pas
Contact:        email: wrouck@syntap.com
Modifications:  JM Holloway, June 2004
                justin@schiznit.net

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version. This
program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details. You should have
received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
02111-1307, USA.
}

unit TMPref;

{ This unit contains the forms for user preferences, called from the File
  menu.  It reads and writes preferences to an INI file.  }

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, INIFiles, ComCtrls, FileCtrl, Buttons, Menus,
  System.ComponentModel, Borland.Vcl.Spin, Borland.Vcl.CheckLst, TMGroup;

type
  TPreferencesForm = class(TForm)
    OKButton: TButton;
    CancelButton: TButton;
    PageControl1: TPageControl;
    GUIOptionsTabSheet: TTabSheet;
    StartReducedCheckBox: TCheckBox;
    DecimalSummaryCheckBox: TCheckBox;
    ShowTrayIconCheckBox: TCheckBox;
    StartTimerOnStartupCheckBox: TCheckBox;
    FileOptionsTabSheet: TTabSheet;
    OpenLastSavedCheckBox: TCheckBox;
    AutoSaveCheckBox: TCheckBox;
    AutoSaveMinutesEdit: TEdit;
    AutoSaveTimeUpDown: TUpDown;
    Label2: TLabel;
    DefaultDirectoryCheckBox: TCheckBox;
    StartScreenSaverWithSlackCheckBox: TCheckBox;
    TrayIconOptionsRadioGroup: TRadioGroup;
    PrintOptionsTabSheet: TTabSheet;
    ResetFontOptionsBitBtn: TBitBtn;
    GroupBox2: TGroupBox;
    PrintSlackInformationCheckBox: TCheckBox;
    PrintFileNameCheckBox: TCheckBox;
    PrintRateInformationCheckBox: TCheckBox;
    ShowTimerInTrayCaptionCheckBox: TCheckBox;
    PrintEndNotesCheckBox: TCheckBox;
    PrintProjectTitleCheckBox: TCheckBox;
    GridOptionsTabSheet: TTabSheet;
    GridColumnGroupBox: TGroupBox;
    HideTaskButtonCheckBox: TCheckBox;
    FlashColorsWhileTimingCheckBox: TCheckBox;
    PrintPageNumbersCheckBox: TCheckBox;
    Print24HoursCheckBox: TCheckBox;
    ShowCalendarCheckBox: TCheckBox;
    DisableTimerKeyShortcutsCheckBox: TCheckBox;
    DirectoryGroupBox: TGroupBox;
    DirectoryEdit: TEdit;
    DirectoryButton: TButton;
    GroupBox1: TGroupBox;
    Label4: TLabel;
    Label6: TLabel;
    DetailLineFontSampleEdit: TEdit;
    ColumnHeaderFontSampleEdit: TEdit;
    SummaryFontComboBox: TComboBox;
    SummarySizeSpinEdit: TSpinEdit;
    DetailFontComboBox: TComboBox;
    DetailSizeSpinEdit: TSpinEdit;
    ColumnHeadersFontDialog: TFontDialog;
    DetailLinesFontDialog: TFontDialog;
    DetailFontButton: TSpeedButton;
    HeaderFontButton: TSpeedButton;
    GridColumnCheckList: TCheckListBox;
    CustomFieldList: TListBox;
    ChangeFieldNameButton: TButton;
    BugzillaGroupBox: TGroupBox;
    BugzillaURLEdit: TEdit;
    GridColumnDownButton: TSpeedButton;
    GridColumnUpButton: TSpeedButton;
    CustomFieldsGroupBox: TGroupBox;
    ChangeDropdownListButton: TButton;
    ProjectOptionsTabSheet: TTabSheet;
    GroupBox3: TGroupBox;
    ProjectTitleEdit: TEdit;
    Label1: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    EndNotesEdit: TEdit;
    HourlyRateEdit: TEdit;
    ResetProjectOptionsBitBtn: TBitBtn;
    GroupOptionsPanel: TPanel;
    PrintCustomFieldsCheckBox: TCheckBox;
    procedure OKButtonClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ShowTrayIconCheckBoxClick(Sender: TObject);
    procedure TrayIconOptionsRadioGroupClick(Sender: TObject);
    procedure ChangeColumnHeaderFontBitBtnClick(Sender: TObject);
    procedure ChangeDetailLineFontBitBtnClick(Sender: TObject);
    procedure HeaderRxFontComboBoxChange(Sender: TObject);
    procedure HeaderFontSizeRxSpinEditChange(Sender: TObject);
    procedure DetailFontSizeRxSpinEditChange(Sender: TObject);
    procedure DetailRxFontComboBoxChange(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ResetProjectFontOptionsBitBtnClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure HideTaskButtonCheckBoxClick(Sender: TObject);
    procedure FlashColorsWhileTimingCheckBoxClick(Sender: TObject);
    procedure DisableTimerKeyShortcutsCheckBoxClick(Sender: TObject);
    procedure DefaultDirectoryCheckBoxClick(Sender: TObject);
    procedure DirectoryButtonClick(Sender: TObject);
    procedure ChangeFieldNameButtonClick(Sender: TObject);
    procedure GridColumnUpButtonClick(Sender: TObject);
    procedure GridColumnDownButtonClick(Sender: TObject);
    procedure ChangeDropdownListButtonClick(Sender: TObject);
    procedure GridColumnCheckListClickCheck(Sender: TObject);
    procedure ResetProjectOptionsBitBtnClick(Sender: TObject);
    procedure ShowCalendarCheckBoxClick(Sender: TObject);
  private
    GroupOptionsFrame   : TGroupOptionsFrame;
    ColumnLayoutChanged : Boolean; { Set true to reload columns on exit }
    procedure LoadFontNames;
    procedure ShowColumnHeaderFont(Name: String=''; Size: Integer=0; Style: TFontStyles=[]);
    procedure ShowDetailLineFont(Name: String=''; Size: Integer=0; Style: TFontStyles=[]);
    procedure LoadGridColumns;
    procedure SaveGridColumns;
    procedure LoadCustomFields;
    procedure SaveCustomFields;
    procedure LoadTSGOptions;
    procedure SaveTSGOptions;
    procedure SetDefaultTSGOptions;
    procedure LoadReportComboBox;
  end;

var
  PreferencesForm : TPreferencesForm;
  INISettingsFile : TIniFile;

implementation

uses TMUtils, TMMain, TMDropListBld, Nevrona.Rave.RvProj, Nevrona.Rave.RpRave;

{$R *.NFM}

const
   WIDTH_FIELDNAME  = 20;


//*****************************************************************************

procedure TPreferencesForm.OKButtonClick(Sender: TObject);
{This procedure adjusts the INI File to reflect user preferences.}
begin
     if OpenLastSavedCheckBox.Checked then
        begin
             INISettingsFile.WriteString('Options','AutoLoad','1');
        end
     else
        begin
             INISettingsFile.WriteString('Options','AutoLoad','0');
        end; {if OpenLastSavedCheckBox.Checked}

     if StartReducedCheckBox.Checked then
        begin
             INISettingsFile.WriteString('Options','StartReduced','1');
        end
     else
        begin
             INISettingsFile.WriteString('Options','StartReduced','0');
        end; {if StartReducedCheckBox.Checked}

     if DecimalSummaryCheckBox.Checked then
        begin
             INISettingsFile.WriteString('Options','DecimalSummary','1');
        end
     else
        begin
             INISettingsFile.WriteString('Options','DecimalSummary','0');
        end; {if DecimalSummaryCheckBox.Checked}

     if HideTaskButtonCheckBox.Checked then
        begin
             INISettingsFile.WriteString('Options','HideTaskButton','1');
        end
     else
        begin
             INISettingsFile.WriteString('Options','HideTaskButton','0');
        end; {if DontHideTaskButtonCheckBox.Checked}

     if ShowTrayIconCheckBox.Checked then
        begin
             INISettingsFile.WriteString('Options','ShowTrayIcon','1');
//tray       TSMainForm.ApplicationRxTrayIcon.Active := TRUE;

             if TMMain.HideTaskButton = TRUE then
                begin
                     showwindow(application.handle, sw_hide);
                end
             else
                begin
                     showwindow(application.handle, sw_showdefault);
                end;

             TMMain.ShowTrayIcon := '1';

             {Write out preference for tray icon click action}
             if TrayIconOptionsRadioGroup.ItemIndex = 0 then
                begin
                     INISettingsFile.WriteString('Options','TrayIconClickRestores','1');
                end
             else
                begin
                     INISettingsFile.WriteString('Options','TrayIconClickRestores','0');
                end;
        end
     else
        begin
             INISettingsFile.WriteString('Options','ShowTrayIcon','0');
//tray       TSMainForm.ApplicationRxTrayIcon.Active := FALSE;
             showwindow(application.handle, sw_showdefault);
             TMMain.ShowTrayIcon := '0';
        end; {if ShowTrayIconCheckBox.Checked}

     if AutoSaveCheckBox.Checked then
        begin
             INISettingsFile.WriteString('Options','AutoSave','1');
             INISettingsFile.WriteString('Options','AutoSaveTime', AutoSaveMinutesEdit.Text);
             TSMainForm.AutoSaveTimer.Interval := StrToInt(AutoSaveMinutesEdit.Text) * 60000;
             TSMainForm.AutoSaveTimer.Enabled  := TRUE;
        end
     else
        begin
             INISettingsFile.WriteString('Options','AutoSave','0');
             INISettingsFile.WriteString('Options','AutoSaveTime','0');
             TSMainForm.AutoSaveTimer.Interval := 0;
             TSMainForm.AutoSaveTimer.Enabled  := FALSE;
        end; {if AutoSaveCheckBox.Checked}

     if StartTimerOnStartupCheckBox.Checked then
        begin
             INISettingsFile.WriteString('Options','StartTimerOnStartup','1');
        end
     else
        begin
             INISettingsFile.WriteString('Options','StartTimerOnStartup','0');
        end; {if StartTimerOnStartupCheckBox.Checked}

     INISettingsFile.WriteBool('Options', 'UseDefaultDirectory', DefaultDirectoryCheckBox.Checked);
     if DefaultDirectoryCheckBox.Checked then
       begin
         INISettingsFile.WriteString( 'Options', 'DefaultDirectory', DirectoryEdit.Text );
         TSMainForm.OpenFileDialog.InitialDir := DirectoryEdit.Text;
         TSMainForm.SaveFileDialog.InitialDir := DirectoryEdit.Text;
       end; {if DefaultDirectoryCheckBox.Checked}

     if StartScreenSaverWithSlackCheckBox.Checked then
        begin
             INISettingsFile.WriteString('Options','StartScreenSaverWithSlack','1');
             TMMain.StartScreenSaverWhenSlackStarts := TRUE;
        end
     else
        begin
             INISettingsFile.WriteString('Options','StartScreenSaverWithSlack','0');
             TMMain.StartScreenSaverWhenSlackStarts := FALSE;
        end; {if StartScreenSaverWithSlackCheckBox.Checked}

     if ShowTimerInTrayCaptionCheckBox.Checked then
        begin
             INISettingsFile.WriteString('Options','ShowTimerInTrayCaption','1');
             TMMain.ShowTimerInTrayCaption := TRUE;
        end
     else
        begin
             INISettingsFile.WriteString('Options','ShowTimerInTrayCaption','0');
             TMMain.ShowTimerInTrayCaption := FALSE;
        end; {if ShowTimerInTrayCaptionCheckBox.Checked}

     if DisableTimerKeyShortcutsCheckBox.Checked then
        begin
             INISettingsFile.WriteString('Options','DisableTimerKeyShortcuts','1');
        end
     else
        begin
             INISettingsFile.WriteString('Options','DisableTimerKeyShortcuts','0');
        end; {if DisableTimerKeyShortcutsCheckBox.Checked}

     {Set report font preferences}
     INISettingsFile.WriteString('Options','HeadingsReportFontSize',IntToStr(SummarySizeSpinEdit.Value));
     INISettingsFile.WriteString('Options','HeadingsReportFontName',SummaryFontComboBox.Items[ SummaryFontComboBox.ItemIndex ]);

     INISettingsFile.WriteString('Options','DetailLineReportFontSize',IntToStr(DetailSizeSpinEdit.Value));
     INISettingsFile.WriteString('Options','DetailLineReportFontName',DetailFontComboBox.Items[DetailFontComboBox.ItemIndex] );

     {Set other report preferences}
     if PrintSlackInformationCheckBox.Checked = FALSE then
        begin
             INISettingsFile.WriteString('Options','PrintSlackInformation','0');
        end
     else
        begin
             INISettingsFile.WriteString('Options','PrintSlackInformation','1');
        end; {if PrintSlackInformationCheckBox.Checked}

     if PrintFileNameCheckBox.Checked = FALSE then
        begin
             INISettingsFile.WriteString('Options','PrintFileName','0');
        end
     else
        begin
             INISettingsFile.WriteString('Options','PrintFileName','1');
        end; {if PrintFileNameCheckBox.Checked = FALSE}

     if PrintRateInformationCheckBox.Checked = FALSE then
        begin
             INISettingsFile.WriteString('Options','PrintRateInformation','0');
        end
     else
        begin
             INISettingsFile.WriteString('Options','PrintRateInformation','1');
        end; {if PrintRateInformationCheckBox.Checked}

     if PrintEndNotesCheckBox.Checked = FALSE then
        begin
             INISettingsFile.WriteString('Options','PrintEndNotes','0');
        end
     else
        begin
             INISettingsFile.WriteString('Options','PrintEndNotes','1');
        end; {if PrintEndNotesCheckBox.Checked}

     if PrintProjectTitleCheckBox.Checked = FALSE then
        begin
             INISettingsFile.WriteString('Options','PrintProjectTitle','0');
        end
     else
        begin
             INISettingsFile.WriteString('Options','PrintProjectTitle','1');
        end; {if PrintEndNotesCheckBox.Checked}

     if PrintPageNumbersCheckBox.Checked = FALSE then
        begin
             INISettingsFile.WriteString('Options','PrintPageNumbers','0');
        end
     else
        begin
             INISettingsFile.WriteString('Options','PrintPageNumbers','1');
        end; {if PrintPageNumbersCheckBox.Checked}

     INISettingsFile.WriteBool('Options','PrintCustomFields',PrintCustomFieldsCheckBox.Checked);

     if Print24HoursCheckBox.Checked = FALSE then
        begin
             INISettingsFile.WriteString('Options','Print24Hours','0');
        end
     else
        begin
             INISettingsFile.WriteString('Options','Print24Hours','1');
        end; {if Print24HoursCheckBox.Checked}

     if FlashColorsWhileTimingCheckBox.Checked = FALSE then
        begin
             INISettingsFile.WriteString('Options','FlashColorsWhileTiming','0');
        end
     else
        begin
             INISettingsFile.WriteString('Options','FlashColorsWhileTiming','1');
        end; {if FlashColorsWhileTimingCheckBox.Checked}

  INISettingsFile.WriteBool( 'Options', 'ShowCalendar', ShowCalendarCheckBox.Checked );
  SaveGridColumns;
  SaveCustomFields;
  SaveTSGOptions;
  if ColumnLayoutChanged then
    TSMainForm.TSGrid.LoadColumnSettings;
end; {procedure TPreferencesForm.OKButtonClick}

//*****************************************************************************

procedure TPreferencesForm.LoadGridColumns;
{ Load the custom column layout and selection
}
var
  i: Integer;
  TmpList: TStringList;
begin
  TmpList := TStringList.Create;
  try
    INISettingsFile.ReadSectionValues( SEC_GRIDCOLUMNS, TmpList );

    // If there are items missing from the .ini file then don't load
    // - use the design-time defaults
    if ( TmpList.Count < GridColumnCheckList.Count ) then
      Exit
    else
      GridColumnCheckList.Clear;
    for i := 0 to TmpList.Count - 1 do
    begin
      GridColumnCheckList.Items.Insert( 0, copy( TmpList[i], pos('=', TmpList[i])+2, Length( TmpList[i])));
      GridColumnCheckList.Checked[0] := ( copy( TmpList[i], pos('=', TmpList[i])+1, 1 ) = '1' );
    end;
  finally
    TmpList.Free;
  end;
end;

procedure TPreferencesForm.SaveGridColumns;
{ Save the custom column layout and selection
}
var
  i           : Integer;
  SelectedStr : String;
begin
  INISettingsFile.EraseSection( SEC_GRIDCOLUMNS );
  for i := GridColumnCheckList.Count - 1 downto 0 do
  begin
    if GridColumnCheckList.Checked[i] then
      SelectedStr := '1'
    else
      SelectedStr := '0';
    INISettingsFile.WriteString( SEC_GRIDCOLUMNS, IntToStr( i ), SelectedStr + GridColumnCheckList.Items[i] );
  end;
end;

procedure TPreferencesForm.LoadFontNames;
{ Populate the font comboboxes.
  This is almost certainly cheating, since the screen fonts don't always map
  to printer fonts... but I'm not sure how to enumfonts() in .Net
}
begin
  if (SummaryFontComboBox.Items.Count > 0) then
    Exit;
  SummaryFontComboBox.Items.Assign( Screen.Fonts );
  DetailFontComboBox.Items.Assign( Screen.Fonts );

  SummaryFontComboBox.Sorted := True;
  DetailFontComboBox.Sorted  := True;
end;

procedure TPreferencesForm.ShowColumnHeaderFont( Name: String=''; Size: Integer=0; Style: TFontStyles = [] );
{ Update header column font display
}
begin
  if ( Name <> '' ) then
    SummaryFontComboBox.ItemIndex := SummaryFontComboBox.Items.IndexOf( Name );

  if ( Size <> 0 ) then
    SummarySizeSpinEdit.Value := Size;

  ColumnHeaderFontSampleEdit.Font.Size := SummarySizeSpinEdit.Value;
  ColumnHeaderFontSampleEdit.Font.Name := SummaryFontComboBox.Text;

  if ( Style <> [] ) then
    ColumnHeaderFontSampleEdit.Font.Style := Style;
end;

procedure TPreferencesForm.ShowDetailLineFont( Name: String=''; Size: Integer=0; Style: TFontStyles = [] );
{ Update detail line font display
}
begin
  if ( Name <> '' ) then
    DetailFontComboBox.ItemIndex := DetailFontComboBox.Items.IndexOf( Name );

  if ( Size <> 0 ) then
    DetailSizeSpinEdit.Value := Size;

  DetailLineFontSampleEdit.Font.Size := DetailSizeSpinEdit.Value;
  DetailLineFontSampleEdit.Font.Name := DetailFontComboBox.Text;

  if ( Style <> [] ) then
    DetailLineFontSampleEdit.Font.Style := Style;
end;

procedure TPreferencesForm.FormCreate(Sender: TObject);
{ This procedure initializes settings when the form is created. }
begin
  GroupOptionsFrame        := TGroupOptionsFrame.Create( Self );
  GroupOptionsFrame.Parent := GroupOptionsPanel;
  GroupOptionsFrame.Align  := alClient;
  ColumnLayoutChanged := True;

  {Set default check box behavior}
  OpenLastSavedCheckBox.Checked := FALSE;

  {Get INI File settings}
  INISettingsFile := TINIFile.Create(TMUtils.GetINIFilePath);

  if INISettingsFile.ReadString('Options','AutoLoad','Error') = '1' then
     begin
          OpenLastSavedCheckBox.Checked := TRUE;
     end; {if INISettingsFile.ReadString('Options','AutoLoad','Error')}

  if INISettingsFile.ReadString('Options','StartReduced','Error') = '1' then
     begin
          StartReducedCheckBox.Checked := TRUE;
     end; {if INISettingsFile.ReadString('Options','StartReduced','Error')}

  if INISettingsFile.ReadString('Options','DecimalSummary','Error') = '1' then
     begin
          DecimalSummaryCheckBox.Checked := TRUE;
     end; {INISettingsFile.ReadString('Options','DecimalSummary','Error')}

  if INISettingsFile.ReadString('Options','ShowTrayIcon','Error') = '1' then
     begin
          ShowTrayIconCheckBox.Checked      := TRUE;
          TrayIconOptionsRadioGroup.Enabled := TRUE;
          HideTaskButtonCheckBox.Enabled    := TRUE;

          {Get Tray Icon click action preference}
          if INISettingsFile.ReadString('Options','TrayIconClickRestores','Error') = '1' then
             begin
                  TrayIconOptionsRadioGroup.ItemIndex := 0;
                  TMMain.TrayIconClickRestores := TRUE;
             end
          else
             begin
                  TrayIconOptionsRadioGroup.ItemIndex := 1;
                  TMMain.TrayIconClickRestores        := FALSE;
             end;
     end
  else
     begin
          TrayIconOptionsRadioGroup.Enabled := FALSE;
          HideTaskButtonCheckBox.Enabled    := FALSE;
     end; {if INISettingsFile.ReadString('Options','ShowTrayIcon','Error')}

  if INISettingsFile.ReadString('Options','ShowTimerInTrayCaption','1') = '1' then
     begin
          ShowTimerInTrayCaptionCheckBox.Checked := TRUE;
     end; {if INISettingsFile.ReadString('Options','ShowTimerInTrayCaption','Error')}

  if INISettingsFile.ReadString('Options','AutoSave','Error') = '1' then
     begin
          AutoSaveCheckBox.Checked := TRUE;
          try
             AutoSaveTimeUpDown.Position := StrToInt(INISettingsFile.ReadString('Options','AutoSaveTime','Error'));
          except
                on EConvertError do
                   begin
                        AutoSaveTimeUpDown.Position := 5;
                   end;
          end; {try}
     end; {if INISettingsFile.ReadString('Options','AutoSave','Error')}

  if INISettingsFile.ReadString('Options','StartTimerOnStartup','Error') = '1' then
     begin
          StartTimerOnStartupCheckBox.Checked := TRUE;
     end; {if INISettingsFile.ReadString('Options','StartTimerOnStartup','Error')}

  {Read default directory setting and apply to boxes}
  DirectoryEdit.Text := INISettingsFile.ReadString( 'Options', 'DefaultDirectory',
                                                    Environment.GetFolderPath( Environment.SpecialFolder.Personal ));
  DefaultDirectoryCheckBox.Checked := INISettingsFile.ReadBool('Options','UseDefaultDirectory', False);
  DirectoryGroupBox.Enabled        := INISettingsFile.ReadBool('Options','UseDefaultDirectory', False);

  if INISettingsFile.ReadString('Options','StartScreenSaverWithSlack','Error') = '1' then
     begin
          StartScreenSaverWithSlackCheckBox.Checked := TRUE;
     end; {if INISettingsFile.ReadString('Options','StartScreenSaverWithSlack','Error')}

  {Set report font preferences}
  LoadFontNames;
  ShowColumnHeaderFont( INISettingsFile.ReadString ('Options','HeadingsReportFontName', DEFAULT_FONTNAME),
                        INISettingsFile.ReadInteger('Options','HeadingsReportFontSize', SUMMARY_FONTSIZE),
                        [fsBold,fsUnderline] );

  ShowDetailLineFont(   INISettingsFile.ReadString ('Options','DetailLineReportFontName',DEFAULT_FONTNAME),
                        INISettingsFile.ReadInteger('Options','DetailLineReportFontSize',DETAIL_FONTSIZE),
                        [] );

  {set other report preferences}
  if INISettingsFile.ReadString('Options','PrintSlackInformation','Error') = '0' then
     begin
          PrintSlackInformationCheckBox.Checked := FALSE;
     end
  else
     begin
          PrintSlackInformationCheckBox.Checked := TRUE;
     end; {if INISettingsFile.ReadString('Options','PrintSlackInformation','Error')}

  if INISettingsFile.ReadString('Options','PrintFileName','Error') = '0' then
     begin
          PrintFileNameCheckBox.Checked := FALSE;
     end
  else
     begin
          PrintFileNameCheckBox.Checked := TRUE;
     end; {if INISettingsFile.ReadString('Options','PrintFileName','Error')}

  if INISettingsFile.ReadString('Options','PrintRateInformation','Error') = '0' then
     begin
          PrintRateInformationCheckBox.Checked := FALSE;
     end
  else
     begin
          PrintRateInformationCheckBox.Checked := TRUE;
     end; {if INISettingsFile.ReadString('Options','PrintRateInformation','Error')}

  if INISettingsFile.ReadString('Options','PrintEndNotes','Error') = '0' then
     begin
          PrintEndNotesCheckBox.Checked := FALSE;
     end
  else
     begin
          PrintEndNotesCheckBox.Checked := TRUE;
     end; {if INISettingsFile.ReadString('Options','PrintRateInformation','Error')}

  if INISettingsFile.ReadString('Options','PrintProjectTitle','Error') = '0' then
     begin
          PrintProjectTitleCheckBox.Checked := FALSE;
     end
  else
     begin
          PrintProjectTitleCheckBox.Checked := TRUE;
     end; {if INISettingsFile.ReadString('Options','PrintProjectTitle','Error')}

  if INISettingsFile.ReadString('Options','PrintProjectTitle','Error') = '0' then
     begin
          PrintProjectTitleCheckBox.Checked := FALSE;
     end
  else
     begin
          PrintProjectTitleCheckBox.Checked := TRUE;
     end; {if INISettingsFile.ReadString('Options','PrintProjectTitle','Error')}

  if INISettingsFile.ReadString('Options','PrintPageNumbers','Error') = '0' then
     begin
          PrintPageNumbersCheckBox.Checked := FALSE;
     end
  else
     begin
          PrintPageNumbersCheckBox.Checked := TRUE;
     end; {if INISettingsFile.ReadString('Options','PrintPageNumbers','Error')}
  PrintCustomFieldsCheckBox.Checked := INISettingsFile.ReadBool('Options','PrintCustomFields',False);
  if INISettingsFile.ReadString('Options','Print24Hours','Error') = '0' then
     begin
          Print24HoursCheckBox.Checked := FALSE;
     end
  else
     begin
          Print24HoursCheckBox.Checked := TRUE;
     end; {if INISettingsFile.ReadString('Options','Print24Hours','Error')}

  if INISettingsFile.ReadString('Options','HideTaskButton','Error') = '0' then
     begin
          HideTaskButtonCheckBox.Checked := FALSE;
     end
  else
     begin
          HideTaskButtonCheckBox.Checked := TRUE;
     end; {if INISettingsFile.ReadString('Options','HideTaskButton','Error')}

  if INISettingsFile.ReadString('Options','FlashColorsWhileTiming','Error') = '1' then
     begin
          FlashColorsWhileTimingCheckBox.Checked := TRUE;
     end
  else
     begin
          FlashColorsWhileTimingCheckBox.Checked := FALSE;
     end; {if INISettingsFile.ReadString('Options','FlashColorsWhileTiming','Error')}

  if INISettingsFile.ReadString('Options','DisableTimerKeyShortcuts','Error') = '1' then
     begin
          DisableTimerKeyShortcutsCheckBox.Checked := TRUE;
     end
  else
     begin
          DisableTimerKeyShortcutsCheckBox.Checked := FALSE;
     end; {if INISettingsFile.ReadString('Options','DisableTimerKeyShortcuts','Error')}

  ShowCalendarCheckBox.Checked := INISettingsFile.ReadBool('Options','ShowCalendar',True);

  LoadCustomFields;
  LoadGridColumns;
  LoadTSGOptions;

  PageControl1.ActivePage := GUIOptionsTabSheet;
end; {procedure TPreferencesForm.FormCreate}

//*****************************************************************************

procedure TPreferencesForm.ShowTrayIconCheckBoxClick(Sender: TObject);
{ This procedure enables the tray icon option if the user has elected to
  enable it. }
begin
     if ShowTrayIconCheckBox.Checked = TRUE then
        begin
             TrayIconOptionsRadioGroup.Enabled := TRUE;
             HideTaskButtonCheckBox.Enabled    := TRUE;
        end
     else
        begin
             TrayIconOptionsRadioGroup.Enabled := FALSE;
             HideTaskButtonCheckBox.Enabled    := FALSE;
        end; {if ShowTrayIconCheckBox.Checked}
end; {procedure TPreferencesForm.ShowTrayIconCheckBoxClick}

//*****************************************************************************

procedure TPreferencesForm.TrayIconOptionsRadioGroupClick(Sender: TObject);
{ This procedure sets a global variable for Tray Icon click behavior after the
  use specified the preference. }
begin
     if TrayIconOptionsRadioGroup.ItemIndex = 0 then
        begin
             TMMain.TrayIconClickRestores := TRUE;
        end
     else
        begin
             TMMain.TrayIconClickRestores := FALSE;
        end; {if TrayIconOptionsRadioGroup.ItemInde}
end; {procedure TPreferencesForm.TrayIconOptionsRadioGroupClick}

//*****************************************************************************

procedure TPreferencesForm.ChangeColumnHeaderFontBitBtnClick(
  Sender: TObject);
{ This procedure allows the user to graphically select font options for
  report column headers.}
begin
  {Set font dialog properties to currently-defined preference}
  ColumnHeadersFontDialog.Font.Size  := ColumnHeaderFontSampleEdit.Font.Size;
  ColumnHeadersFontDialog.Font.Name  := ColumnHeaderFontSampleEdit.Font.Name;
  ColumnHeadersFontDialog.Font.Style := ColumnHeaderFontSampleEdit.Font.Style;

  ColumnHeadersFontDialog.Execute;

  ShowColumnHeaderFont( ColumnHeadersFontDialog.Font.Name,
                        ColumnHeadersFontDialog.Font.Size,
                        ColumnHeadersFontDialog.Font.Style );
end; {procedure TPreferencesForm.ChangeColumnHeaderFontBitBtnClick}

//*****************************************************************************

procedure TPreferencesForm.ChangeDetailLineFontBitBtnClick(
  Sender: TObject);
{ This procedure allows the user to graphically select font options for
  report detail lines.}
begin
  {Set font dialog properties to currently-defined preference}
  DetailLinesFontDialog.Font.Size  := DetailLineFontSampleEdit.Font.Size;
  DetailLinesFontDialog.Font.Style := DetailLineFontSampleEdit.Font.Style;
  DetailLinesFontDialog.Font.Name  := DetailLineFontSampleEdit.Font.Name;

  DetailLinesFontDialog.Execute;

  ShowDetailLineFont( DetailLinesFontDialog.Font.Name,
                      DetailLinesFontDialog.Font.Size,
                      DetailLinesFontDialog.Font.Style );
end; {procedure TPreferencesForm.ChangeDetailLineFontBitBtnClick}

//*****************************************************************************

procedure TPreferencesForm.HeaderRxFontComboBoxChange(Sender: TObject);
{ Reset Preference Box to newly selected font. }
begin
  ColumnHeaderFontSampleEdit.Font.Name := (Sender As TComboBox).Items[ (Sender As TComboBox).ItemIndex ];
end; {procedure TPreferencesForm.HeaderRxFontComboBoxChange}

//*****************************************************************************

procedure TPreferencesForm.HeaderFontSizeRxSpinEditChange(Sender: TObject);
{ Reset Preference Box to newly selected font. }
begin
  ColumnHeaderFontSampleEdit.Font.Size := Trunc(( Sender as TSpinEdit).Value);
end; {procedure TPreferencesForm.HeaderFontSizeRxSpinEditChange}

//*****************************************************************************

procedure TPreferencesForm.DetailFontSizeRxSpinEditChange(Sender: TObject);
{ Reset Preference Box to newly selected font size. }
begin
  DetailLineFontSampleEdit.Font.Size := Trunc((Sender as TSpinEdit).Value);
end; {procedure TPreferencesForm.DetailFontSizeRxSpinEditChange}

//*****************************************************************************

procedure TPreferencesForm.DetailRxFontComboBoxChange(Sender: TObject);
{ Reset Preference Box to newly selected font size. }
begin
  DetailLineFontSampleEdit.Font.Name := (Sender As TComboBox).Items[ (Sender As TComboBox).ItemIndex ];
end; {procedure TPreferencesForm.DetailRxFontComboBoxChange}

//*****************************************************************************

procedure TPreferencesForm.FormDestroy(Sender: TObject);
{ Free resources when Preferences Dialog is destroyed. }
begin
  GroupOptionsFrame.Free;
  INISettingsFile.Free;
end; {TPreferencesForm.FormDestroy}

//*****************************************************************************

procedure TPreferencesForm.ResetProjectFontOptionsBitBtnClick(Sender: TObject);
{ This procedure will reset the font options to defaults when the "Reset
  to Defaults" button is pressed.}
begin
  ShowColumnHeaderFont( DEFAULT_FONTNAME, SUMMARY_FONTSIZE, [fsBold, fsUnderline] );
  ShowDetailLineFont(   DEFAULT_FONTNAME, DETAIL_FONTSIZE, [] );

  PrintSlackInformationCheckBox.Checked := TRUE;
  PrintFileNameCheckBox.Checked         := TRUE;
  PrintRateInformationCheckBox.Checked  := TRUE;
  PrintEndNotesCheckBox.Checked         := TRUE;
  PrintProjectTitleCheckBox.Checked     := TRUE;
  Print24HoursCheckBox.Checked          := FALSE;
end; {procedure TPreferencesForm.ResetFontOptionsBitBtnClick}

//*****************************************************************************

procedure TPreferencesForm.FormShow(Sender: TObject);
begin
end; {procedure TPreferencesForm.FormShow}

//*****************************************************************************

procedure TPreferencesForm.HideTaskButtonCheckBoxClick(Sender: TObject);
begin
     if HideTaskButtonCheckBox.Checked = TRUE then
        begin
             TMMain.HideTaskButton := TRUE;
             showwindow(application.handle, sw_hide);
        end
     else
        begin
             TMMain.HideTaskButton := FALSE;
             showwindow(application.handle, sw_showdefault);
        end;
end; {procedure TPreferencesForm.HideTaskButtonCheckBoxClick}

//*****************************************************************************

procedure TPreferencesForm.FlashColorsWhileTimingCheckBoxClick(
  Sender: TObject);
begin
     if FlashColorsWhileTimingCheckBox.Checked = TRUE then
        begin
             TMMain.FlashColorsWhileTiming := TRUE;
        end
     else
        begin
             TMMain.FlashColorsWhileTiming := FALSE;
        end;

     {Either way, reset timer to "Red" if it is currently clear so when user turns
      this off while timing at it happens to be clear, it will revert to red again.}
     if (TSMainForm.StatusTimer.Enabled = TRUE) and (TMMain.TrayIconIsRedFlag = FALSE) then
        begin
            // TSMainForm.ApplicationRxTrayIcon.Icon := TSMainForm.ApplicationRxTrayIcon.Icons[1]
        end;
end; {procedure TPreferencesForm.FlashColorsWhileTimingCheckBoxClick}

//*****************************************************************************

procedure TPreferencesForm.DisableTimerKeyShortcutsCheckBoxClick(
  Sender: TObject);
begin
  TSMainForm.EnableTimerKeyShortcuts := not DisableTimerKeyShortcutsCheckBox.Checked;
end;

procedure TPreferencesForm.DefaultDirectoryCheckBoxClick(Sender: TObject);
begin
  DirectoryGroupBox.Enabled := (Sender As TCheckbox).Checked;
end;

procedure TPreferencesForm.DirectoryButtonClick(Sender: TObject);
{ Let the user select a directory
}
const
  SELDIRHELP = 1000;
var
  Dir: string;
begin
  Dir := DirectoryEdit.Text;
  if SelectDirectory( Dir, [sdAllowCreate, sdPerformCreate, sdPrompt],SELDIRHELP ) then
    DirectoryEdit.Text := Dir;
end;

procedure TPreferencesForm.ChangeFieldNameButtonClick(Sender: TObject);
var
  i : Integer;
  SelectedText, FieldType, FieldName, OldFieldName : String;
begin
  i := CustomFieldList.ItemIndex;
  if ( i > -1 ) then
  begin
    SelectedText := CustomFieldList.Items[ i ];
    FieldName    := trim( copy( SelectedText, 1, WIDTH_FIELDNAME - 1 ));
    OldFieldName := FieldName;
    if InputQuery('New Field Name','Enter a new field name for this column:', FieldName) then
    begin
      FieldName := Copy( FieldName, 1, WIDTH_FIELDNAME - 1 );
      FieldType := Copy( SelectedText, WIDTH_FIELDNAME + 1, Length( SelectedText ));
      CustomFieldList.Items[ i ] := Format( '%-*s%s',[WIDTH_FIELDNAME, FieldName, FieldType] );

      i := GridColumnCheckList.Items.IndexOf( OldFieldName );
      if ( i > -1 ) then
        GridColumnCheckList.Items[i] := FieldName;
      ColumnLayoutChanged := True;
    end;
  end;
end;

procedure TPreferencesForm.ChangeDropdownListButtonClick(Sender: TObject);
var
  i : Integer;
  FieldName, ColumnTitle : String;
begin
  i := CustomFieldList.ItemIndex;
  if ( i > -1 ) then
  begin
    case i of
      0 : FieldName := 'Unused2';
      1 : FieldName := 'Unused3';
      2 : FieldName := 'Unused5';
      3 : FieldName := 'Unused6';
    end; {case}

    ColumnTitle := trim( copy( CustomFieldList.Items[i], 1, WIDTH_FIELDNAME - 1 ));

    with TDropListBuilderForm.Create( Self ) do
    try
      Execute( FieldName, ColumnTitle );
    finally
      Free;
    end;
  end;
end;

procedure TPreferencesForm.GridColumnUpButtonClick(Sender: TObject);
begin
  if ( GridColumnCheckList.ItemIndex > 0 ) then
  begin
    GridColumnCheckList.Items.Exchange( GridColumnCheckList.ItemIndex, GridColumnCheckList.ItemIndex - 1 );
    ColumnLayoutChanged := True;
  end;
end;

procedure TPreferencesForm.GridColumnDownButtonClick(Sender: TObject);
begin
  if ( GridColumnCheckList.ItemIndex < GridColumnCheckList.Count - 1 ) then
  begin
    GridColumnCheckList.Items.Exchange( GridColumnCheckList.ItemIndex, GridColumnCheckList.ItemIndex + 1 );
    ColumnLayoutChanged := True;
  end;
end;

procedure TPreferencesForm.LoadCustomFields;
{ Load the custom field names - keep design time "types"
}
var
  i : Integer;
  FieldName, FieldType : String;
begin
  for i := 0 to CustomFieldList.Count - 1 do
  begin
    case i of
      0 : FieldName := INISettingsFile.ReadString( SEC_FIELDNAMEMAP, 'Unused2', 'Custom 1' );
      1 : FieldName := INISettingsFile.ReadString( SEC_FIELDNAMEMAP, 'Unused3', 'Custom 2' );
      2 : FieldName := INISettingsFile.ReadString( SEC_FIELDNAMEMAP, 'Unused5', 'Custom 3' );
      3 : FieldName := INISettingsFile.ReadString( SEC_FIELDNAMEMAP, 'Unused6', 'Custom 4' );
    end; {case}

    FieldType := Copy( CustomFieldList.Items[i], WIDTH_FIELDNAME + 1, Length( CustomFieldList.Items[i] ));
    CustomFieldList.Items[i] := Format( '%-*s%s',[WIDTH_FIELDNAME, FieldName, FieldType] );
  end; {for}
end;

procedure TPreferencesForm.SaveCustomFields;
{ Save the custom field name mappings and selection
}
var
  i         : Integer;
  FieldName : String;
begin
  INISettingsFile.EraseSection( SEC_FIELDNAMEMAP );
  for i := 0 to CustomFieldList.Count - 1 do
  begin
    FieldName := Copy( CustomFieldList.Items[i], 1, WIDTH_FIELDNAME - 1 );
    case i of
      0 : INISettingsFile.WriteString( SEC_FIELDNAMEMAP, 'Unused2', FieldName );
      1 : INISettingsFile.WriteString( SEC_FIELDNAMEMAP, 'Unused3', FieldName );
      2 : INISettingsFile.WriteString( SEC_FIELDNAMEMAP, 'Unused5', FieldName );
      3 : INISettingsFile.WriteString( SEC_FIELDNAMEMAP, 'Unused6', FieldName );
    end; {case}
  end; {for}
end;


procedure TPreferencesForm.GridColumnCheckListClickCheck(Sender: TObject);
begin
  ColumnLayoutChanged := True;
end;

procedure TPreferencesForm.ResetProjectOptionsBitBtnClick(Sender: TObject);
{ Reset project (and project group, where applicable) options
}
begin
  SetDefaultTSGOptions;
end;

procedure TPreferencesForm.LoadTSGOptions;
{ Load project (and project group, where applicable) options
}
begin
  ProjectTitleEdit.Text := TMUtils.CurrentProject.ProjectName;
  EndNotesEdit.Text     := TMUtils.CurrentProject.EndNotes;
  HourlyRateEdit.Text   := TMUtils.HourlyRateToEditText( TMUtils.CurrentProject.HourlyRate );

  if CurrentProject.IsProjectGroup then
  begin
    LoadReportComboBox;
    GroupOptionsFrame.CurrencySymbolEdit.Text  := CurrentProject.CurrencySymbol;
    GroupOptionsFrame.ReportComboBox.ItemIndex := GroupOptionsFrame.ReportComboBox.Items.IndexOf( CurrentProject.DefaultReport );
  end;

  GroupOptionsPanel.Visible := CurrentProject.IsProjectGroup;
end;

procedure TPreferencesForm.SaveTSGOptions;
{ Save the project options
  Nb. Save the currency symbol *before* the hourly rate
}
begin
  if CurrentProject.IsProjectGroup then
  begin
    CurrentProject.CurrencySymbol := GroupOptionsFrame.CurrencySymbolEdit.Text;
    CurrentProject.DefaultReport  := GroupOptionsFrame.ReportComboBox.Items[ GroupOptionsFrame.ReportComboBox.ItemIndex ];
    GroupOptionsFrame.Save;
    if GroupOptionsFrame.NeedsReload then
      MessageDlg( 'If you have changed the active projects list,'+#13+#10
                 +'you should now save any changes, '+#13+#10
                 +'then close and re-open the group.', mtWarning, [mbOK], 0);
  end;

  TMUtils.CurrentProject.ProjectName := ProjectTitleEdit.Text;
  TMUtils.CurrentProject.EndNotes    := EndNotesEdit.Text;
  TMUtils.CurrentProject.HourlyRate  := TMUtils.EditTextToHourlyRate( HourlyRateEdit.Text );

  TMUtils.SaveProjectOptions( CurrentProject.FileName, CurrentProject );
end;

procedure TPreferencesForm.SetDefaultTSGOptions;
{ Reset project (and project group, where applicable) options
}
begin
  ProjectTitleEdit.Text := TMUtils.DefaultProject.ProjectName;
  EndNotesEdit.Text     := TMUtils.DefaultProject.EndNotes;
  HourlyRateEdit.Text   := TMUtils.HourlyRateToEditText( TMUtils.DefaultProject.HourlyRate );

  if CurrentProject.IsProjectGroup then
  begin
    GroupOptionsFrame.CurrencySymbolEdit.Text := DefaultProject.CurrencySymbol;
  end;
end;

procedure TPreferencesForm.LoadReportComboBox;
{ Populate the reports combo box with the list of available reports
}
begin
  if FileExists( ExtractFilepath(application.ExeName)+'\TSReport.rav' ) then
    with TRvProject.Create( Self ) do
    try
      ProjectFile := ExtractFilepath(application.ExeName)+'\TSReport.rav';
      Open;
      GetReportList( GroupOptionsFrame.ReportComboBox.Items, False);
      GroupOptionsFrame.ReportComboBox.Items.Insert( 0, TXT_GROUPEDBYDAY );
      GroupOptionsFrame.ReportComboBox.Items.Insert( 1, TXT_GROUPEDBYDAYANDPROJECT );
      GroupOptionsFrame.ReportComboBox.ItemIndex := 1;
    finally
      Free;
    end;
end;

procedure TPreferencesForm.ShowCalendarCheckBoxClick(Sender: TObject);
begin
  TSMainForm.TSGrid.CalendarPanel.Visible := (Sender As TCheckBox).Checked;
end;

end.
