{
Application:    Time Stamp, Copyright 1997 William Rouck
Module:         TMUtils.pas
Contact:        email: wrouck@syntap.com
Modifications:  JM Holloway, June 2004
                justin@schiznit.net

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version. This
program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details. You should have
received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
02111-1307, USA.
}

unit TMUtils;

{This unit contains miscellaneous reusable procedures and functions.}

interface

uses
  Sysutils, Controls, Dialogs, WinTypes, WinProcs, Printers, Graphics, Forms,
  IniFiles, Classes,
  System.IO,
  System.Runtime.Serialization,
  System.Collections,
  System.Runtime.Serialization.Formatters.Binary;

const
  ROW1_COLOR        = $00E1E1E1;
  ROW2_COLOR        = $00C2FDFD;
  DUPLICATE_COLOR   = $004646FF;
  OVERTIME_COLOR    = $00FFA4A4;
  LASTDAYOFWEEK_COLOR = $00CCCC00;
  ApplicationTitle  = 'Time Stamp';
  DEFAULT_FONTNAME  = 'Arial';
  DETAIL_FONTSIZE   = 9;
  SUMMARY_FONTSIZE  = 10;
  DEFINABLE_COLUMNS = 9;
  SEC_GRIDCOLUMNS   = 'Grid Columns';        // ini section name for grid columns
  SEC_FIELDNAMEMAP  = 'Field Name Mappings'; // ini section name for custom columns
  SEC_COLUMNWIDTHS  = 'Column Widths';       // ini section name for column widths
  TSG_OPTIONS        = 'TSG OPTIONS';        // TSG file section name for project group options

  FILE_XML = 'D:\TimestampData\1\TSXML\Derceto_';


  TXT_GROUPEDBYDAY           = 'Grouped by day';
  TXT_GROUPEDBYDAYANDPROJECT = 'Grouped by day then project';

type
  TTimingState      = (csTiming, csSlacking, csStopped, csOpened, csClosed);
  TTimingStatus     = record
    TimingState     : TTimingState;
    BackgroundColor : TColor;
    FontColor       : TColor;
    Text            : String[20];
  end;

var
  tsTiming     : set of TTimingState = [csTiming..csSlacking];
  tsNotTiming  : set of TTimingState = [csStopped..csClosed];
  CurrentState : TTimingState = csClosed;

  TimingStatus : array [csTiming..csClosed] of TTimingStatus = (
    (TimingState: csTiming;     BackgroundColor: clRed;     FontColor: clWhite;      Text: 'TIMING'),
    (TimingState: csSlacking;   BackgroundColor: clYellow;  FontColor: clWindowText; Text: 'SLACKING'),
    (TimingState: csStopped;    BackgroundColor: clGreen;   FontColor: clWhite;      Text: 'Stopped.'),
    (TimingState: csOpened;     BackgroundColor: clBtnFace; FontColor: clWindowText; Text: ''),
    (TimingState: csClosed;     BackgroundColor: clBtnFace; FontColor: clWindowText; Text: '')
  );


type
  TimeFileHeader_TS2 = record
             LastHourlyRate : double;
             FileVersion    : word;
             ProjectName    : String[255];
  end; {record TimeFileHeader_SA2}

  TimeRecord_SA2 = record
             StartTime  : TDateTime;
             EndTime    : TDateTime;
             DiffTime   : double;
             WorkTime   : double;
             SlackTime  : double;
             OverTime   : double;
             HourlyRate : double;
             Task_Num   : double;
             Unused2    : double;
             Unused3    : double;
             Notes      : string[255];
             EndNotes   : string[255];
             Unused5    : string[255];
             Unused6    : string[255];
             pkGUID     : string[255];
             ProjectCode: string[50];
  public
    function DayNumber: Integer;
    function DayNumberStr: String;
    function DayProject: string;
    function IsValid: Boolean;
    procedure CreateNewGuid;
    procedure Clear;
    procedure Assign(Src: TimeRecord_SA2);
    procedure CloneAsNew(Src: TimeRecord_SA2; NewStartTime: TDateTime); 
  end; {record TimeRecord_SA2}

  { A single instance of the current timing structure }
  TTimingRecord = record
    TimeRecord        : TimeRecord_SA2;
    ContinueTime      : Boolean;  {Flag to set if this is a continued (not new) time row}
    SessionStartTime  : TDateTime;
    SessionStartSlack : TDateTime;
  public
    procedure StartTiming(Ts2: TimeRecord_SA2);
    procedure StartNewTiming;
  end;


  TProjectDetail = record
    IsProjectGroup : Boolean;      // Is a .TSG file
    FileName       : String[255];  // Load from/save to
    FileVersion    : word;
    ProjectName    : String[255];  // Project display name and report title
    EndNotes       : String[255];  // Report display end notes
    HourlyRate     : double;       // Default $ per hour
    IndexName      : String;       // Last active sort order
    CurrencySymbol : String;       // Per project group currency symbol, if different from the locale default
    DefaultReport  : String;       // Per project group default report
  end;

  TimeFile_SA2   = FileStream;  //file of TimeRecord_SA2;
  HeaderFile_TS2 = FileStream;  //file of TimeFileHeader_TS2;

procedure SaveTimeRecordDataFile( FileName: String );
procedure SaveTimeRecordDataFileAs;
procedure OpenTimeRecordDataFile( FileName: String );

function  IsXMLFile(const FileName: String): Boolean;
function  XmlFileName(const FileName: String): String;
procedure SaveXMLFile(FileName : String);

procedure OpenProjectGroup( FileName : String );
procedure OpenDataFile (FileName : String; OpenType : String; FileVersion : word; ProjectCode: String='' );
procedure AppendTimeRecordDataFile;
procedure SetMainFormProperties;
procedure ClearStringGrid(ClearType : string);
function  GetINIFilePath : string;
function  GetHelpFilePath : string;
function  GetAutoFillFilePath : string;
function  CalculateTimeDifference (Time1, Time2 : TDateTime) : TDateTime;
procedure VerifyFileSavedStatus;
procedure ExportTimeRecordDataFile;
function  FormatTimeTotal (InputTime : TDateTime) : string;
function  FormatSummaryTime (InputTime : TDateTime; bDecimalSummary: Boolean = False) : string;
procedure SavePreferedLayout;
procedure LoadPreferedLayout;
procedure AdjustAlwaysOnTop (StartToggle : word);
function  ElapsedTime: TDateTime;             { Time elapsed since timing started }
function  FormatTimeDelta (InputTime : TDateTime) : string; { Time diff as 3h 10m }
function  FirstNoteLine( Note: String ): String;        { First line from a note }
procedure ClearTimeRecord( var ts: TimeRecord_SA2 );
procedure SetTimingState( tsNewState: TTimingState );
procedure FlagProjectAsChanged( ProjectName: String );
procedure AddTSGProject(ProjectCode: String);
procedure ClearProject;
function  IsTimingRecord(RowGUID: String): Boolean;

function  LoadProjectOptions( FileName: String ): TProjectDetail;
procedure SaveProjectOptions( FileName: String; ProjectOptions: TProjectDetail );

function  HourlyRateToEditText( HourlyRate: double ): String;
function  EditTextToHourlyRate( EditText: String ): Double;

procedure DebugMsg(Msg: String; Args: array of const); overload;
procedure DebugMsg(Msg: String); overload;


var
  TimeRecord        : TimeRecord_SA2;
  TimingRecord      : TTimingRecord;
  CurrentProject    : TProjectDetail;
  DefaultProject    : TProjectDetail;
  TSGProjects       : TStringList = nil;  // Project group names

implementation

uses
  TMMain, TMGridFrme, DBGrids, TMDataModule;

const
  TXT_FILENOTFOUND    = 'NOT FOUND';
  DEFAULT_FILEVERSION = 2;

procedure SaveDataFile( FileName: String; ProjectCode: String='' ); forward;
procedure SaveProjectGroup( FileName: String ); forward;
procedure SetCurrentFileName( FileName: String ); forward;
procedure SaveHeaderFile( FileName: String; ProjectOptions: TProjectDetail ); forward;

var
  TSGChangedProjects : TStringList = nil;

procedure DebugMsg(Msg: String; Args: array of const); overload;
begin
  OutputDebugString(Format(Msg, Args));
end;

procedure DebugMsg(Msg: String); overload;
begin
  OutputDebugString(Msg);
end;

function IsTimingRecord(RowGUID: String): Boolean;
{ True if the row matches the currently timing record
}
begin
  Result := SameText(RowGUID, TMUtils.TimingRecord.TimeRecord.pkGUID);
end;

procedure ClearProject;
{ Remove all project settings - when starting a new project
}
begin
  CurrentProject.FileName := '';
  SetTimingState( csClosed );

  if Assigned( TMUtils.TSGProjects ) then
    TMUtils.TSGProjects.Clear;
  if Assigned( TMUtils.TSGChangedProjects ) then
    TMUtils.TSGChangedProjects.Clear;

  ClearStringGrid( '' );
  TSMainForm.UpdateGUI;
end;


function  HourlyRateToEditText( HourlyRate: double ): String;
{ Format the hourly rate to display in a user edit control
}
begin
  Result := CurrentProject.CurrencySymbol + FormatFloat( '0.00', HourlyRate );
end;

function  EditTextToHourlyRate( EditText: String ): Double;
{ Format a user input control text to hourly rate field
}
begin
  EditText := StringReplace( EditText, CurrentProject.CurrencySymbol, '', [] );
  EditText := StringReplace( EditText, CurrencyString, '', [] );
  Result   := StrToCurrDef ( EditText, 0.00 );
end;

procedure FlagProjectAsChanged( ProjectName: String );
{ Maintain a list of changed projects so we only save projects with altered records
}
begin
  if not Assigned( TSGChangedProjects ) then
  begin
    TSGChangedProjects            := TStringList.Create;
    TSGChangedProjects.Duplicates := dupIgnore;
    TSGChangedProjects.CaseSensitive := False;
    TSGChangedProjects.Sorted     := True;
  end;
  if (Trim(ProjectName) <> '') and (TSGChangedProjects.IndexOf( ProjectName ) = -1) then
    TSGChangedProjects.Add( ProjectName );
end;

function  LoadProjectOptions( FileName: String ): TProjectDetail;
{ Load project options from the appropriate file
}
var
  TSGFile     : TIniFile;
begin
  Result.IsProjectGroup := SameText( ExtractFileExt( FileName ), '.TSG' );
  Result.FileName       := FileName;

  if SameText( ExtractFileExt( FileName ), '.TSG' ) then
  begin
    TSGFile := TINIFile.Create( FileName );
    try
      Result.ProjectName    := TSGFile.ReadString(  TSG_OPTIONS, 'ProjectName',    DefaultProject.ProjectName );
      Result.EndNotes       := TSGFile.ReadString(  TSG_OPTIONS, 'EndNotes',       DefaultProject.EndNotes );
      Result.FileVersion    := TSGFile.ReadInteger( TSG_OPTIONS, 'FileVersion',    DEFAULT_FILEVERSION );
      Result.HourlyRate     := TSGFile.ReadFloat(   TSG_OPTIONS, 'HourlyRate',     DefaultProject.HourlyRate );
      Result.IndexName      := TSGFile.ReadString(  TSG_OPTIONS, 'IndexName',      DefaultProject.IndexName );
      Result.CurrencySymbol := TSGFile.ReadString(  TSG_OPTIONS, 'CurrencySymbol', DefaultProject.CurrencySymbol );
      Result.DefaultReport  := TSGFile.ReadString(  TSG_OPTIONS, 'DefaultReport',  DefaultProject.DefaultReport );

      if ( Result.CurrencySymbol <> DefaultProject.CurrencySymbol ) then
        CurrencyString := Result.CurrencySymbol;
    finally
      TSGFile.Free;
    end;
  end;
end;

procedure SaveProjectOptions( FileName: String; ProjectOptions: TProjectDetail );
{ Save project group options in the appropriate file
}
var
  i: Integer;
  TSGFile : TIniFile;
begin
  if SameText( ExtractFileExt( FileName ), '.TSG' ) then
  begin
    TSGFile := TINIFile.Create( FileName );
    try
      TSGFile.WriteString(  TSG_OPTIONS, 'ProjectName',   ProjectOptions.ProjectName );
      TSGFile.WriteString(  TSG_OPTIONS, 'EndNotes',      ProjectOptions.EndNotes );
      TSGFile.WriteInteger( TSG_OPTIONS, 'FileVersion',   ProjectOptions.FileVersion );
      TSGFile.WriteString(  TSG_OPTIONS, 'HourlyRate',    FormatFloat( '0.00', ProjectOptions.HourlyRate ));
      TSGFile.WriteString(  TSG_OPTIONS, 'IndexName',     TSMainForm.TSGrid.IndexName );
      TSGFile.WriteString(  TSG_OPTIONS, 'CurrencySymbol',ProjectOptions.CurrencySymbol );
      TSGFile.WriteString(  TSG_OPTIONS, 'DefaultReport', ProjectOptions.DefaultReport );

      if Assigned(TSGProjects) then
        for i := 0 to TSGProjects.Count - 1 do
          TSGFile.WriteBool(TSGProjects[i], 'Active', True);
    finally
      TSGFile.Free;
    end;
  end
  else
    SaveHeaderFile( FileName, CurrentProject );
end;

function ElapsedTime: TDateTime;
{ Add WorkTime and SlackTime figure to StatusDiffTime in case time was continued or
  AutoSave is enabled. This will represent the total task time plus any new time.
}
begin
  if not Assigned( TSMainForm ) then Exit;

  Result := TimingRecord.TimeRecord.WorkTime + TimingRecord.TimeRecord.SlackTime;
  case CurrentState of
    csTiming :
      Result := Result + CalculateTimeDifference( TimingRecord.SessionStartTime, now );
    csSlacking :
      Result := Result + CalculateTimeDifference( TimingRecord.SessionStartSlack, now );
  end;
end;

procedure SetTimingState( tsNewState: TTimingState );
{ Switch to the new mode; cleaning up the current record where applicable

  Passing the currentstate thru this function updates the timingrecord,
  this is used before edit/print/save while a record is timing.
}
var
  SessionTime: TDateTime;
begin
  { Update the time worked }
  case CurrentState of
    csTiming :
      begin
        SessionTime := CalculateTimeDifference( TimingRecord.SessionStartTime, now );
        TimingRecord.TimeRecord.WorkTime  := TimingRecord.TimeRecord.WorkTime + SessionTime;
        TimingRecord.TimeRecord.DiffTime  := TimingRecord.TimeRecord.DiffTime + SessionTime;
      end;
    csSlacking :
      begin
        SessionTime := CalculateTimeDifference( TimingRecord.SessionStartSlack, now );
        TimingRecord.TimeRecord.SlackTime := TimingRecord.TimeRecord.SlackTime + SessionTime;
        TimingRecord.TimeRecord.DiffTime  := TimingRecord.TimeRecord.DiffTime + SessionTime;
      end;
  end;

  { Reset the time started }
  case tsNewState of
    csTiming, csSlacking :
      begin
        TimingRecord.SessionStartTime   := now;
        TimingRecord.SessionStartSlack  := now;
        TimingRecord.TimeRecord.EndTime := now;
      end;
    csStopped :
      TimingRecord.TimeRecord.EndTime := now;
  end;

  if ( tsNewState = CurrentState ) and ( tsNewState in tsTiming ) then
    TimingRecord.ContinueTime := TRUE;
  if ( tsNewState in [csTiming..csStopped] ) and not ( CurrentState = csOpened ) then
    TSMainForm.RecordIsChanged := True;
  CurrentState := tsNewState;
end;

procedure ClearTimeRecord( var ts: TimeRecord_SA2 );
{ Reset the contents of the timerecord; allocate a new GUID
}
var
  EmptyTimeRecord : TimeRecord_SA2;
begin
  ts := EmptyTimeRecord; // The ".NET way" to clear a structure
  ts.CreateNewGuid;
  ts.HourlyRate := CurrentProject.HourlyRate;
end;

function ReadShortString( var r: BinaryReader; iBytes: integer ): String;
{ Read string[x] from a binary file
}
var
  iLen  : Integer;
  myarr : array of byte;
begin
  iLen   := Integer( r.ReadByte );
  myarr  := r.ReadBytes( iBytes );

  if ( iLen >= iBytes ) then
    iLen := iBytes-1;
  Result := Copy( myarr, 0, iLen );
end;

procedure WriteShortString( var w: BinaryWriter; ss: ShortString; iLen: Integer );
{ Write string[x] to a binary file
}
var
  b   : Byte;
begin
  w.Write( byte( ss[0] ) );
  for b := 1 to iLen do
    w.Write( char( ss[b] ));
end;

procedure AppendTimeRecord_SA2ToFile( var bw: BinaryWriter; ts2: TimeRecord_SA2 );
{ Write a TimeRecord_SA2 record to binary file. Overtime is not stored.
}
begin
  with ts2 do
    begin
      bw.Write( Double( StartTime ));
      bw.Write( Double( EndTime ));
      bw.Write( Double( DiffTime ));
      bw.Write( Double( WorkTime ));
      bw.Write( Double( SlackTime ));
      bw.Write( HourlyRate );
      bw.Write( Task_Num );
      bw.Write( Unused2 );
      bw.Write( Unused3 );
      WriteShortString( bw, Notes, 255 );
      WriteShortString( bw, EndNotes, 255 );
      WriteShortString( bw, Unused5, 255 );
      WriteShortString( bw, Unused6, 255 );
    end;
end; {procedure AppendTimeRecord_SA2ToFile}

procedure ReadTimeRecord_SA2FromFile( var r: BinaryReader; var ts2: TimeRecord_SA2 );
{ Populate the ts2 param with a record
  from the current position in the file fs
}
begin
  with ts2 do
    begin
      StartTime  := r.ReadDouble;
      EndTime    := r.ReadDouble;
      DiffTime   := r.ReadDouble;
      WorkTime   := r.ReadDouble;
      SlackTime  := r.ReadDouble;
      HourlyRate := r.ReadDouble;
      Task_Num   := r.ReadDouble;
      Unused2    := r.ReadDouble;
      Unused3    := r.ReadDouble;
      Notes      := ReadShortString( r, 255 );
      EndNotes   := ReadShortString( r, 255 );
      Unused5    := ReadShortString( r, 255 );
      Unused6    := ReadShortString( r, 255 );
      Overtime   := 0; // Overtime is not stored.
    end;
end; {procedure ReadTimeRecord_SA2FromFile}

procedure AppendTimeFileHeader_TS2ToFile( var fs: FileStream; tf2: TimeFileHeader_TS2 );
{ Write a TimeFileHeader_TS2 record to binary file
}
var
  bw  : BinaryWriter;
begin
  bw  := BinaryWriter.Create( fs );
  with tf2 do
    begin
      bw.Write( LastHourlyRate );
      bw.Write( FileVersion );
      WriteShortString( bw, ProjectName, 255 );
    end;
  bw.Close;
end; {procedure AppendTimeFileHeader_TS2ToFile}

function ReadTimeFileHeader_TS2FromFile( var fs: FileStream; var tf2: TimeFileHeader_TS2 ): Boolean;
{ Return true if we could populate the tf2 param with a record
  from the current position in the file fs
}
var
  r: BinaryReader;
begin
  Result := ( fs.Position < fs.Length );

  if Result then
  begin
    r  := BinaryReader.Create( fs );
    try
      with tf2 do
        begin
          LastHourlyRate := r.ReadDouble;
          FileVersion    := r.ReadUInt16;
          ProjectName    := ReadShortString( r, 255 );
        end;
    finally
      r.Close;
    end;
  end;
end; {procedure ReadTimeFileHeader_TS2FromFile}

procedure SetLastSavedFile( FileName: String );
{ Update saved file name  }
var
   INISettingsFile : TIniFile;
begin
   INISettingsFile := TINIFile.Create(TMUtils.GetINIFilePath);
   try
     INISettingsFile.WriteString('Options','LastSavedFile',FileName);
  finally
    INISettingsFile.Free;
  end;
end;

procedure SetCurrentFileName( FileName: String );
{ Update all references to the current project's filename.
}
begin
  if not SameText( CurrentProject.FileName, FileName ) then
    CurrentProject.FileName := FileName;

  TSMainForm.OpenFileDialog.FileName := FileName;
  TSMainForm.SaveFileDialog.FileName := FileName;

  TSMainForm.UpdateTitle;

  if not TMMain.MainFormIsCreating then
  begin
    {Save to MRU List}
    //zz     TSMainForm.MRUManager.Add(TSMainForm.SaveFileDialog.FileName,0);
    SetLastSavedFile( FileName );
  end;
end;

function ExpandedDataFileName( TSGFileName, DataFileName: String ): String;
{ Return the path to a file referenced by a project group.
  Try the relative path first; then the full path.
}
var
  RelativePath: String;
begin
  RelativePath := IncludeTrailingBackSlash( ExtractFilePath( TSGFileName ) );

  if System.IO.File.Exists( RelativePath + DataFileName ) then
    Result := RelativePath + DataFileName
  else if System.IO.File.Exists( DataFileName ) then
    Result := DataFileName
  else
    Result := TXT_FILENOTFOUND;
end;

procedure SaveTimeRecordDataFile( FileName: String );
{ This procedure saves a Time Record file or group
}
begin
  OutputDebugString( 'TMUtils.SaveTimeRecordDataFile ' + FileName );
  TSMainForm.TSGrid.PostChanges;

  if CurrentProject.IsProjectGroup
  or SameText( ExtractFileExt(FileName), '.TSG' ) then
     SaveProjectGroup( FileName )
  else
     SaveDataFile( FileName );

  SetCurrentFileName( FileName );
end;

procedure SaveProjectGroup( FileName: String );
{ Save all the changed project files in a project group
}
var
  TSGFile     : TIniFile;
  i           : Integer;
  DataFileName: String;
  UseXmlDataFile: Boolean;
begin
  Assert( FileName <> '', 'No filename passed to save' );

  UseXmlDataFile := System.IO.File.Exists(XmlFileName(FileName));


  if Assigned( TSGChangedProjects ) and ( TSGChangedProjects.Count > 0 ) then
  begin
    if UseXmlDataFile then
      TsData.Save(XmlFileName(FileName))
    else begin
      TSGFile := TINIFile.Create( FileName );
      try
        for i := 0 to TSGChangedProjects.Count - 1 do
        begin
          DataFileName := ExpandedDataFileName( FileName, TSGFile.ReadString( TSGChangedProjects[i], 'FileName', TXT_FILENOTFOUND ));
          // If the file has vapourised... save under the name of the projectcode
          if SameText( DataFileName, TXT_FILENOTFOUND ) then
            DataFileName := IncludeTrailingBackSlash( ExtractFilePath( FileName ) ) + TSGChangedProjects[i];
          SaveDataFile( DataFileName, TSGChangedProjects[i] );
        end;
  {$IFNDEF TESTSAVING}
        TSGChangedProjects.Clear;
  {$ENDIF}
      finally
        TSGFile.Free;
      end;
    end;
  end;

  SaveProjectOptions( FileName, CurrentProject );
end;

procedure SaveHeaderFile( FileName: String; ProjectOptions: TProjectDetail );
{ Save the project options for an individual project (a .TS2)
}
var
  HeaderFile      : HeaderFile_TS2;
  FileHeader_TS2  : TimeFileHeader_TS2;
begin
  Assert( not ProjectOptions.IsProjectGroup, 'Saving TS2 information in a project group' );

  {Set File Header info}
  FileHeader_TS2.LastHourlyRate := ProjectOptions.HourlyRate;
  FileHeader_TS2.ProjectName    := ProjectOptions.ProjectName;
  FileHeader_TS2.FileVersion    := ProjectOptions.FileVersion;

  {Write File Header File}
  if not System.IO.File.Exists( ChangeFileExt(FileName, '.TS2') ) then
    HeaderFile := FileStream.Create( ChangeFileExt(FileName, '.TS2'), FileMode.CreateNew )
  else
    HeaderFile := FileStream.Create(ChangeFileExt(FileName, '.TS2'), FileMode.Open, FileAccess.Write );

  try
    AppendTimeFileHeader_TS2ToFile(HeaderFile, FileHeader_TS2);
  finally
    HeaderFile.Close;
  end;
end;

procedure SaveDataFile( FileName: String; ProjectCode: String='' );
{This procedure saves an individual Time Record file
}
var
  DataFile        : TimeFile_SA2;
  DataWriter      : BinaryWriter;
  CurrentRow      : Integer;
begin
  Assert( FileName <> '', 'Empty filename passed to TMUtils.SaveDataFile' );
  OutputDebugString( 'Saving file ' + FileName );

  if not CurrentProject.IsProjectGroup then
    SaveHeaderFile( FileName, CurrentProject );

  {Get file handle and write data file}
  {Note that this had been coded if FileExists use FileMode.Open,FileAccess.Write;
   but that method was leaving unused/deleted records at the end of the file. }
{$IFDEF TESTSAVING}
  //FileName := ChangeFileExt(FileName, '.bak');
{$ENDIF}
  DataFile := FileStream.Create( FileName, FileMode.Create );
//  DebugMsg('Saving datafile %s', [FileName]);

  {Loop thru records in dataset writing out to file}
  DataWriter := BinaryWriter.Create( DataFile );
  try
    TSMainForm.TSGrid.BeginUpdate(True);
    TsData.ClearFilter;
    try
      CurrentRow := TSMainForm.TSGrid.ActiveRow;
//      DebugMsg('Stored currentrow recno=%d', [CurrentRow]);
      TsData.First;
      while not TsData.Eof do
        begin
          if ( not CurrentProject.IsProjectGroup ) or ( SameText( ProjectCode, TSMainForm.TSGrid.ActiveTask.ProjectCode )) then
          begin
            DebugMsg('Saving %d of %d %s notes %s',
              [
              TsData.AdoDataset.RecNo,
              TsData.AdoDataset.RecordCount,
              ProjectCode,
              String(TSMainForm.TSGrid.ActiveTask.Notes)
              ]);
            AppendTimeRecord_SA2ToFile( DataWriter, TSMainForm.TSGrid.ActiveTask );
          end;
          TsData.Next;
        end;
      TSMainForm.TSGrid.ActiveRow := CurrentRow;
    finally
      TSMainForm.TSGrid.EndUpdate;
    end;
  finally
    DataWriter.Close;
    DataFile.Close;
  end;

  {Set Change flag}
  TSMainForm.RecordIsChanged := FALSE;
end; {procedure SaveDataFile}

//*****************************************************************************

procedure SaveTimeRecordDataFileAs;
{This procedure saves a Time Record file}
begin
  if TSMainForm.SaveFileDialog.Execute then
     begin
          TMUtils.SaveTimeRecordDataFile( TSMainForm.SaveFileDialog.FileName );
          SetCurrentFileName( TSMainForm.SaveFileDialog.FileName );
     end; {if TSMainForm.SaveFileDialog.Execute}
end; {procedure SaveTimeRecordDataFile}

//*****************************************************************************

procedure OpenDataFile (FileName : String; OpenType : String; FileVersion : word; ProjectCode: String='' );
{ This procedure reads the contents of the passed FileName to a timestamp record;
  that record is then appended to a dynamic dataset.
}
var
  DataFile_SA2     : TimeFile_SA2;
  FileHeader_TS2   : TimeFileHeader_TS2;
  HeaderFile       : HeaderFile_TS2;
  DataFileReader   : BinaryReader;
begin
  OutputDebugString('TMUtils.OpenDataFile ' + FileName);
  CurrentProject.IsProjectGroup := SameText( OpenType, 'GROUP' );

  {Get file handle}
  if FileVersion <> 2 then
    raise Exception.CreateFmt( 'I can''t open version %d files!', [FileVersion] );

  if not CurrentProject.IsProjectGroup then
  begin
    {get file handle to header file}
    HeaderFile   := FileStream.Create( ChangeFileExt(FileName, '.TS2'), FileMode.Open, FileAccess.Read );
    try
      {Read Header info}
      ReadTimeFileHeader_TS2FromFile(HeaderFile, FileHeader_TS2);

       {Assign project info from header unless this is an Append}
       if OpenType <> 'APPEND' then
          begin
             CurrentProject.HourlyRate     := FileHeader_TS2.LastHourlyRate;
             CurrentProject.ProjectName    := FileHeader_TS2.ProjectName;
             CurrentProject.FileVersion    := FileHeader_TS2.FileVersion;
             CurrentProject.DefaultReport  := '';
             CurrentProject.CurrencySymbol := DefaultProject.CurrencySymbol;  // Only stored in project group files, to keep backward compatibility
             if ( CurrencyString <> DefaultProject.CurrencySymbol ) then
               CurrencyString := DefaultProject.CurrencySymbol;
             SetTimingState( csOpened );
             TSMainForm.UpdateTitle;
             TSMainForm.UpdateStatusPanel;
          end; {if OpenType <> 'APPEND'}

       {Close Header File}
       HeaderFile.Close;
    except
       begin
          if OpenType <> 'APPEND' then
             begin
                  CurrentProject.HourlyRate := 25.00;
                  CurrentProject.ProjectName := '';
                  CurrentProject.FileVersion := 2;
              end; {if OpenType <> 'APPEND'}
       end;
    end; {try}
  end;

  DataFile_SA2   := FileStream.Create( FileName, FileMode.Open, FileAccess.Read );
  DataFileReader := BinaryReader.Create( DataFile_SA2 );
  try
    TSMainForm.TSGrid.BeginUpdate;
    try
       while ( DataFile_SA2.Position < DataFile_SA2.Length ) do
             begin
                  ClearTimeRecord( TimeRecord );
                  ReadTimeRecord_SA2FromFile(DataFileReader, TimeRecord);
                  if OpenType = 'GROUP' then
                  begin
{$IFDEF TESTSAVING}
                    FlagProjectAsChanged(ProjectCode);
{$ENDIF}
                    TimeRecord.ProjectCode := ProjectCode;
                  end;
                  TsData.Append( TimeRecord );
             end;  {while}
    finally
      TSMainForm.TSGrid.EndUpdate;
    end;
  finally
    DataFileReader.Close;
    DataFile_SA2.Close; {end if}
  end;

  if ( OpenType = 'OPEN' ) or ( OpenType = 'GROUP' ) then
    TSMainForm.RecordIsChanged := FALSE
  else if (OpenType = 'APPEND') then
    TSMainForm.RecordIsChanged := TRUE;

  TSMainForm.TSGrid.ShowColumn( 'ProjectCode', (OpenType = 'GROUP') );
  TSMainForm.AppendProjectAction.Enabled := (OpenType <> 'GROUP'); 
  if not SameText( OpenType, 'GROUP' ) then
  begin
    TSMainForm.TSGrid.SetFocusToEnd;
    if ( TSMainForm.TSGrid.TSClientDataSet.RecordCount > 0 ) then
      CurrentProject.EndNotes := TSMainForm.TSGrid.ActiveTask.EndNotes;
  end;
end;

function IsXMLFile(const FileName: String): Boolean;
begin
  Result := ExtractFileExt(FileName) = '.xml';
end;

function XmlFileName(const FileName: String): String;
begin
  Result := ChangeFileExt(FileName, '.xml');
end;

procedure SaveXMLFile(FileName : String);
begin
  TsData.Save(XmlFileName(FileName));
end;

procedure ClearTSGProjects;
begin
  if not Assigned(TSGProjects) then
  begin
    TSGProjects := TStringList.Create;
    TSGProjects.Duplicates := dupIgnore;
    TSGProjects.CaseSensitive := False;
    TSGProjects.Sorted := True;
  end
  else
    TSGProjects.Clear;
end;

procedure AddTSGProject(ProjectCode: String);
begin
  if not Assigned(TSGProjects) then
    ClearTSGProjects;
  TSGProjects.Add(ProjectCode);
  FlagProjectAsChanged(ProjectCode);
end;

procedure OpenProjectGroup( FileName : String ) ;
{This procedure opens a Time Stamp Group file
}
var
  TSGFile     : TIniFile;
  i           : Integer;
  DataFileName: String;
  RelativePath: String;
  SectionList : TStringList;
  UseXmlDataFile : Boolean;
begin
  if not System.IO.File.Exists( FileName ) then
    Exit; // Silently, in case this is at startup

      // The passed in name should be .tsg
  if IsXmlFile(FileName) then
    raise Exception.CreateFmt('OpenProjectGroup: %s should be .TSG not .XML', [FileName]);

  UseXmlDataFile := System.IO.File.Exists(XmlFileName(FileName));

  TMUtils.ClearStringGrid('');
  CurrentProject := LoadProjectOptions( FileName );
  RelativePath   := IncludeTrailingBackSlash( ExtractFilePath( FileName ) );

  SectionList := TStringList.Create;
  TSGFile     := TINIFile.Create( FileName );
  try
    ClearTSGProjects;

    TSMainForm.TSGrid.BeginLoadFile;
    try
      { Load project codes (stored as section headers) into the lookup list }
      TSGFile.ReadSections( SectionList );

      { Remove the settings section }
      if SectionList.IndexOf( TSG_OPTIONS ) > -1 then
        SectionList.Delete( SectionList.IndexOf( TSG_OPTIONS ));

      for i := 0 to SectionList.Count - 1 do
        if TSGFile.ReadBool( SectionList[i], 'Active', False ) then
        begin
          DataFileName := ExpandedDataFileName( FileName, TSGFile.ReadString( SectionList[i], 'FileName', TXT_FILENOTFOUND ));
          TSGProjects.Add( SectionList[i] );

          // Old format - use TS2 files
          if not UseXmlDataFile then
            OpenDataFile( DataFileName, 'GROUP', 2, SectionList[i] );
        end;

      // New format - use .NET xml
      if UseXmlDataFile then
      begin
        TsData.LoadXMLFile(XmlFileName(FileName));

        // Read the settings which used to be read from each individual project's header file
        // Therefore for XML, we really need a second data table for each project details
        CurrentProject.ProjectName   := DefaultProject.ProjectName;
        CurrentProject.FileVersion    := DefaultProject.FileVersion;
        CurrentProject.DefaultReport  := DefaultProject.DefaultReport;
        CurrentProject.CurrencySymbol := DefaultProject.CurrencySymbol;
        if ( CurrencyString <> DefaultProject.CurrencySymbol ) then
          CurrencyString := DefaultProject.CurrencySymbol;

        TimeRecord.ProjectCode := CurrentProject.ProjectName;
        TSMainForm.RecordIsChanged := FALSE;
        TSMainForm.TSGrid.ShowColumn('ProjectCode', True);
        TSMainForm.AppendProjectAction.Enabled := False;
      end;

      TSMainForm.TSGrid.InitialiseGrid;   // Enables projectcode column if opened after ts2
      TSMainForm.TSGrid.IndexName := CurrentProject.IndexName;

      //TSMainForm.TSGrid.RecalculateCumulativeTime;
    finally
      TSMainForm.TSGrid.EndLoadFile;
    end;
  finally
    TSGFile.Free;
    SectionList.Free;
  end;

  SetTimingState( csOpened );
  TSMainForm.UpdateTitle;
  TSMainForm.UpdateStatusPanel;
  TSMainForm.TSGrid.SetFocusToEnd;

  TSMainForm.TSGrid.EndUpdate;
end; {procedure OpenTimeRecordDataFile}


//*****************************************************************************

procedure OpenTimeRecordDataFile( FileName: String );
{This procedure opens a Time Record file}
begin
   TSMainForm.TsGrid.BeginUpdate;
   try
     TMUtils.ClearStringGrid('');

     if SameText( ExtractFileExt( FileName ), '.SAV' ) then
       TMUtils.OpenDataFile( FileName, 'OPEN', 1)
     else if SameText( ExtractFileExt( FileName ), '.SA2' ) then
       TMUtils.OpenDataFile( FileName, 'OPEN', 2)
     else if SameText( ExtractFileExt( FileName ), '.TSG' ) then
       TMUtils.OpenProjectGroup( FileName )
     else
       raise Exception.Create( FileName + ' is not a recognised TimeStamp file.' );

     SetTimingState( csStopped );
     SetCurrentFileName( FileName );
   finally
     TSMainForm.TsGrid.EndUpdate;
   end;
end; {procedure OpenTimeRecordDataFile}

//*****************************************************************************

procedure AppendTimeRecordDataFile;
{This procedure appends the tasks from a saved Time Record file to the current
 grid contents.  Component AppendFileDialog was added for this function to
 ensure that the state of the OpenFileDialog.FileName was not changed.}
begin
  if TSMainForm.AppendFileDialog.Execute then
     begin
          if UpperCase(trim(ExtractFileExt(TSMainForm.AppendFileDialog.FileName))) = '.SAV' then
             begin
                  TMUtils.OpenDataFile(TSMainForm.AppendFileDialog.FileName, 'APPEND', 1);
             end {if UpperCase}
          else
              if UpperCase(trim(ExtractFileExt(TSMainForm.AppendFileDialog.FileName))) = '.SA2' then
                 begin
                      TMUtils.OpenDataFile(TSMainForm.AppendFileDialog.FileName, 'APPEND', 2);
                 end {if UpperCase}
              else
                  Borland.Vcl.Dialogs.MessageDlg('The last-saved file has an invalid extension.',mtError,[mbOK],0);
     end; {if TSMainForm.AppendFileDialog.Execute}
end; {procedure AppendTimeRecordDataFile}

//*****************************************************************************

procedure SetStartTimerGUI;
{This procedure initializes the states for all menu items and ToolButtons when
 a new timer row is started.}
begin
  {Menu Items}
  TSMainForm.StartSlackTimerTimersMenuItem.Enabled  := TRUE;
  TSMainForm.StopTimerTimersMenuItem.Enabled        := TRUE;
  TSMainForm.StartTimerTimersMenuItem.Enabled       := FALSE;
  TSMainForm.OpenProjectFileMenuItem.Enabled        := FALSE;
  TSMainForm.SaveProjectFileMenuItem.Enabled        := FALSE;
  TSMainForm.NewProjectFileMenuItem.Enabled         := FALSE;
  TSMainForm.DeleteSelectedTaskEditMenuItem.Enabled := FALSE;

  {Tray Item Popup Menu Items}
  TSMainForm.StartSlackTimerTrayMenuItem.Enabled   := TRUE;
  TSMainForm.StopTimerTrayMenuItem.Enabled         := TRUE;
  TSMainForm.StartTimerTrayMenuItem.Enabled        := FALSE;
//zz     TSMainForm.ApplicationRxTrayIcon.Icon            := TSMainForm.ApplicationRxTrayIcon.Icons[1];

  {Speed Buttons}
  TSMainForm.SlackTimeToolButton.Enabled    := TRUE;
  TSMainForm.LoadFileToolButton.Enabled     := FALSE;
  TSMainForm.SaveFileToolButton.Enabled     := FALSE;
  TSMainForm.NewLogToolButton.Enabled       := FALSE;
  TSMainForm.TaskDeleteToolButton.Enabled   := FALSE;
  TSMainForm.TaskEditToolButton.Enabled     := FALSE;
end; {procedure SetStartTimerGUI}

//*****************************************************************************

procedure SetMainFormProperties;
begin
  if Assigned( TSMainForm ) then
  begin
    TSMainForm.Caption  := TMUtils.ApplicationTitle;
    TimingRecord.ContinueTime := FALSE;
  end;
end; {procedure SetMainFormProperties}


//*****************************************************************************

procedure ClearStringGrid(ClearType : string);
{This procedure clears the string grid when a new timing project
 is started, as well as clear the memory record array.}
begin
  TSMainForm.TSGrid.Clear;

  if ClearType <> 'KeepMemoryRecords' then
  begin
     CurrentProject.ProjectName := '';
     TSMainForm.UpdateTitle;
     TSMainForm.UpdateStatusPanel;
     TSData.DatasetInitialised := False;
  end;
end; {procedure ClearStringGrid}

//*****************************************************************************

function GetINIFilePath : string;
{This function returns the path of the INI file location by determining the
 path of the executed EXE. TimeStamp_Net.ini}
var
  EXEPath : string;
  INIPath : string;
begin
  EXEPath := Application.ExeName;
  INIPath := Borland.Vcl.SysUtils.ChangeFileExt(EXEPath, '.INI');
  Result  := INIPath;
end; {GetINIFilePath}

//*****************************************************************************

function GetHelpFilePath : string;
{This function returns the path of the HLP file location by determining the
 path of the executed EXE.}
var
  HLPPath : string;
begin
  HLPPath := ExtractFilePath(Application.ExeName) + 'TIMESTAMP.HLP';
  Result  := HLPPath;
end; {GetHLPFilePath}

//*****************************************************************************

function GetAutoFillFilePath : string;
{This function returns the path of the auto fill file location by determining the
 path of the executed EXE.}
var
  NOTPath : string;
begin
  NOTPath := ExtractFilePath( Application.ExeName ) + 'TIMESTAMP.NOT';
  Result  := NOTPath;
end; {GetAutoFillFilePath}

//*****************************************************************************

function CalculateTimeDifference (Time1, Time2 : TDateTime) : TDateTime;
{This function calculates the delta between two passed times}
begin
  Result := Abs( Time2 - Time1 );
end; {function CalculateTimeDifference}

//*****************************************************************************

procedure VerifyFileSavedStatus;
{This procedure checks to see if the file saved status flag indicates changed
 data since the last save.  If data needs to be saved, it prompts the user
 to see if they would like to save it before proceding.}
var
  CloseModalResult : TModalResult;
begin
  TMMain.ResumeClose := TRUE;

  if TSMainForm.RecordIsChanged = TRUE then
    begin
      {Allow adjustment for "Always On Top"}
      TMUtils.AdjustAlwaysOnTop(1);

      CloseModalResult := Borland.Vcl.Dialogs.MessageDlg('Time Stamp data has changed.  Save now?',
         mtWarning, [mbYes, mbNo, mbCancel], 0);

      if CloseModalResult = mrYes then
         begin
              if (TSMainForm.OpenFileDialog.FileName > '') then
                 begin
                      TMUtils.SaveTimeRecordDataFile( TSMainForm.OpenFileDialog.FileName );
                 end
              else
                 begin
                      TSMainForm.SaveFileDialog.FileName := TSMainForm.OpenFileDialog.FileName;
                      TMUtils.SaveTimeRecordDataFileAs;
                 end; {if (TSMainForm.OpenFileDialog.FileName}
         end;

      if CloseModalResult = mrCancel then {Do not go through with close}
         begin
              TMMain.ResumeClose := FALSE;
         end;

      {Restore "Always On Top" setting}
      TMUtils.AdjustAlwaysOnTop(0);
    end; {if TSMainForm.RecordIsChanged}
end; {procedure VerifyFileSavedStatus}

//*****************************************************************************

procedure ExportTimeRecord( ExportTextFile: TextFile; CurrentTimeRecord: TimeRecord_SA2; UseDecimalSummary: Boolean );
{ Exports a time record to a TAB-delimited ASCII file.
}
var
  WorkTimeCost: Double;
begin
  {Write Task Number}
  Write(ExportTextFile, IntToStr(trunc(CurrentTimeRecord.Task_Num)) + Chr(09));
  Write(ExportTextFile, CurrentTimeRecord.ProjectCode + Chr(09));

  {Calculate work cost for display purposes since it is not
   stored in the data file}
  WorkTimeCost  := (CurrentTimeRecord.WorkTime * 24)  * CurrentTimeRecord.HourlyRate;

  {Write data values}
  Write(ExportTextFile, FormatDateTime('ddddd h:mm:ss', CurrentTimeRecord.StartTime) + Chr(09));
  Write(ExportTextFile, FormatDateTime('ddddd h:mm:ss', CurrentTimeRecord.EndTime) + Chr(09));

  Write(ExportTextFile, FormatSummaryTime( CurrentTimeRecord.DiffTime,  UseDecimalSummary ) + Chr(09));
  Write(ExportTextFile, FormatSummaryTime( CurrentTimeRecord.SlackTime, UseDecimalSummary ) + Chr(09));
  Write(ExportTextFile, FormatSummaryTime( CurrentTimeRecord.WorkTime,  UseDecimalSummary ) + Chr(09));
  Write(ExportTextFile, FormatSummaryTime( CurrentTimeRecord.OverTime,  UseDecimalSummary ) + Chr(09));

  Write(ExportTextFile, FloatToStr(CurrentTimeRecord.HourlyRate) + Chr(09));
  Write(ExportTextFile, FloatToStrF(WorkTimeCost, ffCurrency, 5, 2) + Chr(09));  // Bug. Euro symbol gets munged in .net

  {Replace carriage returns with a semicolon so multi-line notes fields
   will stay within the line}
  StringReplace( CurrentTimeRecord.Notes, #13#10, ';', [rfReplaceAll] );

  Write(ExportTextFile, CurrentTimeRecord.Notes);
  Writeln(ExportTextFile);
end;

procedure ExportTimeRecordDataFile;
{This procedure exports a time log grid to a TAB-delimited ASCII file.  No summary
 info prints yet, and it only exports what it's told to... it does not change
 automatically if the record types change.}
var
   ExportTextFile     : textfile;
   DecimalSummaryInfo : string;
   INISettingsFile    : TIniFile;
begin
  if TSMainForm.ExportFileDialog.Execute then
    begin
         {Write saved file name to INI File}
         INISettingsFile   := TINIFile.Create(TMUtils.GetINIFilePath);
         try
           {See if user prefers summary time info in decimal format}
           DecimalSummaryInfo := INISettingsFile.ReadString('Options','DecimalSummary','Error');
         finally
           INISettingsFile.Free;
         end;

         {Get file handle}
         AssignFile(ExportTextFile, TSMainForm.ExportFileDialog.FileName);
         Rewrite(ExportTextFile);

         {Write Column Titles}
         Write(ExportTextFile, 'Task Number' + Chr(09));
         Write(ExportTextFile, 'Project' + Chr(09));
         Write(ExportTextFile, 'Start Time' + Chr(09));
         Write(ExportTextFile, 'End Time' + Chr(09));

         {Change labels to hours if user specifies decimal output}
         if (DecimalSummaryInfo = '1') then
            begin
                 Write(ExportTextFile, 'Total Time Hours' + Chr(09));
                 Write(ExportTextFile, 'Slack Time Hours' + Chr(09));
                 Write(ExportTextFile, 'Work Time Hours' + Chr(09));
                 Write(ExportTextFile, 'Overtime Hours' + Chr(09));
            end
         else
            begin
                 Write(ExportTextFile, 'Total Time' + Chr(09));
                 Write(ExportTextFile, 'Slack Time' + Chr(09));
                 Write(ExportTextFile, 'Work Time' + Chr(09));
                 Write(ExportTextFile, 'Overtime' + Chr(09));
            end; {if DecimalSummaryInfo}

         Write(ExportTextFile, 'Hourly Rate' + Chr(09));
         Write(ExportTextFile, 'Work Cost' + Chr(09));
         Write(ExportTextFile, 'Notes');
         Writeln(ExportTextFile);

         {Write TAB-delimited data}
         TsData.BeginUpdate(True);
         try
           TsData.First;
           while not TsData.Eof do
             begin
               ExportTimeRecord( ExportTextFile, TSMainForm.TSGrid.ActiveTask, SameText( DecimalSummaryInfo, '1' ) );
               TsData.Next;
             end;
         finally
           TsData.EndUpdate;
         end;

         CloseFile(ExportTextFile);
    end; {if TSMainForm.ExportFileDialog.Execute}
end; {procedure ExportTimeRecordDataFile}

//*****************************************************************************

function FirstNoteLine( Note: String ): String;
{ Return the first line from any notes
}
var
  iEOLN : Integer;
begin
  iEOLN := Pos( #13, Note );
  if iEOLN = 0 then
    iEOLN := Pos( #10, Note );
  if iEOLN = 0 then
    Result := Note
  else
    Result := Copy( Note, 1, iEOLN-1 );
end;

function FormatTimeDelta (InputTime : TDateTime) : string;
{This function formats a TDateTime variable into hh:mm:ss, but allows Hours
 to be over 24, which FormatDateTime does not do... it increments the Day
 when Hours is over 24.

 Whoa. How many hours do you sit at the computer for, working on a project non-stop? 8-P
}
var
  Hours,
  Minutes,
  Seconds : word;
  HoursString,
  MinutesString,
  SecondsString : string;
  ProductionInputTime : TDateTime;
begin
  {Calculate integer values for hours, minutes, seconds}
  ProductionInputTime := InputTime;
  ProductionInputTime := ProductionInputTime * 24;
  Hours := Trunc(ProductionInputTime);
  ProductionInputTime := ProductionInputTime - Hours;

  Minutes := Trunc(ProductionInputTime * 60);
  ProductionInputTime := (ProductionInputTime * 60) - Minutes;

  Seconds := Trunc(ProductionInputTime * 60);

  HoursString := IntToStr(Hours);

  MinutesString := IntToStr(Minutes);
  if ( Length(MinutesString) = 1 ) and ( Hours > 0 ) then
    MinutesString := '0' + MinutesString;

  SecondsString := IntToStr(Seconds);
  if ( Length(SecondsString) = 1 ) and (( Hours > 0 ) or ( Minutes > 0 )) then
    SecondsString := '0' + SecondsString;

  {Set final string}
  if Hours > 0 then
    Result := HoursString + 'h ' + MinutesString + 'm'
  else if Minutes > 0 then
    Result := MinutesString + 'm'
  else if ( Seconds = 0 ) then
    Result := ''
  else
    Result := SecondsString + 'sec';
end;


function FormatTimeTotal (InputTime : TDateTime) : string;
{This function formats a TDateTime variable into hh:mm:ss, but allows Hours
 to be over 24, which FormatDateTime does not do... it increments the Day
 when Hours is over 24.
}
var
  Hours,
  Minutes,
  Seconds : word;
  HoursString,
  MinutesString,
  SecondsString : string;
  ProductionInputTime : TDateTime;
begin
  if InputTime = 0 then Result := '00:00:00' else
    begin
      {Calculate integer values for hours, minutes, seconds}
      ProductionInputTime := InputTime;
      ProductionInputTime := ProductionInputTime * 24;
      Hours := Trunc(ProductionInputTime);
      ProductionInputTime := ProductionInputTime - Hours;

      Minutes := Trunc(ProductionInputTime * 60);
      ProductionInputTime := (ProductionInputTime * 60) - Minutes;

      Seconds := Trunc(ProductionInputTime * 60);

      HoursString := IntToStr(Hours);

      {Set string to two digits if only one exists for looks}
      MinutesString := IntToStr(Minutes);
      if Length(MinutesString) = 1 then MinutesString := '0' + MinutesString;

      SecondsString := IntToStr(Seconds);
      if Length(SecondsString) = 1 then SecondsString := '0' + SecondsString;

      {Set final string}
      Result := HoursString + ':' + MinutesString + ':' + SecondsString;
    end; {if InputTime = 0}
end; {function FormatTimeTotal}

function FormatSummaryTime(InputTime : TDateTime; bDecimalSummary: Boolean = False) : string;
{ This function formats a TDateTime variable into hh:mm:ss, and applies the user
  preference for decimal summary format where appropriate
}
begin
  if bDecimalSummary then
    Result := FloatToStrF((InputTime * 24), ffFixed, 15, 2)
  else
    Result := FormatTimeDelta( InputTime ); //FormatDateTime('hh:mm:ss', InputTime );  <- FormatTimeTotal
end;

//*****************************************************************************

procedure SavePreferedLayout;
{ This procedure saves the form dimensions, location, and grid column widths
  to the INI file. }
var
  i               : Integer;
  INISettingsFile : TIniFile;
begin
  {Get INI File settings}
  INISettingsFile := TINIFile.Create(TMUtils.GetINIFilePath);

  {Save the main form's dimensions}
  INISettingsFile.WriteString('FormDimensions','Top',IntToStr(TSMainForm.Top));
  INISettingsFile.WriteString('FormDimensions','Left',IntToStr(TSMainForm.Left));
  INISettingsFile.WriteString('FormDimensions','Width',IntToStr(TSMainForm.Width));
  INISettingsFile.WriteString('FormDimensions','Height',IntToStr(TSMainForm.Height));

  { Store column widths (where they are altered) }
  if Assigned( TSMainForm.TSGrid ) then
    for i := 0 to TSMainForm.TSGrid.TaskDBGrid.Columns.Count - 1 do
      //if not TSMainForm.TSGrid.TaskDBGrid.Columns[i].Autosize then
        IniSettingsFile.WriteInteger( SEC_COLUMNWIDTHS,
                                  TSMainForm.TSGrid.TaskDBGrid.Columns[i].FieldName,
                                  TSMainForm.TSGrid.TaskDBGrid.Columns[i].Width );

  INISettingsFile.WriteString('FormDimensions','MainToolBar',IntToStr(TSMainForm.MainToolBar.Left));

  {Free the INI File}
  INISettingsFile.Free;
end;

//*****************************************************************************

procedure LoadPreferedLayout;
{ This procedure loads the form dimensions, location, and grid column widths
  from the INI file, applying defaults if none are found. }
var
  INISettingsFile   : TIniFile;
begin
  {Get INI File settings}
  INISettingsFile := TINIFile.Create(TMUtils.GetINIFilePath);

  {Load the main form's dimensions}
  TSMainForm.Top    := INISettingsFile.ReadInteger( 'FormDimensions', 'Top',   5 );
  TSMainForm.Left   := INISettingsFile.Readinteger( 'FormDimensions', 'Left',  5 );
  TSMainForm.Width  := INISettingsFile.ReadInteger( 'FormDimensions', 'Width', TSMainForm.Width );
  TSMainForm.Height := INISettingsFile.ReadInteger( 'FormDimensions', 'Height',TSMainForm.Height );
  {Going from dual to single monitor / laptop switch}
  if ((TSMainForm.Left + TSMainForm.Width) > Screen.Width) then
  begin
    TSMainForm.Left := 0;
    TSMainForm.Width := Math.Min(Screen.Width, INISettingsFile.ReadInteger( 'FormDimensions', 'Width', TSMainForm.Width));
  end;

  {Load Main Control Bar location}
  TSMainForm.MainToolBar.Left := StrToInt(INISettingsFile.ReadString('FormDimensions','MainToolBar','0'));

  {Free the INI File}
  INISettingsFile.Free;
end;

//*****************************************************************************

procedure  AdjustAlwaysOnTop (StartToggle : word);
{This procedure will take Time Stamp's main form out of "Always On Top" mode
 temporarily to allow other dialogs to appear.}
begin
  if StartToggle = 1 then
     begin
      {If Always On Top is on, turn off temporarily so we can show Notes dialog}
      if OnTopToggle then
         begin
              TSMainForm.FormStyle := fsNormal;
         end; {if OnTopToggle = TRUE}
     end
  else
     begin
        {If Always On Top is on, restore it}
        if OnTopToggle then
          TSMainForm.FormStyle := fsStayOnTop;
     end;
end; {procedure  AdjustAlwaysOnTop}

{ TimeRecord_SA2 }

procedure TimeRecord_SA2.Assign(Src: TimeRecord_SA2);
{ Copy all fields, including the Guid
}
begin
  CloneAsNew(Src, Src.StartTime);
  pkGUID      := Src.pkGUID;
  ProjectCode := Src.ProjectCode;
  StartTime   := Src.StartTime;
  EndTime     := Src.EndTime;
  WorkTime    := Src.WorkTime;
  SlackTime   := Src.SlackTime;
  DiffTime    := Src.DiffTime;
  OverTime    := Src.OverTime;
end;

procedure TimeRecord_SA2.Clear;
{ Reset the contents of the timerecord; allocate a new GUID
}
var
  EmptyTimeRecord : TimeRecord_SA2;
begin
  Self := EmptyTimeRecord; // The ".NET way" to clear a structure
  Self.CreateNewGuid;
  Self.HourlyRate := CurrentProject.HourlyRate;
end;

procedure TimeRecord_SA2.CloneAsNew(Src: TimeRecord_SA2; NewStartTime: TDateTime);
{ Clone enough fields to start a new record with identical settings
}
begin
  ClearTimeRecord(Self);
  StartTime  := NewStartTime;
  ProjectCode:= Src.ProjectCode;
  HourlyRate := Src.HourlyRate;
  Task_Num   := Src.Task_Num;
  Notes      := Src.Notes;
  EndNotes   := Src.EndNotes;
  Unused2    := Src.Unused2;
  Unused3    := Src.Unused3;
  Unused5    := Src.Unused5;
  Unused6    := Src.Unused6;
end;

procedure TimeRecord_SA2.CreateNewGuid;
var
  tmpGUID: GUID;
begin
  if ( CreateGUID( tmpGUID ) = 0 ) then
    pkGUID := GuidToString( tmpGUID )
  else
    raise Exception.Create( 'Could not allocate GUID for record' );
end;

function TimeRecord_SA2.DayNumber: Integer;
begin
  Result := Trunc(Double(StartTime));
end;

function TimeRecord_SA2.DayNumberStr: String;
begin
  Result := IntToStr(DayNumber);
end;

function TimeRecord_SA2.DayProject: string;
{ Used for aggregates, hours worked in project in this day
}
begin
  Result := DayNumberStr + ';' + ProjectCode;
end;

function TimeRecord_SA2.IsValid: Boolean;
begin
  Result := DayNumber > 0;
end;

{ TTimingRecord }

procedure TTimingRecord.StartNewTiming;
begin
  ClearTimeRecord(Self.TimeRecord);
  TimeRecord.StartTime := now;
  SessionStartTime     := now;
end;

procedure TTimingRecord.StartTiming(Ts2: TimeRecord_SA2);
begin
  TimeRecord := Ts2;
  SessionStartTime := now;
end;

initialization
finalization
  if Assigned( TSGChangedProjects ) then
    TSGChangedProjects.Free;

end.
