{
Application:    Time Stamp, Copyright 1997 William Rouck
Module:         TMAbout.pas
Contact:        email: wrouck@syntap.com
Modifications:  JM Holloway, June 2004
                justin@schiznit.net


This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version. This
program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details. You should have
received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
02111-1307, USA.
}

unit TMAbout;
{This form is the About Box for the application.}

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, ShellAPI,
     StdCtrls, Buttons, ExtCtrls, System.ComponentModel, System.Diagnostics;

type
  TAboutBoxDialog = class(TForm)
    Panel1: TPanel;
    ProgramIcon: TImage;
    ProductName: TLabel;
    Version: TLabel;
    Copyright: TLabel;
    Comments: TLabel;
    NameLabel: TLabel;
    Label2: TLabel;
    TimeStampURLLabel: TLabel;
    ProgVersionLabel: TLabel;
    Image1: TImage;
    OKButton: TButton;
    Label3: TLabel;
    Label4: TLabel;
    procedure TimeStampURLLabelClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure NameLabelClick(Sender: TObject);
  end;

implementation

uses TMDevIn;

{$R *.NFM}

//*****************************************************************************

procedure TAboutBoxDialog.TimeStampURLLabelClick(Sender: TObject);
{ This procedure launches the default browser when the label caption
  (masked by properties to make it look like a hyperlink) is
  clicked. }
begin
  ShellExecute(Application.Handle,'open', TimeStampURLLabel.Caption, '', '', SW_NORMAL);
end; {procedure TAboutBoxDialog.TimeStampURLLabelClick}

//*****************************************************************************

procedure TAboutBoxDialog.FormCreate(Sender: TObject);
{ This procedure initializes settings when the form is created. }
var
  VersionInfoStr : String;
begin
  with System.Diagnostics.FileVersionInfo.GetVersionInfo( Application.ExeName ) do
    VersionInfoStr := Format( '%d.%d.%d-%d',
                              [ FileMajorPart, FileMinorPart, FileBuildPart, FilePrivatePart] );

   {Set Help context to Version History so user can hit F1 and get
    this particular Help page.}
   Self.HelpContext := 2;
   ProgVersionLabel.Caption := VersionInfoStr;
end; {procedure TAboutBoxDialog.FormCreate}

//*****************************************************************************

procedure TAboutBoxDialog.NameLabelClick(Sender: TObject);
begin
  with TAboutDeveloperForm.Create( Self ) do
  try
    ShowModal;
  finally
    Free;
  end;
end;

//*****************************************************************************

end.

