{
Application:    Time Stamp, Copyright 1997 William Rouck
Module:         TMEdit.pas
Contact:        email: wrouck@syntap.com
Modifications:  JM Holloway, June 2004
                justin@schiznit.net


This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version. This
program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details. You should have
received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
02111-1307, USA.
}

unit TMEdit;

{ This unit houses the Task Record Edit facility. }

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, Mask, ComCtrls, System.ComponentModel, TMUtils;

type
  TTaskEditDialog = class(TForm)
    Panel1: TPanel;
    OKButton: TButton;
    CancelButton: TButton;
    PreviousTask: TGroupBox;
    PreviousProjectComboBox: TComboBox;
    GroupBox2: TGroupBox;
    PreviousStartDateDateTimePicker: TDateTimePicker;
    PreviousStartTimeDateTimePicker: TDateTimePicker;
    GroupBox5: TGroupBox;
    PreviousEndDateDateTimePicker: TDateTimePicker;
    PreviousEndTimeDateTimePicker: TDateTimePicker;
    PreviousNoteMemo: TMemo;
    CurrentTaskGroupBox: TGroupBox;
    ProjectComboBox: TComboBox;
    GroupBox3: TGroupBox;
    StartDateDateTimePicker: TDateTimePicker;
    StartTimeDateTimePicker: TDateTimePicker;
    TaskEndGroupBox: TGroupBox;
    EndDateDateTimePicker: TDateTimePicker;
    EndTimeDateTimePicker: TDateTimePicker;
    GroupBox4: TGroupBox;
    Label1: TLabel;
    AutoFillComboBox: TComboBox;
    NoteMemo: TMemo;
    GroupBox6: TGroupBox;
    Label7: TLabel;
    Label8: TLabel;
    Label2: TLabel;
    HourlyRateEdit: TEdit;
    SlackTimeEdit: TEdit;
    WorkTimeEdit: TEdit;
    WorkTimeTrackBar: TTrackBar;
    Button1: TButton;
    WorkTimeErrorLabel: TLabel;
    procedure FormShow(Sender: TObject);
    procedure OKButtonClick(Sender: TObject);
    procedure AutoFillComboBoxChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure PreviousProjectComboBoxDblClick(Sender: TObject);
    procedure PreviousEndDateDateTimePickerDblClick(Sender: TObject);
    procedure PreviousNoteMemoDblClick(Sender: TObject);
    procedure GroupBox5DblClick(Sender: TObject);
  private
    FIsTimingRecord : Boolean;
    FCurrentRecord: TimeRecord_SA2;
    FPreviousRecord: TimeRecord_SA2;
    function  RecalculateWorkTime(AShowDialog: Boolean): Boolean;
    procedure UpdateWorkTimeTrackBarPosition;
    procedure UpdateIsTimingColours(IsTimingRecord: Boolean);
    procedure UpdateCurrentTaskDisplay;
    function GuiPreviousEndDateTime: TDateTime;
  public
    StartTime  : TDateTime;
    EndTime    : TDateTime;
    WorkTime   : TDateTime;
    SlackTime  : TDateTime;
    DiffTime   : TDateTime; {Calculates to total time work + slack}
    HourlyRate : double;
    Note       : string[255];
    function Execute(var ATimeRecord, APreviousRecord: TimeRecord_SA2; IsTimingRecord: Boolean = False ) : boolean;
  end;


implementation

uses
  DateUtils,
  //
  TMMain;

{$R *.NFM}

//*****************************************************************************

procedure TTaskEditDialog.GroupBox5DblClick(Sender: TObject);
begin
  FCurrentRecord.StartTime := GuiPreviousEndDateTime;
  UpdateCurrentTaskDisplay;
end;

function TTaskEditDialog.GuiPreviousEndDateTime: TDateTime;
var
  EndTime: Double;
begin
  EndTime := Int(PreviousEndDateDateTimePicker.Date) + Frac(PreviousEndTimeDateTimePicker.Time);
  Result := EndTime;
end;

procedure TTaskEditDialog.UpdateWorkTimeTrackBarPosition;
begin
  // 8 hours = 32 x 15 minute increments on the trackbar
  if (HoursBetween(FCurrentRecord.EndTime, FCurrentRecord.StartTime) >= 8) then
    WorkTimeTrackbar.Position := WorkTimeTrackbar.Max
  else if (MinutesBetween(FCurrentRecord.EndTime, FCurrentRecord.StartTime) <= 15) then
    WorkTimeTrackbar.Position := WorkTimeTrackbar.Min
  else
    WorkTimeTrackbar.Position := Round(Abs(MinutesBetween(FCurrentRecord.EndTime, FCurrentRecord.StartTime)) / 15);
end;

procedure TTaskEditDialog.UpdateIsTimingColours(IsTimingRecord: Boolean);
begin
  FIsTimingRecord         := IsTimingRecord and ( TMUtils.CurrentState in tsTiming );
  TaskEndGroupBox.Enabled := not FIsTimingRecord;
  if FIsTimingRecord then
  begin
    EndDateDateTimePicker.Color := TMUtils.TimingStatus[TMUtils.CurrentState].BackgroundColor;
    EndTimeDateTimePicker.Color := TMUtils.TimingStatus[TMUtils.CurrentState].BackgroundColor;
    if ( CurrentState = csTiming ) then
      WorkTimeEdit.Color  := TMUtils.TimingStatus[TMUtils.CurrentState].BackgroundColor
    else
      SlackTimeEdit.Color := TMUtils.TimingStatus[TMUtils.CurrentState].BackgroundColor;
  end
  else begin
    EndDateDateTimePicker.Color := clWindow;
    EndTimeDateTimePicker.Color := clWindow;
    SlackTimeEdit.Color         := clWindow;
    WorkTimeEdit.Color          := clWindow;
  end;
end;

function TTaskEditDialog.Execute(var ATimeRecord, APreviousRecord: TimeRecord_SA2; IsTimingRecord: Boolean = False) : boolean;
{ Show the form populated with the values for the passed record. Return true if the user
  selected OK to save the record.

  Although it is possible to use data-aware controls here, this format holds to
  the original implementation.
}
begin
  FPreviousRecord.Assign(APreviousRecord);
  FCurrentRecord.Assign(ATimeRecord);

  if APreviousRecord.IsValid then
  begin
    PreviousProjectComboBox.ItemIndex        := PreviousProjectComboBox.Items.IndexOf( APreviousRecord.ProjectCode );
    PreviousNoteMemo.Text                    := APreviousRecord.Notes;
    PreviousStartDateDateTimePicker.DateTime := APreviousRecord.StartTime;
    PreviousStartTimeDateTimePicker.DateTime := APreviousRecord.StartTime;
    PreviousEndDateDateTimePicker.DateTime   := APreviousRecord.EndTime;
    PreviousEndTimeDateTimePicker.DateTime   := APreviousRecord.EndTime;
  end;

  UpdateCurrentTaskDisplay;
  UpdateIsTimingColours(IsTimingRecord);

  ActiveControl := NoteMemo;
  Self.ShowModal;

  // If the user is saving, copy the updated values from the screen into the record.
  if Self.ModalResult = mrOK then
     begin
        ATimeRecord.Assign(FCurrentRecord);
        
        ATimeRecord.StartTime := Int(StartDateDateTimePicker.Date) + Frac(StartTimeDateTimePicker.Time);
        ATimeRecord.EndTime   := Int(EndDateDateTimePicker.Date) + Frac(EndTimeDateTimePicker.Time);

        if ( ATimeRecord.EndTime < ATimeRecord.StartTime ) then
        begin
          MessageDlg('New time / date value was invalid.  Please re-edit.', mtError, [mbOk], 0);
          Result := FALSE;
          exit;
        end; {if DiffTime}

        ATimeRecord.Notes      := NoteMemo.Text;
        HourlyRateEdit.Text    := StringReplace( HourlyRateEdit.Text, CurrentProject.CurrencySymbol, '', [] );
        ATimeRecord.HourlyRate := StrToCurrDef ( HourlyRateEdit.Text, 0.00 );
        ATimeRecord.WorkTime   := StrToFloatDef( WorkTimeEdit.Text, 0 ) / 1440;
        ATimeRecord.SlackTime  := StrToFloatDef( SlackTimeEdit.Text, 0 ) / 1440;
        ATimeRecord.DiffTime   := ATimeRecord.WorkTime + ATimeRecord.SlackTime;

        if CurrentProject.IsProjectGroup then
          ATimeRecord.ProjectCode := ProjectComboBox.Text;

        Result := TRUE;
     end
  else
    Result := FALSE;
end;

procedure TTaskEditDialog.UpdateCurrentTaskDisplay;
begin
  {Fill Edit Dialog values}
  ProjectComboBox.ItemIndex       := ProjectComboBox.Items.IndexOf(FCurrentRecord.ProjectCode);
  NoteMemo.Text                   := FCurrentRecord.Notes;
  HourlyRateEdit.Text             := FloatToStrF(FCurrentRecord.HourlyRate, ffCurrency, 5, 2);
  StartDateDateTimePicker.DateTime:= FCurrentRecord.StartTime;
  StartTimeDateTimePicker.DateTime:= FCurrentRecord.StartTime;
  EndDateDateTimePicker.DateTime  := FCurrentRecord.EndTime;
  EndTimeDateTimePicker.DateTime  := FCurrentRecord.EndTime;
  WorkTimeEdit.Text               := FloatToStrF((FCurrentRecord.WorkTime * 1440), ffFixed, 5, 2);
  SlackTimeEdit.Text              := FloatToStrF((FCurrentRecord.SlackTime * 1440), ffFixed, 5, 2);

  RecalculateWorkTime(False);
  UpdateWorkTimeTrackBarPosition;
end; {function TTaskEditDialog.Execute}

//*****************************************************************************

function TTaskEditDialog.RecalculateWorkTime(AShowDialog: Boolean): Boolean;
{This procedure provides some validation when the "Recalculate Time" button is hit.}
var
  WorkTimeCalc    : double;
  SlackTimeValue  : double;
  ErrorText : String;
begin
  if FIsTimingRecord then
  begin
    EndDateDateTimePicker.DateTime   := now;
    EndTimeDateTimePicker.DateTime   := now;
  end;

  ErrorText := '';
  if Int(EndDateDateTimePicker.Date) > Int(StartDateDateTimePicker.Date) then
    ErrorText := 'The Start and End dates appear to be different days.  This ' + Chr(10) +
                          'will result in a very large figure for Minutes.  Do you wish '+ Chr(10) +
                          'to continue with this calculation?';

  if ErrorText = '' then
  begin
    SlackTimeValue := StrToFloatDef( SlackTimeEdit.Text, 0 );

    WorkTimeCalc := (
                     (Int(EndDateDateTimePicker.Date) + Frac(EndTimeDateTimePicker.Time))-
                     (Int(StartDateDateTimePicker.Date) + Frac(StartTimeDateTimePicker.Time))

                     - (SlackTimeValue / 1440)
                    )
                    * 1440;

    if WorkTimeCalc >= -0.00000000001 then {this takes into account binary 'double' representation that does not always mean that 0 = 0.00}
        WorkTimeEdit.Text := FloatToStrF( ABS(WorkTimeCalc), ffFixed, 5, 2 )
    else
      ErrorText := 'This erroneously calculates to a negative work time.' + Chr(13) +
                         'Please adjust your times to valid values or you will' + Chr(13) +
                         'corrupt your time values for this task and for' + Chr(13) +
                         'summary calculations.';
  end;

  WorkTimeErrorLabel.Caption := ErrorText;
  if AShowDialog and (ErrorText <> '') then
    MessageDlg(ErrorText, mtWarning, [mbOK], 0);

  Result := ErrorText = '';
end;

//*****************************************************************************

procedure TTaskEditDialog.FormShow(Sender: TObject);
var
  AutoFillNotesFile : textfile;
  DummyString       : string;
begin
  {Initialize AutoFill Combo Box}
  AutoFillComboBox.Enabled := TRUE;
  AutoFillComboBox.Items.Clear;

  if FileExists(TMUtils.GetAutoFillFilePath) then
     begin
          AssignFile(AutoFillNotesFile, TMUtils.GetAutoFillFilePath);
          Reset(AutoFillNotesFile);

          while not eof(AutoFillNotesFile) do
                begin
                     ReadLn(AutoFillNotesFile, DummyString);
                     AutoFillComboBox.Items.Add(DummyString);
                     AutoFillComboBox.Enabled := TRUE;

                end;

          CloseFile(AutoFillNotesFile);
     end
  else
     begin
          AutoFillComboBox.Enabled := FALSE;
     end;
end; {procedure TTaskEditDialog.FormShow}

//*****************************************************************************

procedure TTaskEditDialog.OKButtonClick(Sender: TObject);
{ Save and close the form if there are no problems
}
begin
  if RecalculateWorkTime(True) then
    Self.ModalResult := mrOK;
end;

procedure TTaskEditDialog.PreviousProjectComboBoxDblClick(Sender: TObject);
begin
  FCurrentRecord.ProjectCode := (Sender as TComboBox).Text;
  UpdateCurrentTaskDisplay;
end;

procedure TTaskEditDialog.PreviousEndDateDateTimePickerDblClick(Sender: TObject);
begin
  FCurrentRecord.StartTime := GuiPreviousEndDateTime;
  UpdateCurrentTaskDisplay;
end;

procedure TTaskEditDialog.PreviousNoteMemoDblClick(Sender: TObject);
begin
  FCurrentRecord.Notes := PreviousNoteMemo.Text;
  UpdateCurrentTaskDisplay;
end;

{procedure TTaskEditDialog.OKButtonClick}

//*****************************************************************************

procedure TTaskEditDialog.AutoFillComboBoxChange(Sender: TObject);
begin
  NoteMemo.Lines.Add(AutoFillComboBox.Text);
end; {procedure TTaskEditDialog.AutoFillComboBoxChange}

//*****************************************************************************

procedure TTaskEditDialog.FormCreate(Sender: TObject);
begin
  if TMUtils.CurrentProject.IsProjectGroup then
  begin
    ProjectComboBox.Items.Assign( TMUtils.TSGProjects );
    PreviousProjectComboBox.Items.Assign( TMUtils.TSGProjects );
  end;
  ProjectComboBox.Visible := TMUtils.CurrentProject.IsProjectGroup;
  PreviousProjectComboBox.Visible := TMUtils.CurrentProject.IsProjectGroup;
end;

end.
