{ Compatibility wrapper class for a .NET datatable with VCL clientdataset routines
}
unit TMClientDataTable;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, System.ComponentModel, Borland.Vcl.Db, Borland.Vcl.ADODB,
  System.Data, // DataSet
  System.Data.SqlClient, // sqlXXX
  ADONETDb; // TADONETConnector;

const
  TS_DATATABLENAME = 'TimeStampDataTable';

type
  TClientDataTable = class(DataTable)
  private
    DT: DataTable;
    FActive: Boolean;
    FAggregatesActive: Boolean;
    FSchemaCreated: Boolean;
    function  FieldByName(const FieldName: String): TObject;
  public
    constructor Create;
    function  LoadFromFile(FileName: String): Boolean;
    procedure SaveToFile(const FileName: String);
    function  AddFieldDef(const Name: string; FieldType: TFieldType; Size: Integer; Required: Boolean): DataColumn;
    function  AddAggregate(const FieldName: String; FieldType: TFieldType; AExpression: String): DataColumn;
    function  AppendRecord(Args: array of const): DataRow;
    procedure SetActive(const Value: Boolean);
    function  ActiveRow: Integer;
    function  FieldByNameAsDateTime(const FieldName: String): TDateTime;
    function  FieldByNameAsFloat(const FieldName: String): Double;
    function  FieldByNameAsString(const FieldName: String): String;
    procedure SetPrimaryKey(const FieldName: String);
    procedure CreateSchema;
    property  Active: Boolean read FActive write SetActive;
    property  AggregatesActive: Boolean read FAggregatesActive write FAggregatesActive;
    property  SchemaCreated: Boolean read FSchemaCreated write FSchemaCreated;
  end;

implementation

uses
  TMUtils;

{ TClientDataTable }

function TClientDataTable.ActiveRow: Integer;
begin
  Result := DT.Rows.get_Count; // Probably a feature of the dataset, not the datatable
end;

function TClientDataTable.AddAggregate(const FieldName: String; FieldType: TFieldType; AExpression: String): DataColumn;
begin
  Result := AddFieldDef(FieldName, FieldType, 0, False);
  Result.Expression := AExpression;
end;

function TClientDataTable.AddFieldDef(const Name: string; FieldType: TFieldType; Size: Integer; Required: Boolean): DataColumn;
// AddFieldDef( 'StartTime',   ftDateTime, 0, False);
var
  DC: DataColumn;
  DataType: System.Type;
begin
  case FieldType of
    ftString, ftFixedChar, ftWideString, ftFixedWideChar, ftWideMemo, ftMemo, ftFmtMemo :
      DataType := System.Type.GetType('System.String');

    ftSmallint, ftInteger, ftWord, ftLargeint:
      DataType := System.Type.GetType('System.Int32');

    ftDate, ftTime, ftDateTime :
      DataType := System.Type.GetType('System.DateTime');

    ftBoolean:
      DataType := System.Type.GetType('System.Boolean');

    ftFloat:
      DataType := System.Type.GetType('System.Double');

    ftCurrency:
      DataType := System.Type.GetType('System.Decimal');
  else begin
    Assert(False, 'TClientDataTable.AddFieldDef unhandled fieldtype');
    DataType := System.Type.GetType('System.Int32');
    end;
  end;

  DC := DataColumn.Create(Name, DataType);
  DC.AllowDBNull := not Required;
  //DC.Unique := True; // Primary key,.,,

  DT.Columns.Add(DC);
  Result := DC;
end;

function TClientDataTable.AppendRecord(Args: array of const): DataRow;
var
  i: Integer;
begin
  Result := DT.NewRow;
  for i := Low(Args) to High(Args) do
  begin
    Result[i] := Args[i];
  end;

  DT.Rows.Add(Result);
  OutputDebugString('Dt.rows.count=' + IntToSTr(DT.Rows.get_Count));
end;

constructor TClientDataTable.Create;
begin
  inherited;
  FActive := False;
  DT := Self;
  DT.TableName := TS_DATATABLENAME;
end;

procedure TClientDataTable.CreateSchema;
begin
  Active := False;
  FSchemaCreated := False;

  AddFieldDef( 'StartTime',   ftDateTime, 0, False);
  AddFieldDef( 'EndTime',     ftDateTime, 0, False);
  AddFieldDef( 'DiffTime',    ftFloat,    0, False);
  AddFieldDef( 'WorkTime',    ftFloat,    0, False);
  AddFieldDef( 'SlackTime',   ftFloat,    0, False);
  AddFieldDef( 'OverTime',    ftFloat,    0, False);
  AddFieldDef( 'HourlyRate',  ftFloat,    0, False);
  AddFieldDef( 'Task_Num',    ftInteger,  0, False);
  AddFieldDef( 'Unused2',     ftInteger,  0, False);
  AddFieldDef( 'Unused3',     ftFloat,    0, False);
  AddFieldDef( 'Notes',       ftString,   255,  False);
  AddFieldDef( 'EndNotes',    ftString,   255,  False);
  AddFieldDef( 'Unused5',     ftString,   255,  False);
  AddFieldDef( 'Unused6',     ftString,   255,  False);

  AddFieldDef( 'pkGUID',      ftString,   50,   True);
  AddFieldDef( 'ProjectCode', ftString,   50,   False);
  AddFieldDef( 'DateNumber',  ftInteger,  0,    False);
  AddFieldDef( 'WorkCost',    ftFloat,    0,    False);
  AddFieldDef( 'CumulativeTime', ftFloat, 0,    False);
  AddFieldDef( 'CumulativeTimeDaySoFar', ftFloat, 0,    False);
  AddFieldDef( 'CumulativeTimeWeekSoFar', ftFloat, 0,    False);
  AddFieldDef( 'Duplicate',     ftBoolean,  0,    False);
  AddFieldDef( 'LastDayOfWeek', ftBoolean,  0,    False);

  // Aggregates
  AddAggregate( 'TotalTimeElapsed',      ftInteger, 'SUM(WorkTime) + SUM(SlackTime)' );
  AddAggregate( 'TotalSlackTimeElapsed', ftInteger, 'SUM(SlackTime)' );
  AddAggregate( 'TotalWorkTimeElapsed',  ftInteger, 'SUM(WorkTime)'  );

  // These appear to be too complex for the datatable expression :-(
  AddAggregate( 'TotalWorkTimeCost',     ftFloat,   'SUM(WorkTime) * (HourlyRate) * 24' );
  AddAggregate( 'TotalSlackTimeCost',    ftFloat,   'SUM(SlackTime) * (HourlyRate) * 24' );


  AddAggregate( 'AverageHourlyRate',     ftFloat,   'AVG( HourlyRate )' );
  AddAggregate( 'TotalOvertimeElapsed',  ftInteger, 'SUM( Overtime )' );

  AddAggregate( 'FirstDateTime',  ftDateTime, 'Min(StartTime)' );
  AddAggregate( 'LastDateTime',  ftDateTime, 'Max(StartTime)' );

  SetPrimaryKey('pkGUID');

  FSchemaCreated := True;
end;

function TClientDataTable.FieldByName(const FieldName: String): TObject;
var
  DR: DataRow;
begin
  // Get the last row
  DR := DT.Rows.Item[ActiveRow];

  // Get the column named as fieldnane
  Result := DR.Item[FieldName];
end;

function TClientDataTable.FieldByNameAsDateTime(const FieldName: String): TDateTime;
var
  DC: TObject;
begin
  DC := FieldByName(FieldName);
  if (DC.GetType <> System.Type.GetType('System.DateTime')) then
    raise Exception.Create(FieldName + ' is not a datetime');

  Result := TDateTime(DC);
end;

function TClientDataTable.FieldByNameAsFloat(const FieldName: String): Double;
begin
  Result := Double(FieldByName(FieldName));
end;


function TClientDataTable.FieldByNameAsString(const FieldName: String): String;
begin
  Result := String(FieldByName(FieldName));
end;

function TClientDataTable.LoadFromFile(FileName: String): Boolean;
begin
  // But don't yet know if loading the schema from the XML is correctly restoring all my settings/calc fields correctly
  Result := False;
  if FileExists(FileName) then
  begin
    DT.NameSpace := '';
    DT.TableName := TS_DATATABLENAME;
    if (DT.ReadXml(FileName) = XmlReadMode.ReadSchema) then
      Result := True;
  end
  else
    DebugMsg('TClientDataTable.LoadFromFile: "%s" not found', [FileName]);
end;

procedure TClientDataTable.SaveToFile(const FileName: String);
begin
  // The way this works, projects list could also be a data table - then
  // the DT.TableName also wraps each project row record in <DT.Project> field0=val; field1=val </project>
  //  <DT.TS2> field0=val; </DT.TS2>

  // Then use AdoDataset.SaveXmlFile and need to cast CDT as System.Data.DataTable)

  DebugMsg('TClientDataTable.SaveToFile: saving "%s"', [FileName]);
  DT.NameSpace := '';
  DT.TableName := TS_DATATABLENAME;
  DT.WriteXml(FileName, XmlWriteMode.WriteSchema);
end;

procedure TClientDataTable.SetActive(const Value: Boolean);
begin
  FActive := Value;
end;

procedure TClientDataTable.SetPrimaryKey(const FieldName: String);
var
  DC: array of DataColumn;
begin
  SetLength(DC, 1);
  DC[0] := Columns[FieldName];
  PrimaryKey := DC;
end;

end.
