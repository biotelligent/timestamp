{
Application:    Time Stamp, Copyright 1997 William Rouck
Module:         TMGroup.pas
Contact:        email: wrouck@syntap.com
Modifications:  JM Holloway, June 2004
                justin@schiznit.net

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version. This
program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details. You should have
received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
02111-1307, USA.
}

unit TMGroup;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Borland.Vcl.StdCtrls, System.ComponentModel,
  Borland.Vcl.CheckLst, Borland.Vcl.Buttons, Borland.Vcl.IniFiles;

type
  TGroupOptionsFrame = class;

  // In .NET, records can have methods. Much fun...
  TTSProjectItem = record
    ProjectCode : String;
    FileName    : String;
    Active      : Boolean;
    function Add( Sender: TGroupOptionsFrame; sProjectCode: String ): Integer;
  end;

  TGroupOptionsFrame = class(TFrame)
    GroupBox3: TGroupBox;
    CurrencySymbolEdit: TEdit;
    Label5: TLabel;
    ReportComboBox: TComboBox;
    GroupFilesGroupBox: TGroupBox;
    ProjectsCheckList: TCheckListBox;
    Label1: TLabel;
    GroupBox1: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    ProjectCodeEdit: TEdit;
    ProjectFileEdit: TEdit;
    DeleteProjectButton: TSpeedButton;
    AddProjectButton: TSpeedButton;
    SelectFileButton: TSpeedButton;
    OpenFileDialog: TOpenDialog;
    procedure ProjectsCheckListClick(Sender: TObject);
    procedure DeleteProjectButtonClick(Sender: TObject);
    procedure AddProjectButtonClick(Sender: TObject);
    procedure SelectFileButtonClick(Sender: TObject);
    procedure ProjectCodeEditKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure ProjectsCheckListClickCheck(Sender: TObject);
  private
    TSGFile : TIniFile;
    procedure LoadProjectFiles;
    procedure ShowProjectDetails(iList: Integer);
    procedure UpdateProjectDetails(iList: Integer);
  public
    NeedsReload: Boolean;
    constructor Create( AOwner: TComponent ); override;
    destructor  Destroy; override;
    procedure   Save;
  end;

implementation

uses TMUtils;

{$R *.nfm}

  // We must store a mirror of the .TSG file settings so the user can add/alter,
  // then possibly abandon, multiple changes
var
  ProjectList: array of TTSProjectItem;

constructor TGroupOptionsFrame.Create(AOwner: TComponent);
{ Initialise the frame - load the projects
}
begin
  inherited;
  NeedsReload := False;
  if TMUtils.CurrentProject.IsProjectGroup and Assigned( TMUtils.TSGProjects ) then
    ProjectsCheckList.Items.Assign( TMUtils.TSGProjects )
  else
    ProjectsCheckList.Items.Clear;

  TSGFile := TINIFile.Create( TMUtils.CurrentProject.FileName );
  LoadProjectFiles;
end;

destructor TGroupOptionsFrame.Destroy;
{ Release the resources.
  No longer required in .NET, but good practice.
}
begin
  try
    TSGFile.Free;
  finally
    inherited;
  end;
end;

procedure TGroupOptionsFrame.LoadProjectFiles;
{ Load the project group settings and projects.
}
var
  i          : Integer;
  TempList   : TStringList;
  NewProject : TTSProjectItem;
begin
  ProjectsCheckList.Items.Clear;
  SetLength( ProjectList, 0 );

  TempList := TStringList.Create;
  try
    TSGFile.ReadSections( TempList );
    { Remove the settings section }
    if TempList.IndexOf( TSG_OPTIONS ) > -1 then
      TempList.Delete( TempList.IndexOf( TSG_OPTIONS ));

    for i := 0 to TempList.Count - 1 do
      NewProject.Add( Self, TempList[i] );
  finally
    TempList.Free;
  end;
end;

procedure TGroupOptionsFrame.ShowProjectDetails( iList: Integer );
{ Show details for the project at position iList in the checklistbox
}
begin
  if ( iList > -1 ) and ( iList < ProjectsCheckList.Count ) then
  begin
    ProjectsCheckList.Items[iList] := ProjectList[iList].ProjectCode;
    ProjectCodeEdit.Text           := ProjectList[iList].ProjectCode;
    ProjectFileEdit.Text           := ProjectList[iList].FileName;
  end
  else begin
    ProjectCodeEdit.Text := '***No project selected';
    ProjectFileEdit.Text := '';
  end;
end;

procedure TGroupOptionsFrame.UpdateProjectDetails( iList: Integer );
{ Set the project details for the item at position iList
}
begin
  if ( iList > -1 ) and ( iList < Length( ProjectList ) ) then
  begin
    ProjectList[iList].ProjectCode := ProjectCodeEdit.Text;
    ProjectList[iList].FileName    := ProjectFileEdit.Text;
    ProjectList[iList].Active      := ProjectsCheckList.Checked[iList];
  end;
end;

procedure TGroupOptionsFrame.ProjectsCheckListClick(Sender: TObject);
{ Show the description and filename for the selected item
}
begin
  Assert( Sender is TCheckListBox, 'Not a checklist box' );
  ShowProjectDetails( (Sender As TCheckListBox).ItemIndex );
end;

procedure TGroupOptionsFrame.ProjectsCheckListClickCheck(Sender: TObject);
{ Update the active field for the selected item
}
begin
  Assert( Sender is TCheckListBox, 'Not a checklist box' );
  ShowProjectDetails(   (Sender As TCheckListBox).ItemIndex );
  UpdateProjectDetails( (Sender As TCheckListBox).ItemIndex );
end;

procedure TGroupOptionsFrame.Save;
{ Save if OK button (on prefs form) clicked.
  Unfortunately, because the "key field", ie. the project code,
  can be altered by the user; the only way to replace entries is to delete them
  all then repopulate the file.
}
var
  i          : Integer;
  TempList   : TStringList;
begin
  // Step 1, remove all project codes
  TempList := TStringList.Create;
  try
    TSGFile.ReadSections( TempList );
    { Remove the settings section from the list }
    if TempList.IndexOf( TSG_OPTIONS ) > -1 then
      TempList.Delete( TempList.IndexOf( TSG_OPTIONS ));

    for i := 0 to TempList.Count - 1 do
      TSGFile.EraseSection( TempList[i] );
  finally
    TempList.Free;
  end;

  // Step 2, save all project items
  for i := Low( ProjectList ) to High( ProjectList ) do
  begin
    if ( ProjectList[i].ProjectCode = '--' ) then    // skip
      continue;
    TSGFile.WriteString( ProjectList[i].ProjectCode, 'FileName', ProjectList[i].FileName );
    TSGFile.WriteBool(   ProjectList[i].ProjectCode, 'Active',   ProjectList[i].Active );
  end;
end;

procedure TGroupOptionsFrame.DeleteProjectButtonClick(Sender: TObject);
{ Remove the selected project from the project group
}
var
  i : Integer;
begin
  i := ProjectsCheckList.ItemIndex;
  if ( i > -1 ) then
    if (MessageDlg('Really delete '+ProjectsCheckList.Items[i], mtConfirmation, [mbYes, mbNo], 0) in [mrYes, mrNo]) then
    begin
      ProjectList[i].ProjectCode := '--';
      ProjectList[i].Active      := False;
      ProjectList[i].FileName    := '';
      ShowProjectDetails( i );
      NeedsReload := True;
    end;
end;

procedure TGroupOptionsFrame.AddProjectButtonClick(Sender: TObject);
{ Simply insert a blank placeholder and open the filename dialog
}
var
  NewProjectItem : TTSProjectItem;
begin
  NewProjectItem.Add( Self, 'New project '+IntToStr(ProjectsCheckList.Count + 1) );

  ProjectsCheckList.ItemIndex := ProjectsCheckList.Count - 1;
  ProjectsCheckList.Checked[ ProjectsCheckList.Count - 1 ] := True;
  ShowProjectDetails( ProjectsCheckList.Count - 1 );
  ProjectCodeEdit.SelectAll;
  ProjectCodeEdit.SetFocus;

  NeedsReload := True;
end;

procedure TGroupOptionsFrame.SelectFileButtonClick(Sender: TObject);
{ Associate a new file with the code
}
begin
  if ( ProjectsCheckList.Items.Count = 0 ) then
    Exit
  else
    ShowProjectDetails( ProjectsCheckList.ItemIndex ); // Just to make sure...

  if ( OpenFileDialog.FileName = '' ) then
    OpenFileDialog.InitialDir := ExtractFilePath( TMUtils.CurrentProject.FileName )
  else if ( ProjectFileEdit.Text = '' ) then
    OpenFileDialog.InitialDir := ExtractFilePath( OpenFileDialog.FileName )
  else
    OpenFileDialog.FileName := ProjectFileEdit.Text;

  if OpenFileDialog.Execute then
  begin
    ProjectFileEdit.Text := OpenFileDialog.FileName;
    UpdateProjectDetails( ProjectsCheckList.ItemIndex );
  end;
end;

procedure TGroupOptionsFrame.ProjectCodeEditKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
{ Reflect the change the user is making to the project code
}
begin
  UpdateProjectDetails( ProjectsCheckList.ItemIndex );
  ShowProjectDetails( ProjectsCheckList.ItemIndex );
end;

{ TTSProjectItem }
{ We store a mirror of the .TSG file settings in an array so that the user can
  add and alter, and then possibly abandon, multiple changes
}
function TTSProjectItem.Add( Sender: TGroupOptionsFrame; sProjectCode: String ): Integer;
{ Add this item to the list (if it doesn't exist) and return its index.
}
var
  i : Integer;
begin
  // Append it.
  i := Length( ProjectList );
  SetLength( ProjectList, i+1 );

  // Assign the list settings from the file
  ProjectList[i].ProjectCode := sProjectCode;
  ProjectList[i].Active      := Sender.TSGFile.ReadBool( sProjectCode, 'Active',   True );
  ProjectList[i].FileName    := Sender.TSGFile.ReadString( sProjectCode, 'FileName', '' );

  // Update the display
  Sender.ProjectsCheckList.Items.Add( sProjectCode );
  Assert( (i = (Sender.ProjectsCheckList.Count-1)), 'Mismatch between project list array and display' );
  Sender.ProjectsCheckList.Checked[i] := ProjectList[i].Active;
  Result := i;
end;

end.
