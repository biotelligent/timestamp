{
Application:    Time Stamp, Copyright 1997 William Rouck
Module:         TMReport.pas
Contact:        email: wrouck@syntap.com
Modifications:  JM Holloway, June 2004
                justin@schiznit.net

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version. This
program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details. You should have
received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
02111-1307, USA.
}

unit TMReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Borland.Vcl.Db, Borland.Vcl.StdCtrls, System.ComponentModel,
  Borland.Vcl.Buttons, Borland.Vcl.ComCtrls, DateUtils,
  Nevrona.Rave.RpDefine, Nevrona.Rave.RpRave, Nevrona.Rave.RVClass,
  Nevrona.Rave.RVProj, Nevrona.Rave.RVCsStd, Nevrona.Rave.RvCsRpt, Nevrona.Rave.RpMemo,
  Nevrona.Rave.RpBase, Nevrona.Rave.RpSystem, Nevrona.Rave.RpRender,
  TMGridFrme, TMUtils, IniFiles,
  //
  ADONETDb;

type
  TTSReportForm = class(TForm)
    AllDatesCheckBox: TCheckBox;
    DateRangeGroupBox: TGroupBox;
    FromDateTimePicker: TDateTimePicker;
    ToDateTimePicker: TDateTimePicker;
    FilteredCheckBox: TCheckBox;
    FilterEdit: TEdit;
    ReportComboBox: TComboBox;
    Label1: TLabel;
    PrintBtn: TBitBtn;
    PDFBtn: TBitBtn;
    CancelBtn: TBitBtn;
    Label2: TLabel;
    Label4: TLabel;
    Label3: TLabel;
    SortOrderComboBox: TComboBox;
    procedure AllDatesCheckBoxClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure PrintBtnClick(Sender: TObject);
    procedure CancelBtnClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    HeaderFont,
    DetailFont  : TFont;
    TSGrid      : TTSGrid;
    CDS         : TADONETConnector;
    RvSystem    : TRvSystem;
    RvProject   : TRvProject;
    UseDecimalSummary : Boolean;
    //RvRenderPDF : TRvRenderpdf;
    procedure CreateReport( bPreview: Boolean );
    procedure PrintTabularReport(Report: TBaseReport);
    procedure RvSystemPrint(Sender: TObject);
    procedure RvSystemPrintFooter(Sender: TObject);
    procedure RvSystemPrintNewPage(Sender: TObject);
  private
    FPrintSlackInformation: Boolean;
    PrintProjectTitle     : Boolean;
    PrintFileName         : Boolean;
    PrintEndNotes         : Boolean;
    PrintPageNumbers      : Boolean;
    PrintRateInformation  : Boolean;
    Print24Hours          : Boolean;
    PrintCustomFields     : Boolean;
    function  GetUseDateRange: Boolean;
    procedure SetUseDateRange(const Value: Boolean);
  public
    procedure   Execute( bShow, bPreview: Boolean );
    constructor Create( AOwner: TComponent; AGrid: TTSGrid ); reintroduce;
    property    PrintSlackInformation: Boolean read  FPrintSlackInformation
                                               write FPrintSlackInformation;
    property    UseDateRange:          Boolean read  GetUseDateRange
                                               write SetUseDateRange; 
  end;

implementation

uses TMDataModule;

{$R *.nfm}

const
  BTN_PRINT                  = '&Print';
  BTN_PREVIEW                = '&Preview';

procedure TTSReportForm.AllDatesCheckBoxClick(Sender: TObject);
{ Set option when user alters check box
}
begin
  UseDateRange := not ( Sender As TCheckbox ).Checked;
end;

constructor TTSReportForm.Create(AOwner: TComponent; AGrid: TTSGrid);
{ Set up the form references - unlike all other forms except the main form,
  this form instance is kept for the whole of the application session
}
begin
  inherited Create( AOwner );
  TSGrid := AGrid;
  CDS    := AGrid.TSClientDataset;
end;

procedure TTSReportForm.Execute(bShow, bPreview : Boolean);
{ Run the report to preview / printer; showing the filter box if specified
}
begin
  { Load user preferences }
  with TIniFile.Create( TMUtils.GetINIFilePath ) do
  try
    UseDecimalSummary := ReadBool('Options', 'DecimalSummary', False);

    HeaderFont.Name   := ReadString ('Options','HeadingsReportFontName', DEFAULT_FONTNAME);
    HeaderFont.Size   := ReadInteger('Options','HeadingsReportFontSize', SUMMARY_FONTSIZE);
    HeaderFont.Style  := [fsBold,fsUnderline];

    DetailFont.Name   := ReadString ('Options','DetailLineReportFontName', DEFAULT_FONTNAME);
    DetailFont.Size   := ReadInteger('Options','DetailLineReportFontSize', DETAIL_FONTSIZE);
    DetailFont.Style  := [];

    PrintProjectTitle      := ReadBool('Options','PrintProjectTitle', True);
    PrintFileName          := ReadBool('Options','PrintFileName', True);
    PrintEndNotes          := ReadBool('Options','PrintEndNotes', True);
    PrintPageNumbers       := ReadBool('Options','PrintPageNumbers', True);

    PrintSlackInformation  := ReadBool('Options','PrintSlackInformation', False);
    PrintRateInformation   := ReadBool('Options','PrintRateInformation', True);
    Print24Hours           := ReadBool('Options','Print24Hours', True);
    PrintCustomFields      := ReadBool('Options','PrintCustomFields', False);
  finally
    Free;
  end;

  if bPreview then
    PrintBtn.Caption := BTN_PREVIEW
  else
    PrintBtn.Caption := BTN_PRINT;

  if bShow then
    Show
  else
    PrintBtnClick( PrintBtn );
end;

procedure TTSReportForm.FormCreate(Sender: TObject);
{ On the first creation, set up all components
}
begin
  HeaderFont:= TFont.Create;
  DetailFont:= TFont.Create;
  RvSystem  := TRvSystem.Create( Self );
  RvProject := TRvProject.Create( Self );

  RvProject.Engine      := RvSystem;
  RvProject.ProjectFile := ExtractFilepath(application.ExeName)+'\TSReport.rav';

  if FileExists( RvProject.ProjectFile ) then
  begin
    RvProject.Open;
    RvProject.GetReportList( ReportComboBox.Items, False);    
  end else
    OutputDebugString( RvProject.ProjectFile + ' not found' );

  FromDateTimePicker.DateTime := now - 7;
  ToDateTimePicker.DateTime   := now;

  ReportComboBox.Items.Insert( 0, TXT_GROUPEDBYDAY );
  if TMUtils.CurrentProject.IsProjectGroup then
  begin
    ReportComboBox.Items.Insert( 1, TXT_GROUPEDBYDAYANDPROJECT );
    ReportComboBox.ItemIndex := 1;
  end else
    ReportComboBox.ItemIndex := 0;
end;

procedure TTSReportForm.PrintBtnClick(Sender: TObject);
{ Apply the options on the form and create the report
}
begin
  TSGrid.PostChanges( FALSE );
  TsData.BeginUpdate(True);
  try
    CDS.Filtered          := False;
    TsData.First;
    try
      if not AllDatesCheckBox.Checked then
      begin
        CDS.Filter   := Format( 'DateNumber >= %d and DateNumber <= %d',
                              [ Trunc( Double( FromDateTimePicker.DateTime )),
                                Trunc( Double( ToDateTimePicker.DateTime )) ]);
        CDS.Filtered := True;
        //TSGrid.RecalculateAggregates;
      end;
      if Self.Visible then
        Self.Hide;

      if ( SortOrderComboBox.ItemIndex > -1 ) then
        TSGrid.SortBy( SortOrderComboBox.Items[ SortOrderComboBox.ItemIndex ], False )
      else if ( SortOrderComboBox.Text <> '' ) then
        CDS.IndexFieldNames := SortOrderComboBox.Text;

      TsData.First;
      CreateReport( (Sender As TButton).Caption = BTN_PREVIEW );
    finally
      CDS.Filtered := False;
    end;
  finally
    TsData.EndUpdate;
  end;
end;

procedure TTSReportForm.CancelBtnClick(Sender: TObject);
{ User exits
}
begin
  Self.Hide;
end;

procedure TTSReportForm.CreateReport(bPreview: Boolean);
{ Drive the report
}
begin
  RvSystem.SystemSetups  := RvSystem.SystemSetups - [ssAllowSetup]; { TODO -oJUH -cPrinting : Show here if still having PDF problems. Can set up from preview screen though }
  if bPreview then
    RvSystem.ReportDest  := rdPreview
  else
    RvSystem.ReportDest  := rdPrinter;
  RvSystem.OnNewPage     := RvSystemPrintNewPage;
  RvSystem.OnPrintFooter := RvSystemPrintFooter;
  RvSystem.OnPrint       := RvSystemPrint;
  RvSystem.Execute;
end;

procedure TTSReportForm.RvSystemPrintFooter(Sender: TObject);
{ Generate the page footer for the code based report
}
begin  with ( Sender as TBaseReport ) do  begin    MarginBottom := 0.5;    MarginLeft   := 0.4;    MarginRight  := 0.4;    SetFont( DetailFont.Name, DetailFont.Size - 1);    if PrintPageNumbers then      PrintFooter( 'Page ' + IntToStr(CurrentPage), pjLeft);    if PrintFileName then      PrintFooter( CurrentProject.FileName, pjCenter );    PrintFooter( FormatDateTime( 'dd mmm yyyy hh:nn', now ), pjRight);  end;end;
procedure TTSReportForm.RvSystemPrint(Sender: TObject);
{ Handle the code based report OnPrint event
}
begin
  PrintTabularReport( Sender As TBaseReport );
end;

procedure TTSReportForm.RvSystemPrintNewPage(Sender: TObject);
{ Generate the new page header for the code based report
}
begin
  with ( Sender as TBaseReport ) do
  begin    MarginTop   := 0.5;    MarginLeft  := 0.4;    MarginRight := 0.4;    if PrintProjectTitle then    begin      SetFont( HeaderFont.Name, HeaderFont.Size + 4);      if UseDateRange then      begin        PrintHeader( CurrentProject.ProjectName, pjLeft );        SetFont( HeaderFont.Name, HeaderFont.Size + 2);        PrintHeader( Format('%s to %s', [                     FormatDateTime( 'd/MM/yyyy', FromDateTimePicker.DateTime ),                     FormatDateTime( 'd/MM/yyyy', ToDateTimePicker.DateTime )                     ]), pjRight );      end      else        PrintHeader( CurrentProject.ProjectName, pjCenter );      NewLine;    end;    AssignFont( HeaderFont );
    ClearTabs;
    if CurrentProject.IsProjectGroup then
    begin
      SetTab(0.4, pjLeft,  1.0, 0, 0, 0);
      SetTab(1.4, pjLeft,  2.7, 0, 0, 0);
      SetTab(2.5, pjLeft,  0.9, 0, 0, 0);
      SetTab(3.4, pjRight, 0.8, 0, 0, 0);
      if PrintCustomFields then
      begin
        SetTab(4.4,  pjLeft, 0.35, 0, 0, 0);
        SetTab(4.75, pjLeft, 0.65, 0, 0, 0);
        SetTab(5.4, pjLeft, 2.4, 0, 0, 0);
      end
      else
        SetTab(4.4, pjLeft,  3.4, 0, 0, 0);
    end
    else begin
      SetTab(0.4, pjLeft,  2.7, 0, 0, 0);
      SetTab(1.5, pjLeft,  0.9, 0, 0, 0);
      SetTab(2.4, pjRight, 0.8, 0, 0, 0);
      if PrintCustomFields then
      begin
        SetTab(3.4,  pjLeft, 0.35, 0, 0, 0);
        SetTab(3.75, pjLeft, 0.65, 0, 0, 0);
        SetTab(4.4, pjLeft, 3.4, 0, 0, 0);
      end
      else
        SetTab(3.4, pjLeft,  4.4, 0, 0, 0);
    end;
    if CurrentProject.IsProjectGroup then
      PrintTab( 'Project' );
    PrintTab( 'Start Time' );
    PrintTab( 'End Time' );
    PrintTab( 'Work Time' );
    if PrintCustomFields then
    begin
      PrintTab( 'Bug' );
      PrintTab( 'P/O' );
    end;
    PrintTab( 'Notes' );

    AssignFont( DetailFont );
    NewLine;
  end;end;

procedure TTSReportForm.PrintTabularReport(Report: TBaseReport);
{ Loop thru the dataset, supplying data which generates the code based report
}
var
  MemoBuf         : TMemoBuf;
  GroupedRowCount : Integer;
  LastWorkDouble  : Double;
  LastDate        : TDateTime;

  procedure AddRow;
  var
    EndTime  : String;
  begin
    if CurrentProject.IsProjectGroup then
      Report.PrintTab( TSGrid.TSClientDataset['ProjectCode'] );

    if IsSameDay( TSGrid.TSClientDataset['StartTime'], TSGrid.TSClientDataset['EndTime'] ) then
      EndTime := FormatDateTime( 'HH:nn', TSGrid.TSClientDataset['EndTime'] )
    else
      EndTime := FormatDateTime( 'dd/MM/yy HH:nn', TSGrid.TSClientDataset['EndTime'] );

    Report.PrintTab( FormatDateTime( 'dd/MM/yy HH:nn', TSGrid.TSClientDataset['StartTime'] ) );
    Report.PrintTab( EndTime );
    Report.PrintTab( TMUtils.FormatTimeDelta( TSGrid.TSClientDataset['WorkTime'] ) );

    if PrintCustomFields then
    begin
      Report.PrintTab( TSGrid.TSClientDataset.FieldByName( 'Unused2' ).AsString );
      Report.PrintTab( TSGrid.TSClientDataset.FieldByName( 'Unused5' ).AsString );
    end;
    
    if ( TSGrid.TSClientDataset.FieldByName( 'Notes' ).IsNull ) then
      MemoBuf.Text := ' '
    else
      MemoBuf.Text := TSGrid.TSClientDataset.FieldByName( 'Notes' ).AsString;
    if CurrentProject.IsProjectGroup then
      MemoBuf.PrintStart := 4.4
    else
      MemoBuf.PrintStart := 3.4;
    if PrintCustomFields then
      MemoBuf.PrintStart := MemoBuf.PrintStart + 1.0;

    MemoBuf.PrintEnd   := 8.0;
    Report.PrintMemo( MemoBuf, 0, False );
  end;

  function AddGroupRow: String;
  var
    sOutsideTimes: String;
  begin
    if ( GroupedRowCount > 1 ) then
      sOutsideTimes := FormatDateTime( 'HH:mm - ', LastDate )
                     + FormatDateTime( 'HH:mm - ', TSGrid.TSClientDataset['EndTime'] )
    else
      sOutsideTimes := '';

    if CurrentProject.IsProjectGroup then
      Report.PrintTab( '' );
    Report.PrintTab( 'Summary for ' + FormatDateTime( 'ddd dd/MM/yy', LastDate ) );
    Report.PrintTab( '' );
    Report.Bold := True;
    Report.PrintTab( TMUtils.FormatTimeDelta( LastWorkDouble ) );
    Report.Bold := False;
    Report.NewLine;
    Report.NewLine;
  end;

  procedure ResetSummaryInfo;
  begin
    LastDate       := TSGrid.TSClientDataset['StartTime'];
    LastWorkDouble := 0;
    GroupedRowCount:= 0;
  end;

  procedure AddTotalTime( sTitle: String; sField: String );
  var
    sTotal: String;
  begin
    Report.Bold := True;
    if CurrentProject.IsProjectGroup then
      Report.PrintTab( '' );
    Report.PrintTab( sTitle );
    Report.PrintTab( '' );
    Report.Bold := False;
    sTotal      := FormatSummaryTime( Double( TSGrid.TSClientDataset.FieldByName( sField ).AsVariant ), UseDecimalSummary );
    if UseDecimalSummary then
      sTotal := sTotal + ' Hours';
    Report.PrintTab( sTotal );
    Report.NewLine;
  end;

  procedure AddTotalCost( sTitle: String; sField: String );
  begin
    Report.Bold := True;
    if CurrentProject.IsProjectGroup then
      Report.PrintTab( '' );
    Report.PrintTab( sTitle );
    Report.PrintTab( '' );
    Report.Bold := False;
    Report.PrintTab( FloatToStrF( Double( TSGrid.TSClientDataset.FieldByName( sField ).AsVariant ), ffCurrency, 5, 2 ));
    Report.NewLine;
  end;


  procedure AddTotals;
  { Aggregated totals
  }
  begin
    Report.NewLine;
    AddTotalTime( 'Total Time Elapsed', 'TotalTimeElapsed' );
    if PrintSlackInformation then
      AddTotalTime( 'Total Slack Time', 'TotalSlackTimeElapsed' );
    AddTotalTime( 'Total Work Time',    'TotalWorkTimeElapsed' );

    Report.NewLine;
    if PrintSlackInformation then
      AddTotalCost( 'Total Slack Cost', 'TotalSlackTimeCost' );
    AddTotalCost( 'Total Work Cost',  'TotalWorkTimeCost' );

    if PrintEndNotes then
    begin
      Report.NewLine;
      Report.NewLine;
      Report.Bold := True;
      if CurrentProject.IsProjectGroup then
        Report.PrintTab( '' );
      Report.PrintTab( CurrentProject.EndNotes );
      Report.Bold := False;
    end;
  end;

begin
  MemoBuf := TMemoBuf.Create;
  if ( CDS.RecordCount > 0 ) then
  try
    TSGrid.PostChanges( FALSE );
    TSGrid.EnableControls := False;
    try
      TSGrid.TSClientDataset.First;
      ResetSummaryInfo;

      while not TSGrid.TSClientDataset.Eof do
        begin
          { Add summary band }
          if not IsSameDay( TSGrid.TSClientDataset['StartTime'], LastDate ) then
            begin
              AddGroupRow;
              ResetSummaryInfo;
              if Report.LinesLeft <= 7 then
                Report.NewPage;
            end;

          { Add normal info, update summary info }
          AddRow;
          inc( GroupedRowCount );
          LastWorkDouble := LastWorkDouble + TSGrid.TSClientDataset['WorkTime'];
          TSGrid.TSClientDataset.Next;

          if Report.LinesLeft <= 7 then
            Report.NewPage;
        end; { while not eof }

      { Summary line for the last record }
      if ( TSGrid.TSClientDataset.RecordCount > 0 ) then
      begin
        AddGroupRow;
        AddTotals;
      end;

    finally
      TSGrid.EnableControls := True;
    end;
  finally
    MemoBuf.Free;
  end;
end;


function TTSReportForm.GetUseDateRange: Boolean;
begin
  Result := not AllDatesCheckBox.Checked;
end;

procedure TTSReportForm.SetUseDateRange(const Value: Boolean);
{ Show the date range selection group as en/disabled dependent on the checkbox.
  This should *not* be necessary.
  It may be a VCL/.NET bug, but the normal control disabled look does not take effect.
}
begin
  AllDatesCheckBox.Checked  := not Value;
  DateRangeGroupBox.Enabled := Value;
  if DateRangeGroupBox.Enabled then
    DateRangeGroupBox.Color := Self.Color
  else
    DateRangeGroupBox.Color := $00e1e1e1;
end;

procedure TTSReportForm.FormDestroy(Sender: TObject);
{ Clean up on exit.
  Garbage collection in .NET means we don't have to do this, but I can't help it...
}
begin
  RvProject.Free;
  RvSystem.Free;
  HeaderFont.Free;
  DetailFont.Free;
end;

end.
