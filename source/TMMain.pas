unit TMMain;
{
Application:    Time Stamp, Copyright 1997 William Rouck
Module:         TMMain.pas
Contact:        email: wrouck@syntap.com
Modifications:  JM Holloway, June 2004
                justin@schiznit.net

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version. This
program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details. You should have
received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
02111-1307, USA.
}

interface

uses
  Windows, Messages, SysUtils, Variants,
  Classes, Graphics, Controls, Forms,
  Dialogs, Menus, ActnList, ImgList,
  ExtCtrls, ComCtrls, Grids, Buttons, ToolWin, StdCtrls, IniFiles, Printers,
  System.ComponentModel, DB,Calendar, System.Diagnostics, System.Data,
  Spin, TMUtils, TMGridFrme, TMReport;

type
  TTSMainForm = class(TForm)
    XMLFileDialog: TSaveDialog;
    AutoSaveTimer: TTimer;
    TrayIconPopupMenu: TPopupMenu;
    ExitTimeStampTrayMenuItem: TMenuItem;
    N11: TMenuItem;
    RestorePopupMenuItem: TMenuItem;
    MinimizePopupMenuItem: TMenuItem;
    N10: TMenuItem;
    TimeStampHelp1: TMenuItem;
    N9: TMenuItem;
    StartSlackTimerTrayMenuItem: TMenuItem;
    StopSlackTimerTrayMenuItem: TMenuItem;
    N8: TMenuItem;
    StartTimerTrayMenuItem: TMenuItem;
    StopTimerTrayMenuItem: TMenuItem;
    TimeStampImageList: TImageList;
    TimeStampActionList: TActionList;
    CreateProjectAction: TAction;
    OpenProjectAction: TAction;
    AppendProjectAction: TAction;
    SaveProjectAction: TAction;
    SaveProjectAsAction: TAction;
    ExportASCIILogAction: TAction;
    PrintSetupAction: TAction;
    PrintLogAction: TAction;
    PrintPreviewAction: TAction;
    PreferencesAction: TAction;
    ExitAction: TAction;
    EditTaskAction: TAction;
    DeleteTaskAction: TAction;
    ClearNotesAction: TAction;
    StartTimerAction: TAction;
    StopTimerAction: TAction;
    StartSlackAction: TAction;
    SetDefaultHourlyRateAction: TAction;
    SetProjectTitleAction: TAction;
    ReducedModeAction: TAction;
    AlwaysOnTopAction: TAction;
    HelpContentsAction: TAction;
    VersionHistoryAction: TAction;
    LicenseInfoAction: TAction;
    AboutAction: TAction;
    SetDefaultReportEndNotesAction: TAction;
    SetThisProjectsReportEndNotesAction: TAction;
    CreateDefaultNotesListAction: TAction;
    ExportXMLProjectAction: TAction;
    HideToolbarAction: TAction;
    GNUGPLAction: TAction;
    HideProjectsBarAction: TAction;
    ExportFileDialog: TSaveDialog;
    StatusTimer: TTimer;
    OpenFileDialog: TOpenDialog;
    SaveFileDialog: TSaveDialog;
    PrinterSetupDialog: TPrinterSetupDialog;
    TimeStampMainControlBar: TControlBar;
    MainToolBar: TToolBar;
    TSStatusBar: TStatusBar;
    MainPanel: TPanel;
    TSGrid: TTSGrid;
    AppendFileDialog: TOpenDialog;
    MainMenu1: TMainMenu;
    File1: TMenuItem;
    OpenProjectFileMenuItem: TMenuItem;
    SaveProjectFileMenuItem: TMenuItem;
    SaveProjectFileAsMenuItem: TMenuItem;
    N14: TMenuItem;
    ExporttoXMLFileMenuItem: TMenuItem;
    ExporttoASCIIFileMenuItem: TMenuItem;
    N1: TMenuItem;
    PrintSetupFileMenuItem: TMenuItem;
    PrintFileMenuItem: TMenuItem;
    PrintPreviewFileMenuItem: TMenuItem;
    N3: TMenuItem;
    PreferencesFileMenuItem: TMenuItem;
    N6: TMenuItem;
    ExitMenuItem: TMenuItem;
    Edit1: TMenuItem;
    EditSelectedTaskEditMenuItem: TMenuItem;
    DeleteSelectedTaskEditMenuItem: TMenuItem;
    ClearNotesforSelectedTaskEditMenuItem: TMenuItem;
    Timers1: TMenuItem;
    StartTimerTimersMenuItem: TMenuItem;
    StopTimerTimersMenuItem: TMenuItem;
    N2: TMenuItem;
    StartSlackTimerTimersMenuItem: TMenuItem;
    Tools1: TMenuItem;
    SetHourlyChargeRateToolsMenuItem: TMenuItem;
    N7: TMenuItem;
    SetProjectTitleToolsMenuItem: TMenuItem;
    SetProjectendNotes1: TMenuItem;
    SetDefaultReportEndNotesToolsMenuItem: TMenuItem;
    N13: TMenuItem;
    CreateDefaultNotesListMenuItem: TMenuItem;
    HideToolbarToolsMenuItem: TMenuItem;
    Help1: TMenuItem;
    HelpTopicsHelpMenuItem: TMenuItem;
    N4: TMenuItem;
    VersionHistoryHelpMenuItem: TMenuItem;
    LicenseInfoHelpMenuItem: TMenuItem;
    GPLHelpMenuItem: TMenuItem;
    RegistrationInfoHelpMenuItem: TMenuItem;
    N5: TMenuItem;
    AboutHelpMenuItem: TMenuItem;
    ToolButton3: TToolButton;
    StartTimeToolButton: TToolButton;
    SlackTimeToolButton: TToolButton;
    StopTimeToolButton: TToolButton;
    NewLogToolButton: TToolButton;
    LoadFileToolButton: TToolButton;
    SaveFileToolButton: TToolButton;
    PrintPreviewToolButton: TToolButton;
    ToolButton4: TToolButton;
    DefaultHourRateToolButton: TToolButton;
    TaskEditToolButton: TToolButton;
    TaskDeleteToolButton: TToolButton;
    ShowHelpToolButton: TToolButton;
    ToolButton1: TToolButton;
    RemindTimerPanel: TPanel;
    Label2: TLabel;
    RemindTimeSpinEdit: TSpinEdit;
    RemindAction: TAction;
    ToolButton2: TToolButton;
    ToolButton5: TToolButton;
    RemindTimer: TTimer;
    GridPopup: TPopupMenu;
    NotesPopup: TPopupMenu;
    CreateDefaultNotesList1: TMenuItem;
    ClearNotesforSelectedTask1: TMenuItem;
    ContinueTimeAction: TAction;
    ContinueTimingMenuItem: TMenuItem;
    ReducedModeToolButton: TToolButton;
    miView: TMenuItem;
    WorkInReducedMode1: TMenuItem;
    N15: TMenuItem;
    AlwaysonTopMenuItem: TMenuItem;
    N12: TMenuItem;
    StartTimer1: TMenuItem;
    StartSlackTimer1: TMenuItem;
    StopTimer1: TMenuItem;
    CreateGroupAction: TAction;
    NewProjectFileMenuItem: TMenuItem;
    ToolButton6: TToolButton;
    NewFromSelectedAction: TAction;
    NewFromSelectedAction1: TMenuItem;
    AboutTimeStamp1: TMenuItem;
    AddProjectAction: TAction;
    N16: TMenuItem;
    procedure AboutActionExecute(Sender: TObject);
    procedure AlwaysOnTopActionExecute(Sender: TObject);
    procedure AppendProjectActionExecute(Sender: TObject);
    procedure ApplicationMinimizeHandler(Sender: TObject);
    procedure ApplicationRestoreHandler(Sender: TObject);
    procedure ApplicationRxTrayIconClick(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure AutoSaveTimerTimer(Sender: TObject);
    procedure ClearNotesActionExecute(Sender: TObject);
    procedure CreateDefaultNotesListActionExecute(Sender: TObject);
    procedure CreateProjectActionExecute(Sender: TObject);
    procedure DeleteTaskActionExecute(Sender: TObject);
    procedure EditTaskActionExecute(Sender: TObject);
    procedure ExitActionExecute(Sender: TObject);
    procedure ExportASCIILogActionExecute(Sender: TObject);
    procedure ExportXMLProjectActionExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure GNUGPLActionExecute(Sender: TObject);
    procedure HelpContentsActionExecute(Sender: TObject);
    procedure HideToolbarActionExecute(Sender: TObject);
    procedure LicenseInfoActionExecute(Sender: TObject);
    procedure MinimizePopupMenuItemClick(Sender: TObject);
    procedure MRUManagerClick(Sender: TObject; const RecentName,
      Caption: &String; UserData: Integer);
    procedure OpenProjectActionExecute(Sender: TObject);
    procedure PreferencesActionExecute(Sender: TObject);
    procedure PrintLogActionExecute(Sender: TObject);
    procedure PrintPreviewActionExecute(Sender: TObject);
    procedure PrintSetupActionExecute(Sender: TObject);
    procedure RemindTimerTimer(Sender: TObject);
    procedure ReducedModeActionExecute(Sender: TObject);
    procedure RegistrationInfoHelpMenuItemClick(Sender: TObject);
    procedure ResetRemindTimer;
    procedure RestorePopupMenuItemClick(Sender: TObject);
    procedure SaveProjectActionExecute(Sender: TObject);
    procedure SetDefaultHourlyRateActionExecute(Sender: TObject);
    procedure SetDefaultReportEndNotesActionExecute(Sender: TObject);
    procedure SetProjectTitleActionExecute(Sender: TObject);
    procedure SetThisProjectsReportEndNotesActionExecute(Sender: TObject);
    procedure StartSlackActionExecute(Sender: TObject);
    procedure StartTimerActionExecute(Sender: TObject);
    procedure StatusTimerTimer(Sender: TObject);
    procedure VersionHistoryActionExecute(Sender: TObject);
    procedure StopTimerActionExecute(Sender: TObject);
    procedure TSStatusBarDrawPanel(StatusBar: TStatusBar; Panel: TStatusPanel; Rect: TRect);
    procedure RemindActionExecute(Sender: TObject);
    procedure RemindTimeSpinEditChange(Sender: TObject);
    procedure ContinueTimeActionExecute(Sender: TObject);
    procedure GridPopupPopup(Sender: TObject);
    procedure PrintPreviewToolButtonClick(Sender: TObject);
    procedure CreateGroupActionExecute(Sender: TObject);
    procedure NewFromSelectedActionExecute(Sender: TObject);
    procedure AddProjectActionExecute(Sender: TObject);
  private
    VersionInfoStr: &String;
    FStatusText   : &String;
    procedure SetStatusText(const Value: &String);
    procedure SetAutoSavePreference(Enabled: Boolean; Interval: Integer);
    procedure PrintLog( bShow, bPreview: Boolean );
    procedure LoadAutoFillNotes;
    procedure SaveAutoFillNotes;
    procedure AutoNotePopupClick(Sender: TObject);
    procedure UpdateAutoFillNotes;
    procedure DoRecordChanged( Row: Integer; FieldValue: String );
    procedure SetAsTiming;
  public
    FRecordIsChanged: Boolean;
    function  GetRowCounter: Integer;
    procedure SetRecordIsChanged(const Value: Boolean);
    procedure RowScrolled( Sender: TObject );
    procedure SetEnableTimerKeyShortcuts(const Value: Boolean);
  private
    FInit: Boolean;        // Has the form been initialized?
    TSReportForm:  TTSReportForm;
    AutoFillNotes: TStringList;
    procedure InitForm;
  public
    procedure UpdateTitle;
    procedure UpdateStatusPanel;
    procedure UpdateGUI;
    property  RowCounter: Integer read GetRowCounter;
    property  StatusText: String  read FStatusText write SetStatusText;
    property  RecordIsChanged: Boolean read FRecordIsChanged write SetRecordIsChanged;
    property  EnableTimerKeyShortcuts : Boolean write SetEnableTimerKeyShortcuts;
  end;

var
   TSMainForm: TTSMainForm = nil;

   UpdateRow,                            {row to update with end-time information}
   CurrentRow                : longint;  {Currently selected row}
   ResumeClose               : boolean;  {Flag to set if user does not cancel program Exit after save dialog}
   MainFormIsCreating        : boolean;  {Flag to tell units that the main form is being created.  This
                                          is needed for some functions that reuse menu items such as
                                          showing the project bar, but need to include or exclude parts
                                          of themselves if the main form hasn't shown yet.}
   HideToolbarToggle,
   OnTopToggle,
   TrayIconIsRedFlag         : boolean;

   ApplicationwindowState    : TWindowState;

   TrayIconClickRestores,
   ShowTimerInTrayCaption,
   HideTaskButton,
   FlashColorsWhileTiming,
   StartScreenSaverWhenSlackStarts,
   ShowSlackTime,
   ShowWorkTime,
   ShowWorkCost         : boolean;
   ShowTrayIcon         : string[2];

   CurrentFormHeight,
   CurrentFormWidth,
   CurrentMainToolBarLocation : word;

implementation

uses TMEdit, TMAbout, TMNotBld, TMPref, TMDataModule;

{$R *.nfm}

//*****************************************************************************

procedure TTSMainForm.FormCreate(Sender: TObject);
{ TSMainForm does not seem to be instantiated at the time this method is called
  under D8, so functionality has been moved into InitForm instead
}
begin
  InitForm;
  TSReportForm := nil;
end; {TSMainForm.FormCreate}

procedure TTSMainForm.InitForm;
{ This procedure handles initialization of settings for the main interface
  when it is created.
}
var
  INISettingsFile     : TIniFile;
  INISettingInput     : string[10];
  LastSavedFileName   : string;
  i                   : Integer;
begin
  if ( not FInit ) and Assigned( TSMainForm ) then
  begin
     OutputDebugString('Initialising mainform');
     {Set Main Form Is Creating Flag}
     TMMain.MainFormIsCreating := TRUE;
     AutoFillNotes             := nil;

     { Copy blank hints so they automagically come from the item caption... }
     for i := 0 to TimeStampActionList.ActionCount - 1 do
       with TAction( TimeStampActionList.Actions[i] ) do
         if ( Hint = '' ) then
           Hint := StringReplace( Caption, '&', '', [rfReplaceAll] );

     {Set OnMinimize and OnRestore handlers}
     Application.OnMinimize := ApplicationMinimizeHandler;
     Application.OnRestore  := ApplicationRestoreHandler;

     {Call external unit TMUtils for setting various form properties}
     TMUtils.SetMainFormProperties;
     TMUtils.LoadPreferedLayout;
     TMUtils.ClearStringGrid('');

     {Set Help File}
     Application.HelpFile := GetHelpFilePath;

     INISettingsFile    := TINIFile.Create(TMUtils.GetINIFilePath);

     DefaultProject.HourlyRate     := INISettingsFile.ReadFloat('Options','DefaultRate', 25.0 );
     DefaultProject.EndNotes       := INISettingsFile.ReadString('Defaults','DefaultProjectEndNotes','');
     DefaultProject.CurrencySymbol := CurrencyString; // Set to the locale default

     {Get flashing tray icon preferences}
     TMMain.FlashColorsWhileTiming := INISettingsFile.ReadBool('Options','FlashColorsWhileTiming',FALSE);

     {Get timer key shortcut preferences}
     EnableTimerKeyShortcuts := not INISettingsFile.ReadBool('Options','DisableTimerKeyShortcuts', False);

     {See if last saved file should be loaded... open if it should}
     if  INISettingsFile.ReadBool('Options','AutoLoad',True)
     and (ParamCount = 0) then
        begin
           LastSavedFileName := INISettingsFile.ReadString('Options','LastSavedFile','Error');
           if FileExists(LastSavedFileName) then
             TMUtils.OpenTimeRecordDataFile( LastSavedFileName );
        end;

     {Before getting tray icon preferences, get the preference for what to do
      with the task button when the tray icon is used}
     TMMain.HideTaskButton := INISettingsFile.ReadBool( 'Options', 'HideTaskButton', FALSE );

     {See if user prefers a displayed Tray Icon by default}
     TMMain.ShowTrayIcon := INISettingsFile.ReadString('Options','ShowTrayIcon','Error');

     if TMMain.ShowTrayIcon = '1' then
        begin
//tray             ApplicationRxTrayIcon.Active := TRUE;
             if TMMain.HideTaskButton = TRUE then
                     showwindow(application.handle, sw_hide)
             else
                     showwindow(application.handle, sw_showdefault);

             {Get Tray Icon click action preference}
             if INISettingsFile.ReadString('Options','TrayIconClickRestores','Error') = '1' then
                begin
                     TMMain.TrayIconClickRestores := TRUE;
                end
             else
                begin
                     TMMain.TrayIconClickRestores := FALSE;
                end; {if INISettingsFile.ReadString}
        end
     else
        begin
//tray             ApplicationRxTrayIcon.Active := FALSE;
        end; {if TMMain.ShowTrayIcon = '1'}

     {See if user prefers AutoSave to be enabled}
     SetAutoSavePreference( INISettingsFile.ReadBool('Options','AutoSave',False),  INISettingsFile.ReadInteger( 'Options','AutoSaveTime', 5) );

     RemindTimeSpinEdit.Value := INISettingsFile.ReadInteger('Options', 'RemindTime', 30);
     RemindAction.Checked     := INISettingsFile.ReadBool('Options', 'Remind', False);
     RemindActionExecute( Self );
     {Fill MRU List}
//mru     MRUManager.LoadFromIni(INISettingsFile,'MRUList');

     {Set file changed flag}
     FRecordIsChanged := FALSE;

     { Hook changed event }
     TSGrid.OnRecordChanged := DoRecordChanged;

     {Set Hide Toolbar Flag}
     HideToolbarToggle := FALSE;

     {Set Always on Top Flag}
     OnTopToggle       := FALSE;

     {Open File if double-clicked in Windows}
     if ParamCount > 0 then
       TMUtils.OpenTimeRecordDataFile( ParamStr(1) );

     {Set timer display in task bar caption preference.}
     ShowTimerInTrayCaption := INISettingsFile.ReadBool( 'Options', 'ShowTimerInTrayCaption', FALSE );

     {Check user's default directory preferences}
     if INISettingsFile.ReadBool( 'Options', 'UseDefaultDirectory', False ) then
       begin
         OpenFileDialog.InitialDir := INISettingsFile.ReadString('Options','DefaultDirectory','c:\');
         SaveFileDialog.InitialDir := INISettingsFile.ReadString('Options','DefaultDirectory','c:\');
       end;

     {Set Screen Saver startup options - Start Screen Saver with Slack}
     StartScreenSaverWhenSlackStarts := INISettingsFile.ReadBool( 'Options', 'StartScreenSaverWithSlack', False );

     {Get column visibility preferences.}
     ShowSlackTime := INISettingsFile.ReadBool( 'Options','ShowSlackTime', True );
     TSGrid.ShowColumn( 'SlackDiffColumn', ShowSlackTime );

     INISettingInput := INISettingsFile.ReadString('Options','ShowWorkTime','Error');
     ShowWorkTime    := ( INISettingInput = '1' );
     TSGrid.ShowColumn( 'WorkTimeColumn', ShowWorkTime );

     INISettingInput := INISettingsFile.ReadString('Options','ShowWorkCost','Error');
     ShowWorkCost    := ( INISettingInput = '1' );
     TSGrid.ShowColumn( 'WorkCostColumn', ShowWorkCost );
     TSGrid.CalendarPanel.Visible := INISettingsFile.ReadBool('Options','ShowCalendar',True);


     { Re-hook the popup menus at runtime in case designtime frame closure has "lost" them }
     TSGrid.TaskDBGrid.PopupMenu := GridPopup;
     TSGrid.NoteDBMemo.PopupMenu := NotesPopup;
     GridPopup.OnPopup           := GridPopupPopup;
     TSGrid.OnRowScrolled        := RowScrolled;

     LoadAutoFillNotes;

     { Apply user preference to start in reduced mode }
     if INISettingsFile.ReadBool( 'Options', 'StartReduced', False) then
     begin
       ReducedModeAction.Checked := True;
       ReducedModeActionExecute( Self );
     end;

     {See if user prefers to start the timer on startup - Note: make sure this is
      the last preference called or other preferences may not be set when program
      control jumps to StartTimerAction }
     if INISettingsFile.ReadBool( 'Options', 'StartTimerOnStartup', False) then
       StartTimerActionExecute( Self );

     {Set Main Form Is Creating Flag}
     TMMain.MainFormIsCreating := FALSE;
  end;
end;

//*****************************************************************************

procedure TTSMainForm.FormClose(Sender: TObject; var Action: TCloseAction);
{This procedure readies the application for closing}
var
  INISettingsFile : TIniFile;
begin
  if Assigned( TSReportForm ) then
    TSReportForm.Free;

  if Assigned( TMUtils.TSGProjects ) then
    TSGProjects.Free;

  if ( CurrentState in [csClosed] ) or ( TSGrid.TSClientDataSet.FieldCount = 0 ) then
    Exit;

  {Get INI File location}
  INISettingsFile := TIniFile.Create(TMUtils.GetINIFilePath);
  try
    {Write current options to INI File}
    INISettingsFile.WriteString(  'Options', 'DefaultRate', FloatToStrF(DefaultProject.HourlyRate, ffFixed, 5, 2));
    INISettingsFile.WriteInteger( 'Options', 'RemindTime',  TSMainForm.RemindTimeSpinEdit.Value );
    INISettingsFile.WriteBool(    'Options', 'Remind',      TSMainForm.RemindAction.Checked );

    {Write MRU List to INI File}
//mru     MRUManager.SaveToIni(INISettingsFile, 'MRUList');
  finally
    INISettingsFile.Free;
  end;

  {Save the current grid and form layouts as preferences if not in reduced mode}
  if ReducedModeAction.Checked = FALSE then
    TMUtils.SavePreferedLayout;
end; {procedure TTSMainForm.FormClose}

//*****************************************************************************

procedure TTSMainForm.SetAutoSavePreference( Enabled: Boolean; Interval: Integer );
{ Update the auto save settings
}
begin
   AutoSaveTimer.Enabled := Enabled;
   if AutoSaveTimer.Enabled then
     AutoSaveTimer.Interval := Interval * 60000;

   if Enabled then
     SaveProjectAction.Hint := Format( '%s, autosave every %d minutes',
                               [ StringReplace( SaveProjectAction.Caption, '&', '', [rfReplaceAll] ), Interval ] )
   else
     SaveProjectAction.Hint := Format( '%s, autosave is OFF',
                               [ StringReplace( SaveProjectAction.Caption, '&', '', [rfReplaceAll] ) ] );
end;

// Don't step into the following code.
{$DEBUGINFO OFF}
{$LOCALSYMBOLS OFF}
procedure TTSMainForm.TSStatusBarDrawPanel(StatusBar: TStatusBar; Panel: TStatusPanel; Rect: TRect);
{ Owner draw the status panel, coloured for timing state
}
const
  Alignments: array[TAlignment] of Word=(DT_LEFT, DT_RIGHT, DT_CENTER);
var
  R: TRect;
  PnlColor, FntColor: TColor;
begin
  inherited;
  R        := Rect;
  PnlColor := StatusBar.Color;
  FntColor := StatusBar.Font.Color;

  if (Panel.Index = 1) then
    case CurrentState of
      csTiming :
        begin
          pnlColor := clRed;
          fntColor := clWhite;
        end;
      csSlacking:
        begin
          fntColor   := clWindowText;
          pnlColor   := clYellow;
        end;
      csStopped:
        begin
          fntColor   := clWhite;
          pnlColor   := clGreen;
        end;
    end; { case }

  with StatusBar.Canvas do
    begin
      Font.Name   := StatusBar.Font.Name;
      Font.Style  := StatusBar.Font.Style;
      Font.Size   := StatusBar.Font.Size;
      Font.Color  := FntColor;
      Brush.Color := PnlColor;
    end;

  StatusBar.Canvas.FillRect(R);
  DrawText(StatusBar.Canvas.Handle, (Panel.Text), -1, R,
           Alignments[Panel.Alignment] or DT_VCENTER
           or DT_SINGLELINE or DT_END_ELLIPSIS);
end;
{$DEBUGINFO ON}
{$LOCALSYMBOLS ON}

procedure TTSMainForm.UpdateStatusPanel;
{ Update the status bar
}
var
  RecordStatusStr: String;
begin
  with TSStatusBar do
    begin
      Panels[0].Text := VersionInfoStr;

      case TMUtils.CurrentState of
        csTiming   : TSStatusBar.Panels[1].Text := 'TIMING (' + TMUtils.FormatTimeTotal( TMUtils.ElapsedTime) + ')';
        csSlacking : TSStatusBar.Panels[1].Text := 'SLACKING (' + TMUtils.FormatTimeTotal( TMUtils.ElapsedTime) + ')';
        csStopped  : TSStatusBar.Panels[1].Text := 'Stopped.';
        csClosed   : TSStatusBar.Panels[1].Text := '';
      end;

      // The second cell displays the state; either a specific string like "editing preferences..."
      // or the current record selection and modified state.
      RecordStatusStr := FStatusText;
      if ( FStatusText = '' ) and RecordIsChanged then
        RecordStatusStr := '*Modified ';
      if  ( FStatusText = '' )
      and TsData.HasData then
      begin
        if ReducedModeAction.Checked and CurrentProject.IsProjectGroup then
          RecordStatusStr := TSGrid.ActiveTask.ProjectCode
        else if ReducedModeAction.Checked then
          RecordStatusStr := TSGrid.ActiveTask.Notes
        else
          RecordStatusStr := RecordStatusStr + Format( '%d of %d',
                          [ TSGrid.ActiveRow, TSGrid.TSClientDataset.RecordCount ]);
      end;

      Panels[2].Text := RecordStatusStr;
      if ( CurrentProject.ProjectName = '' ) then
        Panels[3].Text := ''
      else
        Panels[3].Text := 'Project: ' + CurrentProject.ProjectName;
      Invalidate;
    end;
end;


procedure TTSMainForm.StatusTimerTimer(Sender: TObject);
{This procedure gives the user a current status of how much time has elapsed
 within the current timing period, without requiring the user to stop the
 timer.}
var
     TimeString,
     TimerStatus,
     ReverseTimerStatus : string;
begin
   if ShowTimerInTrayCaption = TRUE then
     begin
       TimeString         := TMUtils.FormatTimeTotal( TMUtils.ElapsedTime );
       TimerStatus        := 'TIMING (' + TimeString + ')';
       ReverseTimerStatus := '(' + TimeString + ') TIMING';

       {Set tray item and tray icon captions}
       Application.Title          := ReverseTimerStatus + ' ' + CurrentProject.FileName;
//tray            ApplicationRxTrayIcon.Hint := TimerStatus + ' - ' + TMMain.CurrentFileName;

       if FlashColorsWhileTiming = TRUE then
          begin
               if TrayIconIsRedFlag = TRUE then
                  begin
//tray                             TSMainForm.ApplicationRxTrayIcon.Icon := TSMainForm.ApplicationRxTrayIcon.Icons[3];
                       TrayIconIsRedFlag := FALSE;
                  end
               else
                  begin
                      if ( CurrentState <> csSlacking ) then
//tray                                TSMainForm.ApplicationRxTrayIcon.Icon := TSMainForm.ApplicationRxTrayIcon.Icons[2]
                       else
//tray                                TSMainForm.ApplicationRxTrayIcon.Icon := TSMainForm.ApplicationRxTrayIcon.Icons[1];
                       TrayIconIsRedFlag := TRUE;
                  end; {if TrayIconIsRedFlag = TRUE}
          end; {if FlashColorsWhileTiming = TRUE}

   end; {if ShowTimerInTrayCaption = TRUE}
   UpdateStatusPanel;
end; {procedure TTSMainForm.StatusTimerTimer}

//*****************************************************************************

procedure TTSMainForm.ApplicationRxTrayIconClick(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
     if TMMain.TrayIconClickRestores = TRUE then
        {Perform action to restore main form}
        begin
             Application.Restore;
             Application.BringToFront;
        end {if TMMain.TrayIconClickRestores}
     else
        {Perform action to start/stop timer}
        begin
             if ( CurrentState = csSlacking ) then
                begin
                     TSMainForm.StartTimeToolButton.Click;
                end
             else
                begin
                     TSMainForm.SlackTimeToolButton.Click;
                end; {if SlackTimeToolButtonState}
        end; {if TMMain.TrayIconClickRestores = TRUE}
end; {procedure TTSMainForm.ApplicationRxTrayIconClick}

//*****************************************************************************

procedure TTSMainForm.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
{This procedure ensures that a Windows shutdown will not result in a
 loss of data by the saved-file-status not being checked by user.
 Also, when application is closed while timing is active, timers
 will be shut off before file is saved and program exits.}
begin
  if ( CurrentState in [csClosed] ) or ( TSGrid.TSClientDataSet.FieldCount = 0 ) then
    Exit;

   CanClose := FALSE;

   if (CurrentState in tsTiming) then
     StopTimerActionExecute( Sender );

   TMUtils.VerifyFileSavedStatus;

   {If user hits Cancel in the Save before Closing dialog, do not
    exit the system.}
   CanClose := TMMain.ResumeClose;

   if CanClose = FALSE then
      begin
           TMUtils.AdjustAlwaysOnTop(0);
      end;
end; {procedure TTSMainForm.FormCloseQuery}

//*****************************************************************************

procedure TTSMainForm.RestorePopupMenuItemClick(Sender: TObject);
{Restore application}
begin
     Application.Restore;
end; {procedure TTSMainForm.RestorePopupMenuItemClick}

//*****************************************************************************

procedure TTSMainForm.MinimizePopupMenuItemClick(Sender: TObject);
{Minimize Application}
begin
     Application.Minimize;
end; {procedure TTSMainForm.MinimizePopupMenuItemClick}

//*****************************************************************************

procedure TTSMainForm.AutoSaveTimerTimer(Sender: TObject);
{This procedure handles AutoSaving when the AutoSave timer is activated and
 fires}
begin
   if  ( RecordIsChanged or (CurrentState in tsTiming) )
   and ( TSGrid.TSClientDataset.RecordCount > 0 ) then
     SaveProjectActionExecute( Self );
end;

{procedure TTSMainForm.AutoSaveTimerTimer}

//*****************************************************************************

procedure TTSMainForm.MRUManagerClick(Sender: TObject; const RecentName,
  Caption: String; UserData: Integer);
{This procedure handles the loading of a file clicked from the Most Recently
 Used list in the file menu.}
begin
     TMUtils.VerifyFileSavedStatus;

     {Clear string grid and summary statistics}
     TMUtils.ClearProject;

     {Set Open dialog name to MRU list filename and open}
     TSMainForm.OpenFileDialog.FileName := GetLongHint(RecentName);

     {Open file}
     if UpperCase(trim(ExtractFileExt(TSMainForm.OpenFileDialog.FileName))) = '.SA2' then
        begin
             TMUtils.OpenDataFile(TSMainForm.OpenFileDialog.FileName, 'OPEN', 2);
        end
     else
         MessageDlg('The last-saved file has an invalid extension.',mtError,[mbOK],0);
end;

procedure TTSMainForm.NewFromSelectedActionExecute(Sender: TObject);
{ Create a new timing task, copying values from the selected record
}
var
  TsNewRecord: TimeRecord_SA2;
  TsSelectedRecord: TimeRecord_SA2;
begin
  TsSelectedRecord := TsData.ActiveRecordSA2;
  if TsSelectedRecord.IsValid then
  begin
    if (CurrentState in [csTiming, csSlacking]) then
      StopTimerActionExecute(Self);

    TsNewRecord.CloneAsNew(TsSelectedRecord, now);
    TsData.Append(TsNewRecord);

    TMUtils.TimingRecord.StartTiming(TsNewRecord);
    SetAsTiming;
    UpdateGUI;
  end;
end;

{procedure TTSMainForm.MRUManagerClick}

//*****************************************************************************

procedure TTSMainForm.FormShow(Sender: TObject);
{This procedure handles the FormShow event tasks.}
begin
  InitForm;

  {Show version number in Status Bar}
  with System.Diagnostics.FileVersionInfo.GetVersionInfo( Application.ExeName ) do
    VersionInfoStr := Format( 'Version %d.%d.%d-%d',
      [ FileMajorPart, FileMinorPart, FileBuildPart, FilePrivatePart { %s ProductVersion} ] );
  UpdateStatusPanel;
end; {procedure TTSMainForm.FormShow}

//*****************************************************************************

procedure TTSMainForm.ApplicationMinimizeHandler(Sender: TObject);
{This procedure takes the task tray preference into account when application
 is minimized.}
begin
     if TMMain.ShowTrayIcon = '1' then
        begin
             if TMMain.HideTaskButton = TRUE then
                begin
                     showwindow(application.handle, sw_hide);
                end;
        end;
end; {procedure TTSMainForm.ApplicationMinimizeHandler}

//*****************************************************************************

procedure TTSMainForm.ApplicationRestoreHandler(Sender: TObject);
{This procedure controls application restore behavior}
begin
     if TMMain.ShowTrayIcon = '1' then
        begin
             if TMMain.HideTaskButton = TRUE then
                begin
                     ShowWindow(Application.Handle, sw_hide);
                end
             else
                begin
                     ShowWindow(Application.Handle, sw_showdefault);
                end;
        end
     else
        begin
             ShowWindow(Application.Handle, sw_showdefault);
        end;
end; {procedure TTSMainForm.ApplicationRestoreHandler}

//*****************************************************************************

procedure TTSMainForm.CreateProjectActionExecute(Sender: TObject);
{This procedure controls application behavior for creating a new task log file}
begin
  TMUtils.VerifyFileSavedStatus;
  RecordIsChanged               := FALSE;
  TMUtils.ClearProject;

  CurrentProject.IsProjectGroup := FALSE;
  SetTimingState( csOpened );
  TSGrid.LoadColumnSettings;
  UpdateGui;
end; {procedure TTSMainForm.CreateLogActionExecute}

//*****************************************************************************

procedure TTSMainForm.CreateGroupActionExecute(Sender: TObject);
{ Create a new .TSG file and add projects to it}
begin
  TMUtils.VerifyFileSavedStatus;

  TMUtils.ClearProject;
  StatusText := 'Creating new project group';

  SaveFileDialog.Filter   := 'TimeStamp Project Group (*.TSG)|*.tsg';
  SaveFileDialog.FilterIndex := 0;

  if SaveFileDialog.Execute then
  begin
    TMUtils.SaveProjectOptions( SaveFileDialog.FileName, DefaultProject );
    TMUtils.OpenProjectGroup( SaveFileDialog.FileName );

    with TPreferencesForm.Create(Application) do
    try
      PageControl1.ActivePage := ProjectOptionsTabsheet;
      ShowModal;
    finally
      Free;
    end;
  end; {if SaveFileDialog.Execute}

  StatusText :=  '';
end;

//*****************************************************************************

procedure TTSMainForm.OpenProjectActionExecute(Sender: TObject);
{This procedure controls application behavior for opening a task log file}
begin
   StatusText :=  'Open Project...';
   TMUtils.VerifyFileSavedStatus;

   TMUtils.AdjustAlwaysOnTop(1);
   if OpenFileDialog.Execute then
     TMUtils.OpenTimeRecordDataFile( OpenFileDialog.FileName );

   StatusText :=  '';
   TMUtils.AdjustAlwaysOnTop(0);
   TSMainForm.TSGrid.Invalidate;
end; {procedure TTSMainForm.OpenLogActionExecute}

//*****************************************************************************

procedure TTSMainForm.AppendProjectActionExecute(Sender: TObject);
{This procedure handles the appending of a loaded log file to the tasks
 already in the grid.}
begin
   {Allow adjustment for "Always On Top"}
   TMUtils.AdjustAlwaysOnTop(1);

   StatusText :=  'Append Project...';
   TMUtils.AppendTimeRecordDataFile;
   StatusText := '';
   TSMainForm.TSGrid.Invalidate;

   {Restore "Always On Top" setting}
   TMUtils.AdjustAlwaysOnTop(0);
end; {procedure TTSMainForm.AppendLogActionExecute}

//*****************************************************************************

procedure TTSMainForm.SaveProjectActionExecute(Sender: TObject);
{This procedure controls application behavior for saving a task log file}
var
   AutoSaveEnabled : boolean;
begin
   {Stop AutoSaveTimer if it is enabled}
   AutoSaveEnabled       := AutoSaveTimer.Enabled;
   AutoSaveTimer.Enabled := FALSE;

   {Place Saving message on status bar}
   StatusText :=  'Saving...';

   {Try to point user to file name of last opened file.  If this is a "Save
    As" function, do not use the option of QuickSave.}
   if  ( Sender <> SaveProjectAsAction )
   and ( CurrentProject.FileName <> '') then
     TMUtils.SaveTimeRecordDataFile( CurrentProject.FileName )
   else
      begin
         {Allow adjustment for "Always On Top"}
         TMUtils.AdjustAlwaysOnTop(1);

         TMUtils.SaveTimeRecordDataFileAs;

         {Restore "Always On Top" setting}
         TMUtils.AdjustAlwaysOnTop(0);
      end; {if (Sender <> SaveLogFileAsMenuItem)}

   {Clear status planel message}
   StatusText :=  '';

   {Restart AutoSaveTimer if it is enabled}
   AutoSaveTimer.Enabled := AutoSaveEnabled;
end; {procedure TTSMainForm.SaveLogActionExecute}

//*****************************************************************************

procedure TTSMainForm.ExportASCIILogActionExecute(Sender: TObject);
{This procedure launches the ASCII export process}
begin
   {Allow adjustment for "Always On Top"}
   TMUtils.AdjustAlwaysOnTop(1);

   TMUtils.ExportTimeRecordDataFile;

   {Restore "Always On Top" setting}
   TMUtils.AdjustAlwaysOnTop(0);
end; {procedure TTSMainForm.ExportLogActionExecute}

//*****************************************************************************

procedure TTSMainForm.PrintSetupActionExecute(Sender: TObject);
{This procedure displays the Print Setup dialog}
begin
   {Allow adjustment for "Always On Top"}
   TMUtils.AdjustAlwaysOnTop(1);

   StatusText :=  'Print Setup...';
   PrinterSetupDialog.Execute;
   StatusText :=  '';

   {Restore "Always On Top" setting}
   TMUtils.AdjustAlwaysOnTop(0);
end; {procedure TTSMainForm.PrintSetupActionExecute}

//*****************************************************************************

procedure TTSMainForm.PrintLogActionExecute(Sender: TObject);
{ This procedure calls the report generator
  Don't show the report dialog if chosen from the toolbar
}
begin
  PrintLog( not(Sender is TToolButton), False );
end; {procedure TTSMainForm.PrintLogActionExecute}


procedure TTSMainForm.PrintPreviewActionExecute(Sender: TObject);
{ This procedure handles the launching of a Print Preview dialog.
  Don't show the report dialog if chosen from the toolbar
}
begin
  PrintLog( not(Sender is TToolButton), True );
end; {procedure TTSMainForm.PrintPreviewActionExecute}

procedure TTSMainForm.PrintPreviewToolButtonClick(Sender: TObject);
{ This procedure handles the launching of a Print Preview dialog from the toolbar
  as the above ... sender is tobject in .net
  So, don't show the report dialog if chosen from the toolbar
}
begin
  PrintLog( False, True );
end;

procedure TTSMainForm.PrintLog(bShow, bPreview: Boolean);
{ This procedure handles the launching of a Print/Preview dialog.}
begin
  TMUtils.AdjustAlwaysOnTop(1);
  bShow := bShow or not Assigned( TSReportForm );  { TODO: if project changed }
  if not Assigned( TSReportForm ) then
    TSReportForm := TTSReportForm.Create( Self, TSGrid );

  StatusText :=  'Printing...';
  try
    TSReportForm.Execute( bShow, bPreview );
  finally
    StatusText :=  '';
  end;
  TMUtils.AdjustAlwaysOnTop(0);
end;


//*****************************************************************************

procedure TTSMainForm.PreferencesActionExecute(Sender: TObject);
{This procedure launches the Preferences dialog}
begin
   TMUtils.AdjustAlwaysOnTop(1);

   StatusText :=  'Edit Preferences...';
   PreferencesForm := TPreferencesForm.Create(Application);
   try
     PreferencesForm.ShowModal;
   finally
     PreferencesForm.Free;
     StatusText :=  '';
   end;

   TMUtils.AdjustAlwaysOnTop(0);
end; {procedure TTSMainForm.PreferencesActionExecute}

//*****************************************************************************

procedure TTSMainForm.ExitActionExecute(Sender: TObject);
{ This procedure simply tells the application to Close when asked to}
begin
   Close;
end; {procedure TTSMainForm.ExitActionExecute}

//*****************************************************************************

procedure TTSMainForm.EditTaskActionExecute(Sender: TObject);
{ This procedure handles the editing of a task with the Edit Task Dialog
}
var
  TaskEditDialog: TTaskEditDialog;
  UpdateTask    : TimeRecord_SA2;
  PreviousTask  : TimeRecord_SA2;
  IsTimingRecord: Boolean;
begin
  if TSGrid.TSClientDataset.RecordCount < 1 then
    begin
       MessageDlg('No record is selected.',mtError,[mbOK],0);
       Exit;
    end;

  IsTimingRecord := ( TSGrid.IsTimingRow ) and ( CurrentState in tsTiming );
  if IsTimingRecord then
    TSGrid.PostChanges; // Save notes and update the time

   StatusText :=  'Edit Task...';

   TaskEditDialog := TTaskEditDialog.Create(Application);
   TMUtils.AdjustAlwaysOnTop(1);
   TRY
     UpdateTask := TSGrid.ActiveTask;

     if not TsData.Bof then
     begin
       TsData.ADODataSet.DisableControls;
       try
         TsData.Prior;
         PreviousTask := TSGrid.ActiveTask;
         TsData.Next;
       finally
         TsData.ADODataSet.EnableControls;
       end;
     end;
         
     if TaskEditDialog.Execute(UpdateTask, PreviousTask, IsTimingRecord ) then
       TSGrid.ActiveTask := UpdateTask;

     // Re-synchronise any changes back into the timing record
     if IsTimingRecord then
       TMUtils.TimingRecord.TimeRecord := UpdateTask;

   FINALLY
     TMUtils.AdjustAlwaysOnTop(0);
     TaskEditDialog.Free;
     StatusText :=  '';
   END;
end; {procedure TTSMainForm.EditTaskActionExecute}

//*****************************************************************************

procedure TTSMainForm.DeleteTaskActionExecute(Sender: TObject);
{This procedure handles the deletion of a task record}
begin
   StatusText :=  'Delete Task...';
   TMUtils.AdjustAlwaysOnTop(1);

   if MessageDlg('Selected task will be deleted. Are you sure?',
                  mtWarning, [mbYes, mbNo], 0) = mrYes then
      TSGrid.DeleteRow;

   {Restore "Always On Top" setting}
   TMUtils.AdjustAlwaysOnTop(0);

   StatusText :=  '';
end; {procedure TTSMainForm.DeleteTaskActionExecute}

//*****************************************************************************

procedure TTSMainForm.ClearNotesActionExecute(Sender: TObject);
{This procedure provides the user with a quick way to erase the Notes for
 the selected task.}
var
  UpdateTask : TimeRecord_SA2;
begin
  UpdateTask        := TSGrid.ActiveTask;
  UpdateTask.Notes  := '';
  TSGrid.ActiveTask := UpdateTask;
end; {procedure TTSMainForm.ClearNotesActionExecute}

//*****************************************************************************

procedure TTSMainForm.StartTimerActionExecute(Sender: TObject);
{ This procedure controls application behavior when the Start timing command
  is issued.
  JH - Altered this action. It re/starts timing a task, but no longer has
  anything to do with stopping it.
}
var
   DefaultShortDateFormat : string;
   UpdateTask             : TimeRecord_SA2;
begin
   {Store user's setting for date format.  Date to Time String conversions
    require the mm/dd/yy format, but international settings may have this
    set differently.}
   DefaultShortDateFormat := ShortDateFormat;

   if TMUtils.CurrentState = csTiming then {Display end date/time and difference}
      { This section executes when STOP has been clicked }
     StopTimerActionExecute( Sender )
   else begin
     { Executes when we start (and/or continue) timing a task }
     if ( TMUtils.CurrentState <> csSlacking ) then
      begin
        TMUtils.ClearTimeRecord( TMUtils.TimingRecord.TimeRecord );
        TMUtils.TimingRecord.TimeRecord.StartTime := now;
        TMUtils.TimingRecord.SessionStartTime     := now;
        if CurrentProject.IsProjectGroup
        and ( TMUtils.TimingRecord.TimeRecord.ProjectCode = '' ) then
          TMUtils.TimingRecord.TimeRecord.ProjectCode := TSGrid.ActiveTask.ProjectCode;
        TsData.Append( TMUtils.TimingRecord.TimeRecord );
      end;
     SetAsTiming;
   end;
   UpdateGUI;
end; {procedure TTSMainForm.StartTimerActionExecute}

procedure TTSMainForm.ContinueTimeActionExecute(Sender: TObject);
{ Continue timing a task
}
begin
  if ( TSGrid.ActiveRow > 0 ) and not ( CurrentState in tsTiming ) then
  begin
    TMUtils.ClearTimeRecord( TMUtils.TimingRecord.TimeRecord );
    TMUtils.TimingRecord.TimeRecord        := TSGrid.ActiveTask;
    //TMUtils.TimingRecord.TimeRecordEndTime := now;
    TMUtils.TimingRecord.SessionStartTime  := now;
    if CurrentProject.IsProjectGroup
    and ( TMUtils.TimingRecord.TimeRecord.ProjectCode = '' ) then
      TMUtils.TimingRecord.TimeRecord.ProjectCode := TSGrid.ActiveTask.ProjectCode;
    SetAsTiming;
    UpdateGUI;
  end;
end;

procedure TTSMainForm.SetAsTiming;
{ Sets all the state and display flags into timing mode
}
begin
  {Set Change flag}
  StatusText := '';
  StartTimerAction.Checked := True;
  StartTimeToolButton.Down := True;
  TMUtils.CurrentState := csTiming;
  TSGrid.TaskDBGrid.Invalidate;

  {Set Tray Icon Color flag}
  TrayIconIsRedFlag := TRUE;

  {Start the Status Timer}
  {if Preference to show time in Task Bar caption is set, enable timer}
  if ShowTimerInTrayCaption = TRUE then
    StatusTimer.Enabled := TRUE;
end;

//*****************************************************************************

procedure TTSMainForm.StartSlackActionExecute(Sender: TObject);
{ This procedure controls application behavior when the Slack command
  is issued.
  JH - Altered. This starts (and *only* starts) slack time.
       Use stoptimeraction to stop timing.
       Slacking can only occur if we're already timing.
}
begin
  Assert( CurrentState = csTiming, 'Cannot start slack timer without a record.' );
  if ( CurrentState <> csTiming ) then
    Exit;

  StatusText := '';
  { Add the timed amount to the stored amount }
  TMUtils.SetTimingState( csSlacking );

  {Set Change flag}
  RecordIsChanged := TRUE;

  {If user has specified the starting of the screen saver with the
   slack timer as a preference, start it.}
  if StartScreenSaverWhenSlackStarts = TRUE then
    SendMessage(Handle, WM_SYSCOMMAND, SC_SCREENSAVE, 0)
  else
    TSGrid.TaskDBGrid.Invalidate;

  UpdateGUI;
end; {procedure TTSMainForm.StartSlackActionExecute}

procedure TTSMainForm.StopTimerActionExecute(Sender: TObject);
{ Stop timing, whether slacking or normal
}
var
  UpdateTask : TimeRecord_SA2;
begin
  if ( TSGrid.TSClientDataSet.State in [dsEdit, dsInsert] ) then
  TRY
    TSGrid.TSClientDataSet.Post;
  EXCEPT
    TSGrid.TSClientDataSet.Cancel;
  END;

  {Turn off Status Timer}
  StatusTimer.Enabled := FALSE;
  StatusText          := '';
  SetTimingState( csStopped );

  { Read any dynamic values from the grid }
  {Set hourly rate here in case user changed it in the middle of a timing cycle.
   Do not write value if this item was the result of a Continue Time function
   because the older hourly rate must stay the same.}
  TMUtils.TimingRecord.TimeRecord.Notes := TSGrid.TimingTask.Notes;
  if not TMUtils.TimingRecord.ContinueTime then
    TMUtils.TimingRecord.TimeRecord.HourlyRate := CurrentProject.HourlyRate;

  { Write the temporary memory record into the clientdataset record }
  TSGrid.TimingTask := TMUtils.TimingRecord.TimeRecord;

//tray             ApplicationRxTrayIcon.Hint := 'Time Stamp - ' + TMMain.CurrentFileName + ' - Click to Start/Stop';
  UpdateGUI;
end;

//*****************************************************************************

procedure TTSMainForm.SetDefaultHourlyRateActionExecute(Sender: TObject);
{This procedure presents the user with an input box to enter the default
 hourly charge rate for a task.}
var
  HourlyRateString : string;
  UpdateTask       : TimeRecord_SA2;
begin
  {Allow adjustment for "Always On Top"}
  TMUtils.AdjustAlwaysOnTop(1);

  StatusText :=  'Edit Hourly Rate...';

  HourlyRateString := FloatToStrF(CurrentProject.HourlyRate, ffFixed, 5, 2);

  if InputQuery('Hourly Rate','Enter a new Hourly Rate:', HourlyRateString) then
     begin
          {Set HourlyRate for record AND for TMMain variable so that a global
           value can be kept while at the same time enabling different rows
           to have different hourly rates.}
          try
             CurrentProject.HourlyRate := StrToFloat(HourlyRateString);
             DefaultProject.HourlyRate := StrToFloat(HourlyRateString);
             if ( TSGrid.TSClientDataSet.RecordCount > 0 ) then
             begin
               UpdateTask            := TSGrid.ActiveTask;
               UpdateTask.HourlyRate := CurrentProject.HourlyRate;
               TSGrid.ActiveTask     := UpdateTask;
             end;

          except
                on EConvertError do
                   begin
                        ShowMessage('You did not enter a valid hourly rate.');
                   end
          end; {try}

     end; {if Dialogs.InputQuery}

  TMUtils.AdjustAlwaysOnTop(0);
  StatusText :=  '';
end; {procedure TTSMainForm.SetHourlyRateActionExecute}

//*****************************************************************************

procedure TTSMainForm.SetProjectTitleActionExecute(Sender: TObject);
{ This procedure inputs the Project Title text from the user.}
var
  InputText : string;
begin
  {Allow adjustment for "Always On Top"}
  TMUtils.AdjustAlwaysOnTop(1);

  {Pop up InputQuery box for user to type in a project title}
  InputText := TMUtils.CurrentProject.ProjectName;

  if InputQuery('Project Name', 'Enter the name of this project:', InputText) then
  begin
    TMUtils.CurrentProject.ProjectName := InputText;
    RecordIsChanged     := TRUE;
  end; {if InputQuery}

  {Restore "Always On Top" setting}
  TMUtils.AdjustAlwaysOnTop(0);
end; {procedure TTSMainForm.SetProjectTitleActionExecute}

//*****************************************************************************

procedure TTSMainForm.ReducedModeActionExecute(Sender: TObject);
{This procedure handles the switch to and from Reduced Mode}
begin
  if not System.Diagnostics.Debugger.IsAttached then
    LockWindowUpdate( GetDesktopWindow() );
  try
    TSGrid.TaskDBGrid.Visible    := not ReducedModeAction.Checked;
    TSGrid.CalendarPanel.Visible := not ReducedModeAction.Checked;
    TSGrid.NoteEditPanel.Visible := not ReducedModeAction.Checked;
    RemindTimerPanel.Visible     := RemindAction.Checked and not ReducedModeAction.Checked;
    TSStatusBar.SizeGrip         := not ReducedModeAction.Checked;

    {De/activate Task Edit Functions}
    UpdateGUI;

    if ReducedModeAction.Checked then
    begin
      {Get the current window state so if it is maximized we can set
       the reduced size properly, as well as put it back properly.}
      ApplicationWindowState := Self.WindowState;
      if ApplicationWindowState = wsMaximized then
        Self.WindowState := wsNormal;

      {Save current Main Form sizes}
      CurrentFormWidth  := Self.Width;
      CurrentFormHeight := Self.Height;

      CurrentMainToolBarLocation := MainToolBar.Left;
      MainToolBar.Left := 0;

      {Deactivate option to Maximize}
      //Self.BorderIcons := [biSystemMenu, biMinimize]; // .NET mnu_0 exists error?

      if TimeStampMainControlBar.Visible then
        Self.ClientHeight := MainToolBar.Height + TSGrid.TimingStatusHostPanel.Height + 26
      else
        Self.ClientHeight := TSGrid.TimingStatusHostPanel.Height + 26;
      Self.ClientWidth  := MainToolBar.Width + 20;

    end
    else begin
      if ApplicationWindowState = wsMaximized then
        Self.WindowState := wsMaximized;

      Self.Width  := CurrentFormWidth + 2;
      Self.Height := CurrentFormHeight;

      MainToolBar.Left := CurrentMainToolBarLocation;
      {Restore option to Maximize}
      //Self.BorderIcons := [biSystemMenu, biMinimize, biMaximize];
    end;

    Self.Invalidate;
  finally
    LockWindowUpdate( 0 );
  end;
end; {procedure TTSMainForm.ReducedModeActionExecute}

//*****************************************************************************

procedure TTSMainForm.AddProjectActionExecute(Sender: TObject);
var
  AProjectCode: String;
begin
  if CurrentProject.IsProjectGroup then
  begin
    if InputQuery('Project Code Name', 'New project code: ', AProjectCode) and (AProjectCode <> '') then
    begin
      TMUtils.AddTSGProject(AProjectCode);
      TSGrid.LoadProjectComboBox;
      TSGrid.LoadProjectPickList;
    end;
  end
  else
    AppendProjectActionExecute(Sender);
end;

procedure TTSMainForm.AlwaysOnTopActionExecute(Sender: TObject);
{This procedure toggles the application to "Always on Top" mode}
begin
     {Reverse current OnTop state}
     OnTopToggle := not(OnTopToggle);

     AlwaysOnTopAction.Checked := OnTopToggle;

     if OnTopToggle = TRUE then
        begin
             TSMainForm.FormStyle := fsStayOnTop;
        end
     else
        begin
             TSMainForm.FormStyle := fsNormal;
        end; {if OnTopToggle = TRUE}
end; {procedure TTSMainForm.AlwaysOnTopActionExecute}

//*****************************************************************************

procedure TTSMainForm.HelpContentsActionExecute(Sender: TObject);
{This procedure pulls up the Help File's Table of contents (.CNT file) }
begin
    WinHelp( Handle, (Application.HelpFile), HELP_FINDER, 0);
end; {procedure TTSMainForm.HelpContentsActionExecute}

//*****************************************************************************

procedure TTSMainForm.VersionHistoryActionExecute(Sender: TObject);
{This procedure displays the Version History section of the Help File. }
begin
     Application.HelpContext(2);
end; {procedure TTSMainForm.VersionHistoryActionExecute}

//*****************************************************************************

procedure TTSMainForm.LicenseInfoActionExecute(Sender: TObject);
{This procedure displays the License Information section of the Help File}
begin
     Application.HelpContext(3);
end; {procedure TTSMainForm.LicenseInfoActionExecute}

//*****************************************************************************

procedure TTSMainForm.AboutActionExecute(Sender: TObject);
{ This procedure launches the About box }
begin
   with TAboutBoxDialog.Create( Self ) do
   try
     ShowModal;
   finally
     Free;
   end;
end; {procedure TTSMainForm.AboutActionExecute}

//*****************************************************************************

procedure TTSMainForm.RegistrationInfoHelpMenuItemClick(Sender: TObject);
{This procedure displays a message regarding voluntary donations.}
begin
   ShowMessage ('Time Stamp is officially donationware, and you may use it and' + Chr(13) +
               'copy it for other people as you see fit.  There is no formal' + Chr(13) +
               'registration procedure.' + Chr(13) + Chr(13) +
               'HOWEVER,' + Chr(13) + Chr(13) +
               'If you find Time Stamp particularly useful and wish to' + Chr(13) +
               'contribute to its maintenance, freeware status, and web' + Chr(13) +
               'site domain fees, feel free to send any amount you feel' + Chr(13) +
               'is appropriate to the address listed on the "Contact Info"' + Chr(13) +
               'link on the web site.' + Chr(13) + Chr(13) +
               'Payment is not a prerequisite for support.' + Chr(13) + Chr(13) +
               'Thanks for your continued support!');
end; {procedure TTSMainForm.RegistrationInfoHelpMenuItemClick}

//*****************************************************************************

procedure TTSMainForm.SetDefaultReportEndNotesActionExecute(Sender: TObject);
{This action defines an InputQuery box that inputs the project's "End
 Notes" field, which optionally appears at the end of a Time Stamp report.}
var
     InputText : string;
     INISettingsFile   : TIniFile;

begin
     {Allow adjustment for "Always On Top"}
     TMUtils.AdjustAlwaysOnTop(1);

     {Pop up InputQuery box for user to type in project end notes}
     InputText := DefaultProject.EndNotes;

     if InputQuery('Report End Notes (for all Projects)', 'Enter the text for your report end notes:', InputText) = TRUE then
        begin
             {Get INI File settings}
             INISettingsFile := TINIFile.Create( TMUtils.GetINIFilePath );
             INISettingsFile.WriteString('Defaults','DefaultProjectEndNotes',InputText);

             if MessageDlg('Do you want set this project''s End Notes to the Default end notes you just entered?',mtInformation,[mbYes, mbNo],0) = mrYes then
                begin
                     {Store input in first record's EndNotes field}
                     TimeRecord.EndNotes     := InputText;
                     CurrentProject.EndNotes := InputText;
                     RecordIsChanged  := TRUE;
                end;

              DefaultProject.EndNotes := InputText;

             {Free INI File}
             INISettingsFile.Free;

        end; {if InputQuery}

     {Restore "Always On Top" setting}
     TMUtils.AdjustAlwaysOnTop(0);
end; {procedure TTSMainForm.SetProjectEndNotesActionExecute}

//*****************************************************************************

procedure TTSMainForm.SetThisProjectsReportEndNotesActionExecute(
  Sender: TObject);
var
     INISettingsFile : TIniFile;
     InputText       : string;
begin
     {Allow adjustment for "Always On Top"}
     TMUtils.AdjustAlwaysOnTop(1);

     {Pop up InputQuery box for user to type in project end notes}
     InputText := CurrentProject.EndNotes;

     if InputQuery('Report End Notes (for this project only)', 'Enter the text for your report end notes:', InputText) = TRUE then
        begin
             {Store input in first record's EndNotes field}
             TimeRecord.EndNotes := InputText;

             if MessageDlg('Do you want to set this project''s end notes to be the default for all projects?',mtInformation,[mbYes, mbNo],0) = mrYes then
                begin
                     DefaultProject.EndNotes := InputText;

                     {Get INI File settings}
                     INISettingsFile := TINIFile.Create(  TMUtils.GetINIFilePath);
                     INISettingsFile.WriteString('Defaults','DefaultProjectEndNotes',InputText);
                     INISettingsFile.Free;
                end;

             CurrentProject.EndNotes := InputText;
             RecordIsChanged  := TRUE;
        end; {if InputQuery}

     {Restore "Always On Top" setting}
     TMUtils.AdjustAlwaysOnTop(0);
end;

//*****************************************************************************

procedure TTSMainForm.CreateDefaultNotesListActionExecute(Sender: TObject);
{ Open the "create/edit default notes" form.
  If the user saves, update both the right click menu and the notes file.
}
begin
  if not Assigned( AutoFillNotes ) then
    LoadAutoFillNotes;

  TMUtils.AdjustAlwaysOnTop(1);
  with TNoteBuilderForm.Create( Self ) do
  try
    if Execute( AutoFillNotes ) then
    begin
      UpdateAutoFillNotes;
      SaveAutoFillNotes;
    end;
  finally
    Free;
  end;
  TMUtils.AdjustAlwaysOnTop(0);
end;

//*****************************************************************************

procedure TTSMainForm.ExportXMLProjectActionExecute(Sender: TObject);
{This procedure launches the XML export process}
begin
   {Allow adjustment for "Always On Top"}
   TMUtils.AdjustAlwaysOnTop(1);

   //TMUtils.ExportXMLFile;
  if XMLFileDialog.Execute then
    TsData.ExportToXML(XMLFileDialog.FileName);
   {Restore "Always On Top" setting}
   TMUtils.AdjustAlwaysOnTop(0);
end; {procedure TTSMainForm.ExportLogActionExecute}

//*****************************************************************************

procedure TTSMainForm.HideToolbarActionExecute(Sender: TObject);
{ This procedure toggles the application to hide/show the toolbar
  JH - Removed a lot of layout code which happens automagically if we use alTop,
       MaxConstraint and Autocheck
}
begin
  {Reverse current OnTop state}
  HideToolbarToggle := not(HideToolbarToggle);

  HideToolbarToolsMenuItem.Checked := HideToolbarToggle;

  TSMainForm.TimeStampMainControlBar.Visible := not(HideToolbarToggle);
end; {procedure TTSMainForm.HideToolbarActionExecute}


procedure TTSMainForm.GNUGPLActionExecute(Sender: TObject);
begin
  Application.HelpContext(18);
end;

procedure TTSMainForm.ResetRemindTimer;
{ The prompt timer runs always, regardless of whether its in use or not
}
begin
  RemindTimer.Enabled := False;
  RemindTimer.Interval:= StrToIntDef( RemindTimeSpinEdit.Text, 15 ) * 60000; // Minutes
  RemindTimer.Enabled := RemindAction.Checked;
end;

procedure TTSMainForm.RemindTimerTimer(Sender: TObject);
{ If the reminder (to enter a note) timer is set, bring us to the front or flash.

  TODO:
  Nicer would be to pop-up a hint window; maar het boeit mee voor geen meter nu.

  If we assume the timing interval is 15 minutes, then we don't want the timer
  to bother us if any of the following conditions is true:
    1. We are currently timing; and the timer was started or altered < 7.5 minutes ago
    2. We are currently timing; and we have edited the timing record in the last 7.5 minutes
    3. We are not timing; but timestamp has been in the foreground in the last 7.5 minutes
}
begin
 OutputDebugString( 'Remind timer triggered' );
 if RemindAction.Checked then
    begin
      TSGrid.FlashReminder := True;
      Application.Restore;
      Application.BringToFront;
    end;
end;


procedure TTSMainForm.SetStatusText(const Value: &String);
begin
  FStatusText := Value;
  UpdateStatusPanel;
end;

procedure TTSMainForm.UpdateGUI;
{ Update the GUI, based on the timing state. This replaces the TMUtils
  equivalent SetStartTimerGUI, SetStopTimerGUI, SetStopSlackTimerGUI
}
begin
   { Actions - timing }
   TSMainForm.StartTimerAction.Enabled       := ( CurrentState <> csTiming );
   TSMainForm.StartSlackAction.Enabled       := ( CurrentState  = csTiming );
   TSMainForm.StopTimerAction.Enabled        := ( CurrentState in tsTiming );

   { Actions - file }
   TSMainForm.CreateProjectAction.Enabled    := ( CurrentState in tsNotTiming );
   TSMainForm.CreateGroupAction.Enabled      := ( CurrentState in tsNotTiming );
   TSMainForm.OpenProjectAction.Enabled      := ( CurrentState in tsNotTiming );
   TSMainForm.AddProjectAction.Enabled       := not ( CurrentState in [csClosed] );
   TSMainForm.SaveProjectAction.Enabled      := not ( CurrentState in [csClosed] );
   TSMainForm.SaveProjectAsAction.Enabled    := not ( CurrentState in [csClosed] );
   TSMainForm.ExportASCIILogAction.Enabled   := not ( CurrentState in [csClosed] );
   TSMainForm.ExportXMLProjectAction.Enabled := not ( CurrentState in [csClosed] );

   { Actions - editing }
   TSMainForm.ClearNotesAction.Enabled       := not (( CurrentState in [csClosed] ) or ReducedModeAction.Checked );
   TSMainForm.EditTaskAction.Enabled         := not (( CurrentState in [csClosed] ) or ReducedModeAction.Checked );
   TSMainForm.DeleteTaskAction.Enabled       := not (( CurrentState in [csClosed] ) or ReducedModeAction.Checked );
   TSMainForm.NewFromSelectedAction.Enabled  := not (( CurrentState in [csClosed] ) or ReducedModeAction.Checked );

   UpdateTitle;
(*tray  {Tray Item Popup Menu Items}
   TSMainForm.StartSlackTimerTrayMenuItem.Enabled   := TRUE;
   TSMainForm.StopTimerTrayMenuItem.Enabled         := TRUE;
   TSMainForm.StartTimerTrayMenuItem.Enabled        := FALSE;
   TSMainForm.ApplicationRxTrayIcon.Icon            := TSMainForm.ApplicationRxTrayIcon.Icons[1];
*)

   UpdateStatusPanel;
end;

procedure TTSMainForm.UpdateTitle;
{ Update the display to in/exclude the project name in the title 
}
var
  TitleStr: &String;
begin
  {Set tray item and tray icon captions}
  if ( CurrentProject.FileName = '' ) or ( CurrentState = csClosed ) then
    TitleStr := 'Time Stamp'
  else
    TitleStr := 'Time Stamp - ' + CurrentProject.FileName;

   Application.Title := TitleStr;
   Self.Caption      := TitleStr;
//zz     TSMainForm.ApplicationRxTrayIcon.Hint := TitleStr;
end;

function TTSMainForm.GetRowCounter: Integer;
begin
  Result := TSGrid.TSClientDataSet.RecordCount;
end;

procedure TTSMainForm.RemindActionExecute(Sender: TObject);
begin
  inherited;
  RemindTimerPanel.Height  := TimeStampMainControlBar.Height;
  RemindTimerPanel.Visible := RemindAction.Checked and not ReducedModeAction.Checked;;
  ResetRemindTimer;
end;

procedure TTSMainForm.RemindTimeSpinEditChange(Sender: TObject);
begin
  ResetRemindTimer;
end;

procedure TTSMainForm.GridPopupPopup(Sender: TObject);
{ Popup menu when right-clicked on grid
}
begin
  ContinueTimeAction.Enabled :=  ( TSGrid.TSClientDataset.RecordCount > 0 )
                             and not ( CurrentState in tsTiming );
end;

procedure TTSMainForm.LoadAutoFillNotes;
{ (Re)populate the auto fill notes list
}
begin
  if not Assigned( AutoFillNotes ) then
    AutoFillNotes := TStringList.Create
  else
    AutoFillNotes.Clear;

  if FileExists(TMUtils.GetAutoFillFilePath) then
    AutoFillNotes.LoadFromFile( TMUtils.GetAutoFillFilePath );

  UpdateAutoFillNotes;
end;

procedure TTSMainForm.UpdateAutoFillNotes;
{ (Re)populate the auto fill notes on the popup menu
}
var
  i   : Integer;
  mnu : TMenuItem;
begin
  // Clear old (note) items
  for i := NotesPopup.Items.Count-1 downto 0 do
    if not Assigned( NotesPopup.Items[i].Action ) then
      NotesPopup.Items.Delete( i );

  // Throw in a separator
  if ( AutoFillNotes.Count > 0 ) then
  begin
    mnu := TMenuItem.Create(Self);
    mnu.Caption := '-';
    NotesPopup.Items.Add( mnu );
  end;

  // Tack on the notes
  for i := 0 to AutoFillNotes.Count - 1 do
  begin
    mnu := TMenuItem.Create( Self );
    begin
      mnu.Caption := AutoFillNotes[i];
      mnu.OnClick := AutoNotePopupClick;
      NotesPopup.Items.Add( mnu );
    end;
  end;
end;

procedure TTSMainForm.SaveAutoFillNotes;
{ Save the autofill notes list to a .not file
}
begin
  if ( AutoFillNotes.Count > 0 ) then
    AutoFillNotes.SaveToFile( TMUtils.GetAutoFillFilePath );
end;

procedure TTSMainForm.AutoNotePopupClick( Sender: TObject );
{ Right-clicking on an autofill note menu item appends it to the current note.
}
var
  Note: String;
begin
  if ( TSGrid.TSClientDataset.RecordCount > 0 ) then
  begin
    if not ( TSGrid.TSClientDataset.State in [dsEdit, dsInsert] ) then
      TSGrid.TSClientDataset.Edit;
    Note := TSGrid.TSClientDataset.FieldByName( 'Notes' ).AsString;
    if ( trim( Note ) <> '' ) then
      Note := Note + #13#10;
     TSGrid.TSClientDataset.FieldByName( 'Notes' ).AsString :=
       Note + StringReplace( ( Sender As TMenuItem ).Caption, '&', '', [rfReplaceAll] );
  end;
end;

procedure TTSMainForm.DoRecordChanged(Row: Integer; FieldValue: String);
{ This event is hooked to changes in grid editing state.
  If this is a projectgroup; update the list of changed projects.
}
begin
  RecordIsChanged := True;
  if CurrentProject.IsProjectGroup then
    TMUtils.FlagProjectAsChanged( FieldValue );
end;

procedure TTSMainForm.SetRecordIsChanged(const Value: Boolean);
begin
  FRecordIsChanged := Value;
  UpdateStatusPanel;
end;

procedure TTSMainForm.RowScrolled(Sender: TObject);
{ Hooked to the AfterScroll event to indicate that the active record has changed
  (the user is scrolling up and down the grid)
}
begin
  UpdateStatusPanel;
end;

procedure TTSMainForm.SetEnableTimerKeyShortcuts(const Value: Boolean);
{ Set up shortcuts for the timer actions
}
begin
  if Value then
    begin
      TSMainForm.StartTimerAction.ShortCut := ShortCut(VK_F9, []);
      TSMainForm.StopTimerAction.ShortCut  := ShortCut(VK_F10, []);
      TSMainForm.StartSlackAction.ShortCut := ShortCut(VK_F11, []);
    end
  else begin
    TSMainForm.StartTimerAction.ShortCut := 0;
    TSMainForm.StopTimerAction.ShortCut  := 0;
    TSMainForm.StartSlackAction.ShortCut := 0;
  end; {EnableTimerKeyShortcuts}
end;

end.



