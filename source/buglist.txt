Bugs
  XML
    - on addrecord agg update
    - on edtrecord agg update
    - save btn enable
    - project add
    

  Update calculated fields / aggs onbeforepost and onsetrecordchanged
  Update display of timing record in display routine

Todo - DerUsability
20110318
  Log on/off tickbox, autofilter, verify save thru filter
  Tasklist applied to notes
  Useful reminder / prompts
  Return to tasklist ods inclusive
  

  Opening a project group after a single project throws grid error (col project not found)

  Reports
     list index error if sort order changed
     sometimes get "object not set to instance of" in printing (and when switched user)

  Read-only columns show as editable in grid when dgediting is on
  XML export fails on my windows 2000 machine, is OK on XP
  Ascii output euro symbol munged
  Cannot "save as" a project group

  + Can't save a new project without at least one record
  + Clearing grid after a project group was open keeps the project group column
    +saving then throws an access violation (null)

  Index ixUnused5 not found on column header grid (text type only)

Todo
  - correct the aggregated totals for a report using a date range (currently does not apply filter)
  - mru
  - tray icon
  - option for 24h display of time (hh:mm:ss) in grid, as per timestamp 3.12

  Reports
    24 hour time
    to pdf (doesn't do anything)
    store report settings to .tsg
    grouping
    howto custom name with column name parameters
    Add custom columns to reports


Non-obvious use:
  - right-click on the grid to continue timing
  - right-click on the notes memo to add a note from the notes list
  - click on a date in the calendar to jump to the first task note for that day.
  - the tool button for print preview only prompts for the report type the first time
    in any session, after that it reuses those settings in the preview and won't prompt


Wishlist
  Custom columns
    - allow editing on the edit form
    - fix bugzilla url click
    - fix header titles to use painted sort order symbols
  Graphs

  Autopreview (dcbgrid) layout for notes

  Popup menu for columns

  Grouped aggregates (project, week, month) <- show in grid; not panel.
                                               Toggle via toolbutton.
  Aggregates for MIN( StartDate ) MAX( EndDate ) and DaysBetween
  Report summary band choices (eg day,week,month)
  Report to PDF
  Export to Excel

  TSG
    - contiguous time blocks
    - graph of project split; or even type (phone calls) split


  Type + rate split
  Client id's
  Journalling hooks from Office for email etc
  Journal by monitoring folder changes

  ADO
    - Integrate w. time and billing
    - Customer name field

===============
Altered List - items abandoned in port from TS3.12

  New config vs inifiles
  .SAV files (ts1)
  Outlook bar
  Reduced mode
  Hypergrid and recordstruct
  Managed sorts
  Record limit
  Form and panel sizings (the hard-coded ones)
  Fully qualified names such as Dialogs.ShowMessage conflict against .Net namespace;
    eg. Borland.Vcl.Dialogs vs TMUtils.Procedure
