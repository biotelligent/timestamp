{
Application:    Time Stamp, Copyright 1997 William Rouck
Module:         TMGridFrme.pas
Contact:        email: wrouck@syntap.com
Modifications:  JM Holloway, June 2004
                justin@schiznit.net

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version. This
program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details. You should have
received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
02111-1307, USA.
}

unit TMGridFrme;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Borland.Vcl.Db, Borland.Vcl.Grids, Borland.Vcl.DBGrids,
  System.ComponentModel, TMUtils, Borland.Vcl.Mask,
  Borland.Vcl.DBCtrls, Borland.Vcl.StdCtrls, Borland.Vcl.ExtCtrls, DateUtils,
  Borland.Vcl.ComCtrls, Borland.Vcl.CommCtrl, Borland.Vcl.Menus, ADONETDb,

  IniFiles,
  TMClientDataTable,
  TMDataModule;

type
  { Bug fixes for TMonthCalendar.
    I didn't receive any feedback on these issues in newsgroups, so never notified quality central.
  }
  TMonthCalendar = class(Borland.Vcl.ComCtrls.TMonthCalendar)
  private
    bMonthChanged : Boolean;
    FOnChange     : TNotifyEvent;
    procedure CNNotify(var Message: TWMNotifyMC); message CN_NOTIFY;
    procedure DoChange;
  public
    constructor Create( AOwner: TComponent ); override;
    property  DateTime;
  published
    property  OnChange:TNotifyEvent read FOnChange write FOnChange;
  end;

  { Scroll record event }
  TOnRecordChanged = procedure( Row: Integer; FieldValue: String ) of object;

  { Dataset and grid frame }
  TTSGrid = class(TFrame)
    TaskDBGrid: TDBGrid;
    NoteEditPanel: TPanel;
    NoteDBMemo: TDBMemo;
    TimingStatusHostPanel: TPanel;
    TimeElapsedContainerPanel: TPanel;
    Label3: TLabel;
    TotalProjectTimeLabel: TLabel;
    Label8: TLabel;
    TotalTimeElapsedPanel: TPanel;
    TotalProjectTimePanel: TPanel;
    TotalWorkTimeElapsedPanel: TPanel;
    CostAndRateContainerPanel: TPanel;
    DayElapsedTimeLabel: TLabel;
    DayProjectTimeLabel: TLabel;
    HourlyRateLabel: TLabel;
    DayElapsedTimePanel: TPanel;
    ProjectTimePanel: TPanel;
    HourlyRatePanel: TPanel;
    TotalWorkTimeElapsedDBText: TDBText;
    AverageHourlyRateDBText: TDBText;
    ReminderFlashLabel: TLabel;
    ReminderFlashTimer: TTimer;
    ReminderClickLabel: TLabel;
    CalendarPanel: TPanel;
    SelectMonthCalendar: TMonthCalendar;
    pnlWorkCount: TPanel;
    dsCdt: TDataSource;
    FilterPanel: TPanel;
    FromDateTimePicker: TDateTimePicker;
    ToDateTimePicker: TDateTimePicker;
    ProjectComboBox: TComboBox;
    FilterCheckBox: TCheckBox;
    ButtonRecalc: TButton;
    procedure TaskDBGridDrawColumnCell(Sender: TObject; Rect: TRect;
                                       DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure TaskDBGridTitleClick(Column: TColumn);
    procedure FrameResize(Sender: TObject);
    procedure ReminderFlashLabelClick(Sender: TObject);
    procedure ReminderFlashTimerTimer(Sender: TObject);
    procedure TaskDBGridKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure TaskDBGridColumnMoved(Sender: TObject; FromIndex, ToIndex: Integer);
    procedure FilterCheckBoxClick(Sender: TObject);
    procedure ButtonRecalcClick(Sender: TObject);
 private
    NoteEditPanelDesignedHeight : Integer;
    CalendarPanelDesignedWidth  : Integer;
    FTimingTask                 : TimeRecord_SA2;
    IniSettings                 : TIniFile;
    FDayCount                   : Integer;
    FWeekCount                  : Integer;
    procedure MonthCalendarChange(Sender: TObject);
    procedure SetRecordChanged( Row: Integer; ProjectCode: String );
    function  GetActiveRow: Integer;
    function  GetActiveTask: TimeRecord_SA2;
    procedure SetActiveRow(const Value: Integer);
    procedure SetActiveTask(const Value: TimeRecord_SA2);
    procedure StartDateTimeGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure StopDateTimeGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure FloatTimeDeltaGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure WorkCostGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure SumTimeDeltaGetText(Sender: TField; var Text: string; DisplayText: Boolean);
    procedure AverageHourlyRateGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure SumCostGetText(Sender: TField; var Text: string; DisplayText: Boolean);
    procedure AddColumn(sFieldName, sCaption: String; iWidth: Integer=-1);
    procedure SetColumnEditMode(iColumn: Integer; sFieldName: String);
    procedure HourlyRateGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure PicklistSetText(Sender: TField; const Text: string);
    procedure UpdateWorkCount;
    procedure CheckInitialised;
    procedure ApplyGridFilter(Enable: Boolean);
    procedure UpdateAggregateDisplay;
  protected
    procedure CDS_AddIndex(sFieldName: String; ixOpts: TIndexOptions);
  public
    FTimingTaskNum: Integer;
    FControlsEnabled: Boolean;
    FOnRecordChanged: TOnRecordChanged;
    FOnRowScrolled: TNotifyEvent;
    TSClientDataset: TAdoNetConnector;
    function  GetTimingTask: TimeRecord_SA2;
    procedure SetTimingTask(const Value: TimeRecord_SA2);
    function  IsTimingRow: Boolean;
    procedure SetTimingTaskNum(const Value: Integer);
    procedure SetControlsEnabled(const Value: Boolean);
    function  GetIndexName: String;
    procedure SetIndexName(const Value: String);
    procedure SetFlashReminder(const Value: Boolean);
    procedure TSClientDataSetBeforePost(DataSet: TDataSet);
    procedure TSClientDataSetAfterPost(DataSet: TDataSet);
    procedure TSClientDataSetBeforeDelete(DataSet: TDataSet);
    procedure TSClientDataSetAfterScroll(DataSet: TDataSet);
    procedure TSClientDataSetNewRecord(DataSet: TDataSet);
  public
    constructor Create( AOwner: TComponent ); override;
    function  ColumnIndex( sColumn: String ): Integer;
    procedure RecalculateAggregates;
    procedure RecalculateCumulativeTime;
    procedure InitialiseGrid;
    procedure LoadProjectComboBox;
    procedure LoadProjectPickList;
    procedure LoadColumnSettings;
    procedure BeginUpdate(StoreCurrentPosition: Boolean=False);
    procedure EndUpdate;
    procedure BeginLoadFile;
    procedure EndLoadFile;
    procedure Clear;
    procedure DeleteRow;
    procedure PostChanges( ReenableDisplay: Boolean = True );
    procedure SortBy    ( sColumn: String;  bDescending: Boolean ); overload;
    procedure SortBy    ( iColumn: Integer; bDescending: Boolean ); overload;
    procedure ShowColumn( sColumn: String;  bVisible: Boolean );
    procedure SetFocusToEnd;
    property  ActiveTask     : TimeRecord_SA2   read GetActiveTask    write SetActiveTask;
    property  TimingTask     : TimeRecord_SA2   read GetTimingTask    write SetTimingTask;
    property  ActiveRow      : Integer          read GetActiveRow     write SetActiveRow;     // Equiv. TMUtils.CurrentRow
    property  TimingTaskNum  : Integer          read FTimingTaskNum   write SetTimingTaskNum; // Equiv. TMUtils.CurrentRow
    property  EnableControls : Boolean          read FControlsEnabled write SetControlsEnabled;
    property  FlashReminder  : Boolean                                write SetFlashReminder;
    property  IndexName      : String           read GetIndexName     write SetIndexName;
    property  OnRecordChanged: TOnRecordChanged read FOnRecordChanged write FOnRecordChanged;
    property  OnRowScrolled  : TNotifyEvent     read FOnRowScrolled   write FOnRowScrolled;
  end;

implementation

uses TMPref;

const
  WIDTH_SCROLLBAR = 40;

{$R *.nfm}


{ TTSGrid }
{              StartTime  : TDateTime;
               EndTime    : TDateTime;
               DiffTime   : double;       // Initial calculation is End-Start; thereafter timed.
               WorkTime   : double;       // DiffTime - SlackTime
               SlackTime  : double;       // Timed
               DayCumulativeTime : double;
               OverTime   : double;       // *New* 20071202 -- Worktime >= 8 hrs/day or Sat or Sun
               HourlyRate : double;       // Per record
               Task_Num   : double;       // not used
               Unused2    : double;       // custom field 1
               Unused3    : double;       // custom field 2
               Notes      : string[255];
               EndNotes   : string[255];  // Always the same for every item
               Unused5    : string[255];  // custom field 3
               Unused6    : string[255];  // custom field 4
               pkGUID     : string;       // unique id; runtime only
               ProjectCode: string[50];   // project code within project group

               DateNumber    : Trunc( Double( StartTime )); // Used for sameday groupings
               WeekNumber    : Weeks elapsed since start of project / or start of year
               WorkTimeCost  : Calculated( WorkTime * HourlyRate )
               SlackTimeCost : Calculated( SlackTime * HourlyRate )

  Slacktime is assumed to be incremented via a counter
  DiffTime can be re-calculated as EndTime - StartTime, but is timer based; because you can "continue" retiming a task.
  WorkTime can always be calculated as DiffTime - SlackTime;
  WorkTimeCost can always be calculated (WorkTime * 24) * CurrentRow.HourlyRate;

  Aggregates:
    TotalWorkTimeCost    := SUM( WorkTimeCost )
    TotalSlackTimeCost   := SUM( SlackTime * 24 * HourlyRate );

    TotalWorkTimeElapsed  := SUM( WorkTime );
    TotalSlackTimeElapsed := SUM( SlackTime);
    TotalOverTimeElapsed  := SUM( OverTime);

    GroupDayWorktime      := SUM( WorkTime ) in a given day (ie. cumulative worktime across projects within a day)

    TotalTimeElapsed      := TotalWorkTimeElapsed + TotalSlackTimeElapsed; // Set this value on aggregate gettext;
    AverageHourlyRate
}

procedure TTSGrid.BeginLoadFile;
begin
  BeginUpdate(False);

  FilterCheckBox.Checked := False;
  ApplyGridFilter(False);
end;

procedure TTSGrid.BeginUpdate(StoreCurrentPosition: Boolean=False);
{ Disable screen updates and dataset events whenever performing batch operations
}
begin
  CheckInitialised;
  TsData.BeginUpdate(StoreCurrentPosition);
end;

procedure TTSGrid.ButtonRecalcClick(Sender: TObject);
begin
  TsData.BeginUpdate(True);
  try
    RecalculateCumulativeTime;
    Recalculateaggregates;
    UpdateAggregateDisplay;
  finally
    TsData.EndUpdate;
  end;
end;

procedure TTSGrid.Clear;
{ Empty the grid contents
}
begin
  if TsData.DatasetInitialised then
  begin
    BeginUpdate;
    try
      TsData.ClearDataset;
      TaskDbGrid.Refresh;
    finally
      EndUpdate;
    end;
  end;
end;

function TTSGrid.ColumnIndex(sColumn: &String): Integer;
{ Return the grid column matching the column title or fieldname
}
var
  i : Integer;
begin
  CheckInitialised;
  Result := -1;  // fieldnames not initialized
  for i := 0 to TaskDBGrid.Columns.Count - 1 do
    if SameText( TaskDBGrid.Columns[i].FieldName, sColumn )
    or (Assigned(TaskDBGrid.Columns[i].Field) and SameText( TaskDBGrid.Columns[i].Field.DisplayName, sColumn ))
    or SameText( TaskDBGrid.Columns[i].Title.Caption, sColumn ) then
      begin
        Result := i;
        Break;
      end;
end;

constructor TTSGrid.Create;
{ Set up the frame
}
begin
  inherited Create( AOwner );
  TaskDbGrid.DataSource        := DsCdt; 
  NoteEditPanelDesignedHeight  := NoteEditPanel.Height;
  CalendarPanelDesignedWidth   := CalendarPanel.Width;
  SelectMonthCalendar.DateTime := now;
  SelectMonthCalendar.OnChange := MonthCalendarChange;
end;

procedure TTSGrid.LoadProjectComboBox;
begin
  ProjectComboBox.Items.Assign(TMUtils.TSGProjects);
  ProjectComboBox.Items.Insert(0, '<ALL>');
  if (ProjectComboBox.ItemIndex = -1) then
    ProjectComboBox.ItemIndex := 0;
end;

procedure TTSGrid.EndLoadFile;
begin
  // Update filter settings
  LoadProjectComboBox;
  FromDateTimePicker.Date := TsData.AdoDataset.FieldByName('LastDateTime').AsDateTime - 7;
  ToDateTimePicker.Date := Date;
  SelectMonthCalendar.Date := Date;

  RecalculateCumulativeTime;  // This only needs to happen the once
  EndUpdate;
end;

procedure TTSGrid.EndUpdate;
{ Re-enable screen updates and dataset events after batch operations
}
begin
  TsData.EndUpdate;
  if not TsData.IsUpdating then
  begin
    //TsData.ADODataSet.Refresh;
    TaskDbGrid.Refresh;
    UpdateAggregateDisplay;
  end;
end;

function TTSGrid.GetActiveRow: Integer;
{ Record number of the selected row
}
begin
  Result := TSClientDataset.RecNo;
end;

function TTSGrid.GetActiveTask: TimeRecord_SA2;
{ Return the current TSClientDataset record as a TimeRecord_SA2 structure
}
begin
  Result := TsData.ActiveRecordSA2;

  // Update the quick lookup record for the timing row
  if IsTimingRow then
    FTimingTask := Result;
end;

function TTSGrid.GetTimingTask: TimeRecord_SA2;
{ Return the timing row as a TimeRecord_SA2.
  If no row is timing, return an empty record.
  (Callers should check IsTiming before a call to this procedure).
}
var
  TimingTask_SA2 : TimeRecord_SA2;
begin
  { In .NET you can initialize a var or structure to blank by pasting a local copy to it }
  Result := TimingTask_SA2;

  { Try the activetask first }
  if IsTimingRow then
    Result := ActiveTask
  else if ( TimingTaskNum > -1 ) and
          ( TSClientDataset.State in [dsBrowse] ) then
          Result := FTimingTask;
end;

procedure TTSGrid.CDS_AddIndex( sFieldName: String; ixOpts: TIndexOptions );
{ Add an index to the clientdataset on the passed field
}
begin
//  TSClientDataset1.AddIndex('ix' + sFieldName, sFieldName, ixOpts,'', '', 0);
//  Include( ixOpts, ixDescending );
//  TSClientDataset1.AddIndex('dx' + sFieldName, sFieldName, ixOpts,'', '', 0);
end;


procedure TTSGrid.LoadProjectPickList;
var
  i: Integer;
begin
  for i := 0 to TaskDBGrid.Columns.Count - 1 do
    if SameText(TaskDBGrid.Columns[i].FieldName, 'ProjectCode') then
    begin
      TaskDBGrid.Columns[i].PickList    := TMUtils.TSGProjects;
      TaskDBGrid.Columns[i].ButtonStyle := cbsAuto;
      Break;
    end;
end;

procedure TTSGrid.SetColumnEditMode( iColumn: Integer; sFieldName: String );
{ Set the task grid and this column as editable (F2 key or double click).
  Only custom fields and the project code are editable within the grid.
}
var
  SectionName: String;
begin
  TaskDBGrid.Options := TaskDBGrid.Options + [dgEditing];
  if SameText( sFieldName, 'ProjectCode' ) then
  begin
    TaskDBGrid.Columns[iColumn].PickList    := TMUtils.TSGProjects;
    TaskDBGrid.Columns[iColumn].ButtonStyle := cbsAuto;
  end
  else begin
    // Assign a picklist from the .ini file if it exists
    SectionName := sFieldName + ' dropdown list';
    if IniSettings.SectionExists( SectionName ) then
      IniSettings.ReadSection( SectionName, TaskDBGrid.Columns[iColumn].PickList );
    if TaskDBGrid.Columns[iColumn].PickList.Count > 0 then
    begin
      TaskDBGrid.Columns[iColumn].ButtonStyle := cbsAuto;
      TSClientDataset.FindField( sFieldName ).OnSetText := PickListSetText;
    end
    else
      TSClientDataset.FindField( sFieldName ).OnSetText := nil;
  end;

  TaskDBGrid.Columns[iColumn].ReadOnly := False;
end;

procedure TTSGrid.AddColumn( sFieldName, sCaption: String; iWidth: Integer = -1 );
{ Add a column with the passed properties
  iWidth is width in characters. Its value is only used if there is no stored (.ini)
  width for the column.
}
begin
  if not Assigned( TSClientDataset.FindField( sFieldName ) ) then
    Exit
  else
    TSClientDataset.FindField( sFieldName ).DisplayLabel := sCaption;

  TaskDBGrid.Columns.Add;
  with TaskDBGrid.Columns[ TaskDBGrid.Columns.Count - 1 ] do
  begin
    case TSClientDataset.FieldByName( sFieldName ).DataType of
      ftString,
      ftWideString,
      ftDateTime : Alignment := taLeftJustify
    else
      Alignment  := taRightJustify;
    end;
    FieldName       := sFieldName;
    DisplayName     := sCaption;
    ReadOnly        := True;
    Title.Caption   := sCaption;
    iWidth          := IniSettings.ReadInteger( SEC_COLUMNWIDTHS, FieldName, iWidth );
    if ( iWidth > -1 ) and (iWidth < (TaskDbGrid.Width - 20)) then
      Width    := iWidth;

    if ( FieldName = 'ProjectCode' ) or ( Pos( 'Unused', FieldName ) = 1 ) then
      SetColumnEditmode( TaskDBGrid.Columns.Count - 1, FieldName );
  end;
end;

procedure TTSGrid.CheckInitialised;
{ Ensure that the grid is initialised - this check does not re-initialise for a new project
}
begin
  if not (TsData.DatasetInitialised) then
    InitialiseGrid;
end;

procedure TTSGrid.InitialiseGrid;
{ Setup the display components that use our clientdataset.
  This call is re-done during a datafile open as project column depends on file type.
}
begin
  if not TsData.DatasetInitialised then
    TsData.InitialiseDataset(DsCdt);

  if not Assigned(TsClientDataset) then
    TsClientDataset := TsData.ADODataSet;

  // Assign display formatting routines
  if (TsData.ADODataSet.FieldCount = 0) then
    raise Exception.Create('Dataset fieldcount is 0, corrupt or missing metadata');

  OutputDebugString( 'Sequence 3: TTSGrid.InitialiseGrid column and display settings' );
//  if not Assigned(TsData.AdoDataset.FieldByName('StartTime' ).OnGetText) then
//  begin
    TsData.AdoDataset.FieldByName( 'StartTime' ).OnGetText := StartDatetimeGetText;
    TsData.AdoDataset.FieldByName( 'EndTime' ).OnGetText   := StopDatetimeGetText;
    TsData.AdoDataset.FieldByName( 'DiffTime' ).OnGetText  := FloatTimeDeltaGetText;
    TsData.AdoDataset.FieldByName( 'SlackTime' ).OnGetText := FloatTimeDeltaGetText;
    TsData.AdoDataset.FieldByName( 'WorkTime' ).OnGetText  := FloatTimeDeltaGetText;
    TsData.AdoDataset.FieldByName( 'CumulativeTime' ).OnGetText  := FloatTimeDeltaGetText;
    TsData.AdoDataset.FieldByName( 'OverTime' ).OnGetText  := FloatTimeDeltaGetText;
    TsData.AdoDataset.FieldByName( 'HourlyRate' ).OnGetText:= HourlyRateGetText;
    TsData.AdoDataset.FieldByName( 'WorkCost' ).OnGetText  := WorkCostGetText;

    // Assign aggregate display formatting routines
    TsData.AdoDataset.FieldByName( 'TotalTimeElapsed' ).OnGetText      := SumTimeDeltaGetText;
    TsData.AdoDataset.FieldByName( 'TotalSlackTimeElapsed' ).OnGetText := SumTimeDeltaGetText;
    TsData.AdoDataset.FieldByName( 'TotalWorkTimeElapsed' ).OnGetText  := SumTimeDeltaGetText;
    TsData.AdoDataset.FieldByName( 'TotalWorkTimeCost' ).OnGetText     := SumCostGetText;
    TsData.AdoDataset.FieldByName( 'TotalSlackTimeCost' ).OnGetText    := SumCostGetText;
    TsData.AdoDataset.FieldByName( 'AverageHourlyRate' ).OnGetText     := AverageHourlyRateGetText;
    TsData.AdoDataset.FieldByName( 'TotalOverTimeElapsed' ).OnGetText  := SumTimeDeltaGetText;


    // Assign data-aware fields to the display fields shown above the grid
    NoteDBMemo.DataField                  := 'Notes';
    //TotalTimeElapsedDBText.DataField      := 'TotalTimeElapsed';
    //TotalSlackTimeElapsedDBText.DataField := 'TotalSlackTimeElapsed';
    TotalWorkTimeElapsedDBText.DataField  := 'TotalWorkTimeElapsed';
    //TotalWorkTimeCostDBText.DataField     := 'TotalWorkTimeCost';
    //TotalSlackTimeCostDBText.DataField    := 'TotalSlackTimeCost';
    AverageHourlyRateDBText.DataField     := 'AverageHourlyRate';
    //OverTimeDBText.DataField              := 'TotalOverTimeElapsed';
//  end;

  LoadColumnSettings;
end;

procedure TTSGrid.LoadColumnSettings;
{ Load and apply the column settings including custom fields
}
var
  i           : Integer;
  tm          : TTextMetric;
  charwidth   : Integer;
  TmpList     : TStringList;
  FieldName   : String;
  ColumnTitle : String;
  SortColumn  : Integer;
begin
  CheckInitialised;
  OutputDebugString( 'Sequence 4: TTSGrid.LoadColumns with '+IntToStr( TSClientDataset.FieldCount )+' fields.' );
  GetTextMetrics( TaskDBGrid.Canvas.handle, tm );
  charwidth := tm.tmAveCharWidth;

  // Set up the default display options
  TaskDBGrid.Columns.Clear;
  TaskDBGrid.Options := TaskDBGrid.Options - [dgEditing];

  TmpList     := TStringList.Create;
  inisettings := TINIFile.Create( TMUtils.GetIniFilePath );
  try
    // Prepend static columns
    if CurrentProject.IsProjectGroup then
    begin
      AddColumn( 'ProjectCode','Project',  charwidth*15 );
      SortColumn := 1;
    end
    else
      SortColumn := 0;

    AddColumn( 'StartTime',  'Start Time' );
    AddColumn( 'EndTime',    'Stop Time' );
    AddColumn( 'DiffTime',   'Total',    charwidth*12 );

    INISettings.ReadSectionValues( SEC_GRIDCOLUMNS, TmpList );

    // If there are items missing from the .ini file (or it is new)
    // use the defaults
    if ( TmpList.Count < DEFINABLE_COLUMNS ) then
    begin
      AddColumn( 'SlackTime',      'Slack',    charwidth*12 );
      AddColumn( 'WorkTime',       'Work',     charwidth*12 );
      AddColumn( 'CumulativeTime', 'Day',      charwidth*12 );
      AddColumn( 'OverTime',       'Overtime', charwidth*12 );
      AddColumn( 'WorkCost',       'Work Cost',charwidth*12 );
      AddColumn( 'Notes',          'Task Notes' );
    end
    else begin
      // Load the custom and/or selectable columns
      for i := (TmpList.Count - 1) downto 0 do
        if ( copy( TmpList[i], pos('=', TmpList[i])+1, 1 ) <> '1' ) then
          continue  // not visible
        else begin
          ColumnTitle := copy( TmpList[i], pos('=', TmpList[i])+2, Length( TmpList[i]));
          if SameText( ColumnTitle, INISettings.ReadString( SEC_FIELDNAMEMAP, 'Unused2', 'Custom 1' )) then
            FieldName := 'Unused2'
          else if SameText( ColumnTitle, INISettings.ReadString( SEC_FIELDNAMEMAP, 'Unused3', 'Custom 2' )) then
            FieldName := 'Unused3'
          else if SameText( ColumnTitle, INISettings.ReadString( SEC_FIELDNAMEMAP, 'Unused5', 'Custom 3' )) then
            FieldName := 'Unused5'
          else if SameText( ColumnTitle, INISettings.ReadString( SEC_FIELDNAMEMAP, 'Unused6', 'Custom 4' )) then
            FieldName := 'Unused6'
          else if SameText( ColumnTitle, 'Slack' ) then
            FieldName := 'SlackTime'
          else if SameText( ColumnTitle, 'Day' ) then
            FieldName := 'Cumulativetime'
          else if SameText( ColumnTitle, 'Overtime' ) then
            FieldName := 'OverTime'
          else if SameText( ColumnTitle, 'Work' ) then
            FieldName := 'WorkTime'
          else if SameText( ColumnTitle, 'Rate' ) then
            FieldName := 'HourlyRate'
          else if SameText( ColumnTitle, 'Work Cost' ) then
            FieldName := 'WorkCost'
          else if SameText( ColumnTitle, 'Task Notes' ) then
            FieldName := 'Notes'
          else
            continue;

          AddColumn( FieldName, ColumnTitle );
        end;
    end;
  finally
    inisettings.Free;
    TmpList.Free;
  end;

  // Autosize the last column
  FrameResize( Self );
  SortBy( SortColumn, False );
end;

function TTSGrid.IsTimingRow: Boolean;
{ Return true if the current row is the timing row
}
begin
  Result := TMUtils.IsTimingRecord(TSClientDataset['pkGUID']);
end;

procedure TTSGrid.StartDateTimeGetText(Sender: TField; var Text: &String; DisplayText: Boolean);
{ Consistently display a short date and time throughout the grid.
  - if its within the last week show the day and time, else show medium date only
}
var
  MyDate: TDateTime;
begin
  inherited;
  if (csdesigning in componentstate) then Exit;
  TRY
    if not Sender.IsNull then
    begin
      MyDate := (Sender.AsDateTime);                             { Date only part for day comparison }
      if (not DisplayText) and (Sender.EditMask='!99/99/00 90:00;1;_') then             { in edit mode }
        Text := FormatDateTime('dd/MM/yy HH:nn',Sender.AsDateTime)
      else if IsTimingRow and IsToday(MyDate) then
        Text := FormatDateTime('ddd hh:nn:ss',Sender.AsDateTime)      { if timing show seconds as well }
      else begin                                                                   { display only mode }
        if IsToday(MyDate) or ((DaysBetween(MyDate,Date) < 6) and (MyDate < Now)) then
          Text := FormatDateTime('ddd hh:nn',Sender.AsDateTime)  { in past week - just show dow & time }
        else if (MyDate > Now) then                         { in future (want to know dow, day & time) }
          Text := FormatDateTime('ddd d/MM hh:nn',Sender.AsDateTime)
        else                                                                  { longer than a week ago }
          Text := FormatDateTime('dd/MM/yy hh:nn',Sender.AsDateTime);
      end;
    end;
  EXCEPT on E:Exception do
    Text := 'Error: ' + E.Message;
  END;
end;

procedure TTSGrid.StopDateTimeGetText(Sender: TField; var Text: &String; DisplayText: Boolean);
{ Consistently display a stop time. If its the same day as the start date (likely), just show the end time.
}
var
  MyDate: TDateTime;
begin
  inherited;
  if (csdesigning in componentstate) then Exit;
  TRY
    if not Sender.IsNull then
    begin
      if (not DisplayText) and (Sender.EditMask='!99/99/00 90:00;1;_') then { in edit mode }
        Text := FormatDateTime('dd/MM/yy HH:mm',Sender.AsDateTime)
      else if IsTimingRow and ( TMUtils.CurrentState in tsTiming ) then
        Text := ' ' + TMUtils.TimingStatus[TMUtils.CurrentState].Text + ' '
      else begin                                                            { display only mode }
        MyDate := (Sender.AsDateTime);                      { Date only part for day comparison }

        if SameDate( MyDate, Sender.Dataset.FieldByName( 'StartTime' ).AsDateTime ) then
          Text := FormatDateTime('hh:nn',Sender.AsDateTime)
        else
          Text := FormatDateTime('dd/MM/yy hh:nn',Sender.AsDateTime);
      end;
    end;
  EXCEPT on E:Exception do
    Text := 'Error: ' + E.Message;
  END;
end;

procedure TTSGrid.SetActiveRow(const Value: Integer);
{ Set the selected row to the record number passed
}
begin
  TSClientDataset.RecNo := Value;
end;

procedure TTSGrid.SetActiveTask(const Value: TimeRecord_SA2);
{ Copy the passed record values into the matching clientdataset fields for the
  selected row.
}
begin
  if not ( TSClientDataset.State in [dsEdit, dsInsert] ) then
    TSClientDataset.Edit;
  TRY
    TSClientDataset['StartTime']    := Value.StartTime;
    TSClientDataset['EndTime']      := Value.EndTime;
    TSClientDataset['DiffTime']     := Value.DiffTime;
    TSClientDataset['WorkTime']     := Value.WorkTime;
    TSClientDataset['SlackTime']    := Value.SlackTime;
    TSClientDataset['OverTime']     := Value.OverTime;
    TSClientDataset['HourlyRate']   := Value.HourlyRate;
    TSClientDataset['Task_Num']     := trunc( Value.Task_Num );
    TSClientDataset['Unused2']      := trunc( Value.Unused2 );
    TSClientDataset['Unused3']      := Value.Unused3;
    TSClientDataset['Notes']        := Value.Notes;
    TSClientDataset['EndNotes']     := Value.EndNotes;
    TSClientDataset['Unused5']      := Value.Unused5;
    TSClientDataset['Unused6']      := Value.Unused6;
    TSClientDataset['pkGUID']       := Value.pkGUID;
    TSClientDataset['ProjectCode']  := Value.ProjectCode;
    TSClientDataset['DateNumber']   := trunc( Double( Value.StartTime ));
    TSClientDataset['WorkCost']     := (Value.WorkTime * 24) * Value.HourlyRate;
    TSClientDataset.Post;
  EXCEPT on E:Exception do
    begin
      TSClientDataset.Cancel;
      raise E;
    end;
  END;

  // Update the quick lookup record for the timing row
  if IsTimingRow then
    FTimingTask := Value;
end;

procedure TTSGrid.SetTimingTask(const Value: TimeRecord_SA2);
{ Update the timing task record.
  nb. FTimingTask gets assigned by the 2nd line call to SetActiveTask
}
begin
  if IsTimingRow then
    ActiveTask := Value
  else if ( TSClientDataset.State in [dsBrowse] ) then
  begin
    EnableControls := False;
    TSClientDataset.First;
    while not TSClientDataset.Eof do
    begin
      if IsTimingRow then
        begin
          ActiveTask := Value;
          Break;
        end;
      TSClientDataset.Next;
    end;
    EnableControls := True;
  end
  else
    Assert( False, 'Oops. Didn''t handle TSGrid.SetTimingTask with Edit state. But...this shouldn''t happen.' );
end;

procedure TTSGrid.SetTimingTaskNum(const Value: Integer);
{ Update the "task number" that points to the timing record.
  Duplicates ActiveRow, but still used by some functions.
}
begin
  FTimingTaskNum := Value;
end;

procedure TTSGrid.ShowColumn(sColumn: &String; bVisible: Boolean);
{ Set a column in/visible
}
begin
  CheckInitialised;
  if ColumnIndex( sColumn ) > -1 then
  begin
    TaskDBGrid.Columns.Items[ ColumnIndex( sColumn ) ].Visible := bVisible;
    FrameResize( Self );
  end;
end;

procedure TTSGrid.SortBy(sColumn: String; bDescending: Boolean);
{ Sort the dataset by this column/field
}
begin
  CheckInitialised;

  if SameText( sColumn, 'Work Cost' ) then  // Work cost is rate*constant
    sColumn := 'HourlyRate';
  Sortby( ColumnIndex( sColumn ), bDescending );
end;

procedure TTSGrid.SortBy(iColumn: Integer; bDescending: Boolean);
{ Set the dataset by this column/field and update the title's display
}
var
  i: Integer;
begin
  CheckInitialised;
//  if ( TSClientDataSet1.IndexDefs.IndexOf( 'ix' + TaskDBGrid.Columns[iColumn].FieldName ) = -1 ) then
//    begin
//      { Add persistent indices "on the fly" as the user clicks on the title of the grid.
//        Using the addindex method rather than indexfieldnames allows us to sort descending.
//      }
//      if ( TaskDBGrid.Columns[iColumn].Field.DataType = ftString ) then
//        CDS_AddIndex( 'Notes',       [ixCaseInsensitive] )
//      else
//        CDS_AddIndex( TaskDBGrid.Columns[iColumn].FieldName, [] );
//      TSClientDataset1.IndexDefs.Update;
//    end;

  // Clear column headers of up/down toggle
  for i := 0 to TaskDBGrid.Columns.Count - 1 do
    if Assigned(TaskDBGrid.Columns[i].Field) then
      TaskDBGrid.Columns[i].Title.Caption := TaskDBGrid.Columns[i].Field.DisplayName;

  // Update column header and index
//  if bDescending then            Can't do descending in AdoDataTable mode
//    begin
//      TaskDBGrid.Columns[iColumn].Title.Caption := TaskDBGrid.Columns[iColumn].Title.Caption + '  v';
//      TsData.IndexName                 := {'dx'+}TaskDBGrid.Columns[iColumn].FieldName;
//    end
//  else begin
  if (iColumn > -1) then
  begin
    TaskDBGrid.Columns[iColumn].Title.Caption := TaskDBGrid.Columns[iColumn].Title.Caption + '  '+#94;
    TsData.IndexName                 := {'ix'+}TaskDBGrid.Columns[iColumn].FieldName;
  end;
end;

procedure TTSGrid.TaskDBGridDrawColumnCell(Sender: TObject; Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
{ Custom drawing for the grid
}
var
  OutRect : TRect;
begin
  OutRect := Rect;
  try

    { Paint the timing row red... }
    if ( SameText( Column.FieldName, 'EndTime' )
         or (( Column.Index > 1 ) and ( Column.Index < 5 )))
    and ( TMUtils.CurrentState in [csTiming, csSlacking] ) 
    and SameText( Column.Field.DataSet['pkGUID'], TMUtils.TimingRecord.TimeRecord.pkGUID ) then
    begin
      (Sender As TDBGrid).Canvas.Brush.Color := TimingStatus[ CurrentState ].BackGroundColor;
      (Sender As TDBGrid).Canvas.Font.Color  := TimingStatus[ CurrentState ].FontColor;
    end

    { Paint rows using alternate colors, except for the selected cell }
    else if not (gdSelected in State) then
      begin
        if TSClientDataset.FieldByName( 'Duplicate' ).AsBoolean then
          (Sender As TDBGrid).Canvas.Brush.Color := DUPLICATE_COLOR
//        else if ( DayOfWeek( TSClientDataset.FieldByName( 'StartTime' ).AsDateTime ) in [DaySaturday,DaySunday] ) then
//          (Sender As TDBGrid).Canvas.Brush.Color := OVERTIME_COLOR
        else if TSClientDataset.FieldByName( 'LastDayOfWeek' ).AsBoolean then
          (Sender As TDBGrid).Canvas.Brush.Color := LASTDAYOFWEEK_COLOR
        else if ( (TSClientDataset.RecNo mod 2) = 0 ) then
          (Sender As TDBGrid).Canvas.Brush.Color := ROW2_COLOR
        else
          (Sender As TDBGrid).Canvas.Brush.Color := ROW1_COLOR;
      end;

    { For the notes column in the grid, truncate the output to display only the first line }
    if SameText( Column.FieldName, 'Notes' ) then
      begin
         OutRect.Bottom := OutRect.Bottom + 100;
        (Sender As TDBGrid).Canvas.FillRect( OutRect );
        (Sender As TDBGrid).Canvas.TextRect(OutRect, OutRect.Left+2, OutRect.Top+2, TMUtils.FirstNoteLine( Column.Field.AsString ));
      end
    else
      (Sender As TDBGrid).DefaultDrawColumnCell(OutRect, DataCol, Column, State);

  except on E: Exception do begin
    (Sender As TDBGrid).DefaultDrawColumnCell(OutRect, DataCol, Column, State);
    DebugMsg('TTSGrid.TaskDBGridDrawColumnCell exception ' + E.Message);
  end;
  end;
end;

procedure TTSGrid.HourlyRateGetText(Sender: TField; var Text: &String; DisplayText: Boolean);
{ Display the hourly rate as a currency. It is stored as a double in the record+file structure.
}
begin
  inherited;
  if (csdesigning in componentstate) then Exit;
  TRY
    if ( not Sender.IsNull ) then
      Text := FloatToStrF(Sender.Value, ffCurrency, 5, 2);
  EXCEPT on E:Exception do
    Text := 'Error: ' + E.Message;
  END;
end;

procedure TTSGrid.WorkCostGetText(Sender: TField; var Text: &String; DisplayText: Boolean);
{ Calculate and display the work cost as a currency.
  It is stored in the record/file/dataset as a float.
}
var
  WorkTimeCost: double;
begin
  inherited;
  if (csdesigning in componentstate) then Exit;
  TRY
    if ( not Sender.IsNull ) then
      begin
        WorkTimeCost := (Sender.Dataset['WorkTime'] * 24)  * Sender.Dataset['HourlyRate'];
        Text         := FloatToStrF(WorkTimeCost, ffCurrency, 5, 2);
      end;
  EXCEPT on E:Exception do
    Text := 'Error: ' + E.Message;
  END;
end;

procedure TTSGrid.DeleteRow;
{ Deletes the active row
}
var
  EndNotes   : String;
  UpdateTask : TimeRecord_SA2;
begin
  if ( TSClientDataset.State in [dsInsert] ) then
    begin
      TSClientDataset.Cancel;
      Exit;
    end;

  if ( TSClientDataset.State in [dsEdit] ) then
    TSClientDataset.Cancel;

  if ( TSClientDataset.RecordCount > 0 ) then
    begin
      {If the first record is going to be deleted, make sure the End
      Notes are copied to the second record, which will be the new
      first record.}
      if ( TSClientDataset.RecNo = 0 ) and ( TSClientDataset.RecordCount > 1 ) then
        begin
          EndNotes := ActiveTask.EndNotes;
          TSClientDataset.Delete;
          TSClientDataset.First;
          TSClientDataset.Edit;
          TRY
            TSClientDataset['EndNotes'] := EndNotes;
            TSClientDataset.Post;
          EXCEPT on E:Exception do
            TSClientDataset.Cancel;
          END;
        end { RecNo = 0 }
      else
        TSClientDataset.Delete;
    end; { RecordCount > 0 }
end;

procedure TTSGrid.SetControlsEnabled(const Value: Boolean);
{ En/disable the clientdataset controls.
}
begin
  FControlsEnabled := Value;
  if Value then
    TSClientDataset.EnableControls
  else
    TSClientDataset.DisableControls;
end;

procedure TTSGrid.TaskDBGridTitleClick(Column: TColumn);
{ Respond to the user clicking on a column title
  - perform a new or invert the current sort order.
}
var
  bDescending: Boolean;
begin
  bDescending := ( copy( Column.Title.Caption, Length( Column.Title.Caption ), 1 ) = #94 );
  SortBy( Column.Index, bDescending );
end;

procedure TTSGrid.SumCostGetText(Sender: TField; var Text: string; DisplayText: Boolean);
{ Display the aggregate values (summary labels) as currency
}
begin
  inherited;
  if (csdesigning in componentstate) then Exit;
  TRY
    if not Sender.IsNull then
      Text := FloatToStrF( Double( Sender.Value ), ffCurrency, 5, 2 );
  EXCEPT on E:Exception do
    Text := 'Error: ' + E.Message;
  END;
end;

procedure TTSGrid.SumTimeDeltaGetText(Sender: TField; var Text: string; DisplayText: Boolean);
{ Consistently display a time difference
}
begin
  inherited;
  if (csdesigning in componentstate) then Exit;
  TRY
    if not Sender.IsNull then
      Text := TMUtils.FormatTimeDelta( Double( Sender.Value ) );
  EXCEPT on E:Exception do
    Text := 'Error: ' + E.Message;
  END;
end;

procedure TTSGrid.FilterCheckBoxClick(Sender: TObject);
begin
  if not TsData.IsUpdating then
    ApplyGridFilter((Sender as TCheckbox).Checked);
end;

procedure TTSGrid.ApplyGridFilter(Enable: Boolean);
var
  WasFiltered: Boolean;
  ProjectFilter: String;
  DateFilter: String;
begin
  WasFiltered := TsData.ADODataSet.Filtered;

  BeginUpdate(False);
  try
    TsData.ADODataSet.Filtered := False;
    DateFilter := Format( '(DateNumber >= %d) and (DateNumber <= %d)',
                   [ Trunc( Double( FromDateTimePicker.DateTime )),
                     Trunc( Double( ToDateTimePicker.DateTime )) ]);
    if (ProjectComboBox.ItemIndex > 0) then
      ProjectFilter := Format('(ProjectCode = ''%s'') and ', [ProjectComboBox.Text]);

    TsData.ADODataSet.Filter := ProjectFilter + DateFilter;

    TsData.ADODataSet.Filtered := Enable;

    // Has the filtered changed or been de/reactivated?
    if (TsData.ADODataSet.Filtered <> WasFiltered) then
    begin
      //RecalculateCumulativeTime; == only needed at startup
      RecalculateAggregates;
    end;

  finally
    TsData.First;
    EndUpdate;
  end;
end;

procedure TTSGrid.FloatTimeDeltaGetText(Sender: TField; var Text: String; DisplayText: Boolean);
{ Consistently display a time difference when the calling field is a float
}
begin
  inherited;
  if (csdesigning in componentstate) then Exit;
  TRY
    if not Sender.IsNull then
    begin
      if (not DisplayText) and (Sender.EditMask='!99/99/00 90:00;1;_') then { in edit mode }
        Text := FormatDateTime('HH:mm', Sender.AsFloat )
      else begin
        Text := TMUtils.FormatTimeDelta( Sender.AsFloat );
      end;
    end;
  EXCEPT on E:Exception do
    Text := 'Error: ' + E.Message;
  END;
end;

procedure TTSGrid.AverageHourlyRateGetText(Sender: TField; var Text: String; DisplayText: Boolean);
{ Display the average hourly rate next to the current hourly rate for the project
}
begin
  inherited;
  if (csdesigning in componentstate) then Exit;
  TRY
    if Sender.IsNull or ( Sender.Value = 0 ) then
    begin
      HourlyRateLabel.Caption := 'Hourly Rate:';
      Text := FloatToStrF( CurrentProject.HourlyRate, ffCurrency, 5, 2 );
    end
    else begin
      HourlyRateLabel.Caption := 'Hourly Rate / Avg:';
      Text := FloatToStrF( CurrentProject.HourlyRate, ffCurrency, 5, 2 ) + ' / '
            + FloatToStrF( Double( Sender.Value ), ffFixed, 5, 2 );
    end;
  EXCEPT on E:Exception do
    Text := 'Error: ' + E.Message;
  END;
end;

procedure TTSGrid.FrameResize(Sender: TObject);
{ Autosize the rightmost (usually notes) column
}
var
  i      : Integer;
  iWidth : Integer;
begin
  if TsData.DatasetInitialised and ( TaskDBGrid.Columns.Count > 1 ) then
  begin
    iWidth := 0;
    for i := 0 to TaskDBGrid.Columns.Count - 2 do
      iWidth := iWidth + TaskDBGrid.Columns[i].Width;
    if ( TaskDBGrid.Width - iWidth - WIDTH_SCROLLBAR ) > 0 then
      with TaskDBGrid.Columns[TaskDBGrid.Columns.Count - 1] do
      begin
        Autosize := False;
        Width    := TaskDBGrid.Width - iWidth - WIDTH_SCROLLBAR;
      end;

    //ProjectComboBox.Left := TaskDBGrid.Left;
    ProjectComboBox.Width :=  TaskDBGrid.Columns[0].Width;
    FromDateTimePicker.Left := ProjectComboBox.Left + ProjectComboBox.Width;
    FromDateTimePicker.Width :=  TaskDBGrid.Columns[1].Width;
    ToDateTimePicker.Left :=  FromDateTimePicker.Left + FromDateTimePicker.Width;
    ToDateTimePicker.Width :=  TaskDBGrid.Columns[2].Width;
  end;

  iWidth := TimingStatusHostPanel.Width - ( CostAndRateContainerPanel.Left + CostAndRateContainerPanel.Width );

  if ( iWidth div CalendarPanelDesignedWidth ) > 1 then
    CalendarPanel.Width := CalendarPanelDesignedWidth * 2 // max 2 cals( iWidth div CalendarPanelDesignedWidth )
  else
    CalendarPanel.Width := CalendarPanelDesignedWidth;
  SelectMonthCalendar.Width := CalendarPanel.Width;
end;

procedure TTSGrid.SetFocusToEnd;
{ Jump to the last record and highlight it as selected - called after load
}
begin
  if TSClientDataset.Recordcount > 0 then
  begin
    TSClientDataset.Last;
    if TaskDBGrid.CanFocus then
      TaskDBGrid.SetFocus;
    SelectMonthCalendar.DateTime := TSClientDataset.FieldByName( 'StartTime' ).AsDateTime;
  end;
end;

procedure TTSGrid.MonthCalendarChange(Sender: TObject);
{ Jump to the nearest date when the user clicks on a date
}
begin
  inherited;
{ Jump to the nearest date when the user clicks on a date
}
  if TsData.DatasetInitialised and ( TSClientDataset.RecordCount > 0 ) then
  begin
     ShowMessage('TTSGrid.MonthCalendarChange(Sender: TObject); todo');
     SortBy( 'Start Time', False );
     //TSClientDataset.Fi SetKey;
     TSClientDataset.FieldByName( 'StartTime' ).AsDateTime := (Sender As TMonthCalendar).DateTime;
    //TSClientDataset1.GotoNearest;
  end;
end;

procedure TTSGrid.PostChanges( ReenableDisplay: Boolean = True );
{ Post any record changes to the dataset, including from the timing record.
  For the timing record; we need to copy edits that were made in the grid into
  the record structure itself.

  Although the grid has the property [CancelOnExit := False], when save or print is
  clicked, we lose notes field changes if the record is in dsEdit state; and the
  endtime of the timing record is not current.
}
var
  ActiveRowBookmark: Integer;
begin
  if ( TSClientDataset.State in [dsEdit, dsInsert] ) then
    TSClientDataset.Post;

  if ( TMUtils.CurrentState in tsTiming ) then
  begin
    ActiveRowBookmark := ActiveRow;
    EnableControls    := False;
    try
      // Don't loop through all the records if we're already on the timing one,
      // which is often the case
      if not IsTimingRow then
        TSClientDataset.First;

      while not TSClientDataset.Eof do
        if IsTimingRow then
          begin
            TMUtils.TimingRecord.TimeRecord.ProjectCode := TSClientDataset.FieldByName( 'ProjectCode' ).AsString;
            TMUtils.TimingRecord.TimeRecord.Unused2     := Trunc( TSClientDataset.FieldByName( 'Unused2' ).AsFloat );
            TMUtils.TimingRecord.TimeRecord.Unused3     := TSClientDataset.FieldByName( 'Unused3' ).AsFloat;
            TMUtils.TimingRecord.TimeRecord.Unused5     := TSClientDataset.FieldByName( 'Unused5' ).AsString;
            TMUtils.TimingRecord.TimeRecord.Unused6     := TSClientDataset.FieldByName( 'Unused6' ).AsString;
            TMUtils.TimingRecord.TimeRecord.Notes       := TSClientDataset.FieldByName( 'Notes' ).AsString;
            TMUtils.SetTimingState( TMUtils.CurrentState );  // Forces write of current endtime
            ActiveTask := TMUtils.TimingRecord.TimeRecord;
            Break;
          end
        else
          TSClientDataset.Next;
      ActiveRow := ActiveRowBookmark;
    finally
      EnableControls := True;
    end;
  end;
end;

function TTSGrid.GetIndexName: String;
{ Name of the current index
}
begin
  Result := TSClientDataset.IndexFieldNames;
end;

procedure TTSGrid.SetIndexName(const Value: String);
{ Set the sort indexname. Applied indirectly so display of the
  sort column header appears correctly
}
//var
//  bDescending : Boolean;
//  sColumnName : String;
begin
  if TsData.DatasetInitialised and ( Value <> '' ) then
  begin
//    sColumnName := copy( Value, 3, Length(Value) );
//    bDescending := SameText( copy( Value, 1, 2 ), 'dx' );
//    SortBy( sColumnName, bDescending );
    SortBy(Value, False);
  end;
end;

procedure TTSGrid.RecalculateAggregates;
{ Using aggregates with filters on a CDS is a known source of problems.
  This is a workaround that forces all aggregates to recalc. whenever
  there is a change to a CDS filter, or a change to an index when a filter
  is/has been used.

  In practice this is somewhat dubious. Closure of the dataset causes the loss of most
  gettext events, and aggregate calculations are erratic... I've stopped using it,
  but left it here if I need to reuse it.
}
//var
//  i      : integer;
//  Agg    : TAggregate;
//  AggFld : TAggregateField;
begin
  TsData.RecalculateAggregates;
//  UpdateAggregateDisplay;

  // do the AggFields ...
//  TSClientDataset1.Close;
//  for i := 0 to TSClientDataset.AggFields.Count - 1 do
//  begin
//    AggFld := (TSClientDataset.AggFields[i] as TAggregateField);
//    if (AggFld <> nil) and AggFld.Active then
//    begin
//      AggFld.Active := False;
//      AggFld.Active := True;
//    end;
//  end;
//  // do the Aggs ...
//  for i := 0 to TSClientDataset1.Aggregates.Count - 1 do
//  begin
//    Agg := TSClientDataset1.Aggregates[i];
//    if Agg.Active then
//    begin
//      Agg.Active := False;
//      Agg.Active := True;
//    end;
//  end;
//
//  TSClientDataset1.AggregatesActive := False;
//  TSClientDataset1.AggregatesActive := True;
//  TSClientDataset1.Open;
  //RecalculateCumulativeTime;
end;

procedure TTSGrid.RecalculateCumulativeTime;
{ Update the cumulative time in the dataset and set grouping boundaries
}
var
  iLastDayNumber : Integer;
  iLastDayOfWeek : Integer;
  fDayCumulativeTime : Double;
  fWeekCumulativeTime : Double;
  fDisplayCumulativeTime : Double;
  fOvertime : Double;
  dow: Word;
  bEof: Boolean;
  tmLastStartTime: TDateTime;
  dowEnd: Integer;
begin
  iLastDayNumber := -1;
  iLastDayOfWeek := -1;
  fDayCumulativeTime := 0;
  fWeekCumulativeTime := 0;
  tmLastStartTime := 0;
  fDayCount := 0;
  fWeekCount := 0;
  dowEnd := DayThursday;            // Thursday should show our weekcumulative total; weeksofar

  TSClientDataSet.DisableControls;
  try
    TsData.First;

    while not TsData.Eof do
    begin
      if not TsData.IsDisplayedRecord then
      begin
        TsData.Next;
        Continue;
      end;

      TSClientDataSet.Edit;
      TSClientDataSet.FieldByName( 'Duplicate' ).AsBoolean := ( TSClientDataSet.FieldByName( 'StartTime' ).AsDateTime = tmLastStartTime );
      TSClientDataSet.Post;
      tmLastStartTime := TSClientDataSet.FieldByName( 'StartTime' ).AsDateTime;

      if ( iLastDayNumber <> ( TSClientDataSet.FieldByName( 'DateNumber' ).AsInteger )) then
      begin
        iLastDayNumber     := TSClientDataSet.FieldByName( 'DateNumber' ).AsInteger;
        fDayCumulativeTime := 0;
        inc( fDayCount );
      end;

      fDayCumulativeTime := fDayCumulativeTime + TSClientDataset.FieldByName( 'Worktime' ).AsFloat;
      fWeekCumulativeTime := fWeekCumulativeTime + TSClientDataset.FieldByName( 'Worktime' ).AsFloat;

      TsData.Next;
      bEof := TsData.Eof;

      // Only write the record if it is the last occurence of data in a day
      if bEof or ( iLastDayNumber <> ( TSClientDataSet.FieldByName( 'DateNumber' ).AsInteger )) then
      begin
        if not bEof then
          TSClientDataSet.Prior;

        // Overtime is /any/ time worked on a saturday or sunday, or > 8 hours on a weekday
        dow := DayOfWeek( TSClientDataSet.FieldByName( 'StartTime' ).AsFloat );
        if ( dow in [DaySaturday, DaySunday] ) then // 1=Sunday, 7=Saturday
          fOvertime := fDayCumulativeTime
        else begin
          if (( fDayCumulativeTime * 24 ) > 9 ) then
            fOvertime := IncHour( fDayCumulativeTime, -9 )
          else
            fOvertime := 0;
        end;

        if ( iLastDayOfWeek = dowEnd ) then
        begin
          fDisplayCumulativeTime := fWeekCumulativeTime;
          fWeekCumulativeTime := 0;
          inc( fWeekCount );
        end
        else
          fDisplayCumulativeTime := fDayCumulativeTime;

        TSClientDataSet.Edit;
        TSClientDataSet.FieldByName( 'CumulativeTime' ).AsFloat := fDisplayCumulativeTime;
        TSClientDataSet.FieldByName( 'CumulativeTimeDaySoFar' ).AsFloat := fDayCumulativeTime;
        TSClientDataSet.FieldByName( 'CumulativeTimeWeekSoFar' ).AsFloat := fWeekCumulativeTime;
        TSClientDataSet.FieldByName( 'OverTime' ).AsFloat := fOvertime;
        TSClientDataSet.FieldByName( 'LastDayOfWeek' ).AsBoolean := ( iLastDayOfWeek = dowEnd );
        TSClientDataSet.Post;

        // Update the scrolling dow grouping
        iLastDayOfWeek := dow;

        // Because we scrolled back off the last record, Eof does not get set again
        if bEof then
          Break
        else
          TsData.Next;
      end;
    end;
  finally
    TSClientDataSet.EnableControls;
  end;
  UpdateWorkCount;
end;

procedure TTSGrid.TSClientDataSetAfterPost(DataSet: TDataSet);
{ Notify the main form that the current record has changed - status bar display
}
begin
  SetRecordChanged( Dataset.RecNo, Dataset.FieldByName( 'ProjectCode' ).AsString );
end;

procedure TTSGrid.TSClientDataSetBeforeDelete(DataSet: TDataSet);
{ Notify the main form that the current record has changed - status bar display
}
begin
  SetRecordChanged( Dataset.RecNo, Dataset.FieldByName( 'ProjectCode' ).AsString );
end;

procedure TTSGrid.SetRecordChanged(Row: Integer; ProjectCode: String);
{ Notify that the record has changed. Hooked by the TMMain.RecordIsChanged property.
}
begin
  FlashReminder := False;

  if Assigned( FOnRecordChanged ) then
    FOnRecordChanged( Row, ProjectCode );
end;

procedure TTSGrid.TSClientDataSetAfterScroll(DataSet: TDataSet);
{ Notify that the user has changed record. Hooked to the current record number
  display in the status panel.
}
begin
  if Assigned( FOnRowScrolled ) then
    FOnRowScrolled( Self );
  UpdateAggregateDisplay;
end;

procedure TTSGrid.UpdateAggregateDisplay;
var
  Day: String;
  Project: String;
  ActiveRecord: TimeRecord_SA2;
begin
  ActiveRecord := TsData.ActiveRecordSA2;
  if TsData.IsUpdating or not ActiveRecord.IsValid then
    Exit;

  Day := FormatDateTime('ddd dd MMM',  ActiveRecord.StartTime);
  Project := String(ActiveRecord.ProjectCode);
  if Pos(' ', Project) > 0 then
    Project := Copy(Project, Pos(' ', Project) + 1, Length(Project));

  DayElapsedTimeLabel.Caption := Format('%s Time: ', [Day]);
  DayElapsedTimePanel.Caption := TsData.DayAggregate[ActiveRecord.DayNumberStr].WorktimeDelta;

  DayProjectTimeLabel.Caption := Format('%s: ', [Project]);
  ProjectTimePanel.Caption := TsData.DayProjectAggregate[ActiveRecord.DayProject].WorktimeDelta;

  TotalProjectTimeLabel.Caption := Format('%s: ', [Project]);
  TotalProjectTimePanel.Caption := TsData.ProjectAggregate[ActiveRecord.ProjectCode].WorktimeDelta;

  TotalTimeElapsedPanel.Caption := TsData.TotalWorkTimeAggregate[''].WorktimeDelta;
end;


procedure TTSGrid.ReminderFlashLabelClick(Sender: TObject);
{ OK, OK, stop flashing the reminder
}
begin
  FlashReminder := False;
end;

procedure TTSGrid.ReminderFlashTimerTimer(Sender: TObject);
{ Change the color of the label in an animated effect so the user knows why
  timestamp has just rudely jumped to the front - they set a reminder.
}
begin
  if ( ReminderFlashLabel.Color = clMaroon ) then
    ReminderFlashLabel.Color := clGreen
  else
    ReminderFlashLabel.Color := clMaroon;
end;

procedure TTSGrid.SetFlashReminder(const Value: Boolean);
{ En/disable the reminder flashing
}
begin
  if Value then
  begin
    ReminderFlashTimer.Interval := 500;
    ReminderFlashTimer.OnTimer  := ReminderFlashTimerTimer;
  end;
  ReminderFlashTimer.Enabled  := Value;
  ReminderFlashLabel.Visible  := Value;
  ReminderClickLabel.Visible  := Value;
end;

procedure TTSGrid.TSClientDataSetBeforePost(DataSet: TDataSet);
{ Update our "fake" calculated field, work cost.
  This is really only required for the sort order to be correct.
}
begin
  Dataset['WorkCost'] := (Dataset['WorkTime'] * 24)  * Dataset['HourlyRate'];
end;

procedure TTSGrid.TaskDBGridKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
{ The grid "dgediting" property also enables inserting of new records by the user.
  We need to suppress this, because the timing structure is not correctly initiated.
}
begin
  if not (( ssCtrl in Shift ) or ( ssAlt in Shift )) then
    if ( Key = VK_INSERT ) then
    begin
      OutputDebugString('User dbgrid insert key disallowed.');
      Key := 0;
    end;
end;

procedure TTSGrid.PicklistSetText(Sender: TField; const Text: string);
{ When one of the custom field picklist values changes, check for an hourly
  rate association and apply it
}
var
  ratesettings : TIniFile;
  ratelist     : TStringList;
  ratesection  : String;
  picksection  : String;
  picklist     : TStringList;
  rateindex    : Integer;
  rate         : Double;
begin
  OutputDebugString( Sender.FieldName + ' .settext' );
  Sender.AsString := Text;
  if ( trim(Text) = '' ) then
    Exit;

  ratesettings := TINIFile.Create( TMUtils.GetIniFilePath );
  try
    RateSection := Sender.FieldName + ' rates';
    PickSection := Sender.FieldName + ' dropdown list';
    picklist := TStringList.Create;
    try
      rateSettings.ReadSection( PickSection, PickList );
      rateindex := picklist.IndexOf( Text );
    finally
      picklist.Free;
    end;

    if  ( rateindex > -1 )
    and ( ratesettings.ReadString( RateSection, 'rates', '' ) <> '' ) then
    begin
      ratelist := TStringList.Create;
      try
        RateList.Delimiter     := ';';
        RateList.DelimitedText := ratesettings.ReadString( RateSection, 'rates', '' );

        if ( ratelist.count > rateindex ) and ( ratelist[rateindex] <> '' ) then
        begin
          rate := TMUtils.EditTextToHourlyRate( ratelist[rateindex] );
          if ( rate <> 0 ) then
            TSClientDataset.FindField( 'HourlyRate' ).AsFloat := rate;
        end;
      finally
        ratelist.Free;
      end;
    end;
  finally
    ratesettings.Free;
  end;
end;

procedure TTSGrid.TSClientDataSetNewRecord(DataSet: TDataSet);
{ The "dgediting" property enables both in-grid editing, and inserting of new
  records by the user.
  We want to allow editing of existing rows, but not insertion. This event must
  be set in addition to killing the keydown, as the user can "insert" a record
  just by scrolling down.

  The InsertAllowed flag indicates the insert is being performed by the program,
  not the user.
}
begin
  if ( TsData.UpdateCount <= 0 ) and not TsData.InsertAllowed then
  begin
    OutputDebugString('User dbgrid insert disallowed.');
    Abort;
  end;
  TsData.InsertAllowed := False;
end;

procedure TTSGrid.UpdateWorkCount;
begin
  pnlWorkCount.Caption := Format( '%d w %d d', [FWeekCount, FDayCount] );
end;

procedure TTSGrid.TaskDBGridColumnMoved(Sender: TObject; FromIndex, ToIndex: Integer);
{ The "dgcolumnresize" property enables both resize and column drag and drop.
  We want to allow resize, but not repositioning, since we have static columns.
  Repositioning is allowed on the preferences page, but here, we "kill" it.
}
begin
  if MessageDlg('You may only adjust the column positions from the file->preferences->grid settings page.'
                +#13+#10+'Do you want to set these now?', mtConfirmation, [mbYes, mbCancel], 0) = mrYes then
    with TPreferencesForm.Create(Application) do
      try
        ShowModal;
      finally
        Free;
      end;
  LoadColumnSettings; // "Kill" any change made by the user dragging the column.
end;

//*****************************************************************************
{ TMONTHCALENDAR fixes

  1. Allow the user to change the date ( IsBlankSysTime method )
  2. Reimplement the create event (related to 1.)
  3. Expose the datetime field so we can access as a .net tdatetime
  4. Implement a change event
}
//*****************************************************************************

constructor TMonthCalendar.Create(AOwner: TComponent);
var
  stnow: TSystemTime;
begin
  inherited Create(AOwner);
  DateTimeToSystemTime( now, stnow );
  SendStructMessage( GetCalendarHandle, MCM_SETCURSEL, 0, stnow);
  Invalidate;
end;

function IsBlankSysTime(const ST: TSystemTime): Boolean;
begin
  with ST do
    Result := ((wYear + wMonth + wDayOfWeek + wDay +
               wHour + wMinute + wSecond + wMilliseconds ) = 0 );
end;

procedure TMonthCalendar.CNNotify(var Message:TWMNotifyMC);
{ Fix the isblanksystime bug in borland.vcl.comctrls;
  in addition fire a change event if the selection actually changes the date

  MCN_SELECT is sent whenever a selection has occured (via mouse or keyboard)
  MCN_SELCHANGE is sent whenever the currently displayed date changes
  via month change, year change, keyboard navigation, prev/next button

  MCN_GETDAYSTATE is sent for MCS_DAYSTATE controls whenever new daystate
}
var
  bChanged               : boolean;
  dLastDate,dLastEndDate : TDateTime;
  sts, ste               : TSystemTime;
begin
  with Message, NMHdr do
  begin
    if code = MCN_GETDAYSTATE then
      bMonthChanged := True;

    if bMonthChanged and (code=MCN_SELCHANGE) then
    begin
      { Prevent a recursive loop when GETDAYSTATE fires SELCHANGE fires inherited GETDAYSTATE... }
      bMonthChanged := False;
      exit;
    end;

    if (code=MCN_SELECT) or (code=MCN_SELCHANGE) then
    begin
      bMonthChanged  := False;
      dLastDate    := Int( Date );
      dLastEndDate := Int( EndDate );

      sts := NMSelChange.stSelStart;
      if not IsBlankSysTime(sts) then
        DateTime := SystemTimeToDateTime(sts);
      bChanged:= Int(Date) <> Int(dLastDate);

      if MultiSelect then
      begin
        ste := NMSelChange.stSelEnd;
        if not IsBlankSysTime(ste) then
          EndDate := Int( SystemTimeToDateTime(ste) );

        bChanged := bChanged or (Int(dLastEndDate)<>Int(EndDate));
      end;
      if bChanged then
        DoChange;
    end;
  end;
  inherited;
end;

procedure TMonthCalendar.DoChange;
begin
  if Assigned(FOnChange) then FOnChange(Self);
end;

end.

