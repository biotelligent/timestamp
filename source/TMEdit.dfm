�
 TTASKEDITDIALOG 0�  TPF0TTaskEditDialogTaskEditDialogLeft� TopVBorderIcons BorderStylebsDialogCaptionEdit Task RecordClientHeightqClientWidth�Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style OldCreateOrder	PositionpoScreenCenterScaledOnShowFormShowPixelsPerInch`
TextHeight TPanelPanel1LeftTopWidth�HeightI
BevelInnerbvRaised
BevelOuter	bvLoweredTabOrder  	TGroupBox	GroupBox1LeftTopWidth� HeightaCaption
Task StartFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTabOrder  TLabelLabel3Left	TopWidthHeightCaptionDate:Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.Style 
ParentFont  TLabelLabel4Left	Top:WidthHeightCaptionTime:Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.Style 
ParentFont  TDateTimePickerStartDateDateTimePickerLeft@TopWidthaHeightCalAlignmentdtaLeftDate �����@Time �����@
DateFormatdfShortDateMode
dmComboBoxFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style KinddtkDate
ParseInput
ParentFontTabOrder   TDateTimePickerStartTimeDateTimePickerLeft@Top7WidthaHeightCalAlignmentdtaLeftDate P n��@Time P n��@
DateFormatdfShortDateModedmUpDownFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style KinddtkTime
ParseInput
ParentFontTabOrder   	TGroupBox	GroupBox2LeftToppWidth� HeightaCaptionTask EndFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTabOrder TLabelLabel5Left	TopWidthHeightCaptionDate:Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.Style 
ParentFont  TLabelLabel6Left	Top:WidthHeightCaptionTime:Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.Style 
ParentFont  TDateTimePickerEndDateDateTimePickerLeft@TopWidthaHeightCalAlignmentdtaLeftDate �����@Time �����@
DateFormatdfShortDateMode
dmComboBoxFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style KinddtkDate
ParseInput
ParentFontTabOrder   TDateTimePickerEndTimeDateTimePickerLeft@Top7WidthaHeightCalAlignmentdtaLeftDate P n��@Time P n��@
DateFormatdfShortDateModedmUpDownFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style KinddtkTime
ParseInput
ParentFontTabOrder   	TGroupBox	GroupBox3Left� TopWidth!Height� CaptionPeripheral DataFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTabOrder TLabelLabel7Left	TopWidthtHeightCaptionWork Time (Minutes):Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.Style 
ParentFont  TLabelLabel8Left	TopvWidthCHeightCaptionHourly Rate:Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.Style 
ParentFontOnClickLabel8Click  TLabelLabel2Left	Top1WidthvHeightCaptionSlack Time (Minutes):Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.Style 
ParentFont  TButtonRecalculateWorkTimeButtonLeftTopPWidth� HeightHint7Set work time to Start/End Difference, minus slack timeCaptionRecalculate Work TimeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontParentShowHintShowHint	TabOrder OnClickRecalculateWorkTimeButtonClick   	TGroupBox	GroupBox4Left� Top� Width!Height� CaptionNotesFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTabOrder TLabelLabel1LeftTopWidth@HeightCaptionAutofill Text:Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.Style 
ParentFont  	TComboBoxAutoFillComboBoxLeftPTopWidth� HeightHintQYou may add items to this drop-down list using Tools -> Create Default Notes ListStylecsDropDownListFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ItemHeight
ParentFontParentShowHintShowHint	TabOrder OnChangeAutoFillComboBoxChange  TMemoNoteMemoLeftTop.WidthHeightkFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style Lines.StringsNoteMemo 	MaxLength� 
ParentFont
ScrollBars
ssVerticalTabOrderOnChangeNoteMemoChange   TPanelPanel2LeftTop� Width� Heighta
BevelInner	bvLowered
BevelOuterbvNoneTabOrder TLabelLabel9LeftTopWidth`HeightCaptionTask BreakdownFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFont  TLabelLabel10LeftdTop(WidthIHeight*Caption- Blue: Work

- Yellow: SlackFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFont    TButtonOKButtonLeftPTopTWidthKHeightCaptionOKFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style ModalResult
ParentFontTabOrderOnClickOKButtonClick  TButtonCancelButtonLeft�TopTWidthKHeightCaptionCancelFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style ModalResult
ParentFontTabOrder   