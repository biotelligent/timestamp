{
Application:    Time Stamp, Copyright 1997 William Rouck
Module:         TMNotBld.pas
Contact:        email: wrouck@syntap.com
Modifications:  JM Holloway, June 2004
                justin@schiznit.net


This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version. This
program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details. You should have
received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
02111-1307, USA.
}

unit TMNotBld;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, System.ComponentModel, Borland.Vcl.Buttons;

type
  TNoteBuilderForm = class(TForm)
    SaveButton: TButton;
    CancelButton: TButton;
    DefaultNoteEdit: TEdit;
    Label1: TLabel;
    DefaultNotesListBox: TListBox;
    AddToMasterListButton: TButton;
    DeleteButton: TButton;
    MoveDownButton: TSpeedButton;
    MoveUpButton: TSpeedButton;
    procedure AddToMasterListButtonClick(Sender: TObject);
    procedure DeleteButtonClick(Sender: TObject);
    procedure MoveUpButtonClick(Sender: TObject);
    procedure MoveDownButtonClick(Sender: TObject);
  public
    function  Execute( var AutoFillNotes: TStringList ): Boolean; 
  end;

implementation

uses TMUtils;

{$R *.NFM}

procedure TNoteBuilderForm.AddToMasterListButtonClick(Sender: TObject);
{This procedure adds the text in the DefaultNoteEdit box to the list of
 notes.}
begin
  DefaultNotesListBox.Items.Add(DefaultNoteEdit.Text);
  DefaultNoteEdit.Clear;
end; {procedure TNoteBuilderForm.AddToMasterListButtonClick}

//*****************************************************************************

function TNoteBuilderForm.Execute(var AutoFillNotes: TStringList): Boolean;
{ Show the form, returning true if the user alters (and wants to save)
  the notes list
}
begin
  DefaultNotesListBox.Items.Clear;
  DefaultNotesListBox.Items.Assign( AutoFillNotes );
  Result := ( ShowModal = mrOK );
  if Result then
    AutoFillNotes.Assign( DefaultNotesListBox.Items );
end;

//*****************************************************************************

procedure TNoteBuilderForm.DeleteButtonClick(Sender: TObject);
{This procedure deletes the selected Default Note from the list of Default
 Notes.}
var
  Count : word;
begin
  for Count := 0 to (DefaultNotesListBox.Items.Count - 1) do
  begin
    if DefaultNotesListBox.Selected[Count] then
    begin
      DefaultNotesListBox.Items.Delete(Count);
      Break;
    end; {if DefaultNotesListBox.Selected}
  end; {for}
end; {procedure TNoteBuilderForm.DeleteButtonClick}

//*****************************************************************************

procedure TNoteBuilderForm.MoveUpButtonClick(Sender: TObject);
{ Move the selected item up in the list
}
begin
  if ( DefaultNotesListBox.ItemIndex > 0 ) then
    DefaultNotesListBox.Items.Exchange( DefaultNotesListBox.ItemIndex, DefaultNotesListBox.ItemIndex - 1 );
end;

procedure TNoteBuilderForm.MoveDownButtonClick(Sender: TObject);
{ Move the selected item down in the list
}
begin
  if  ( DefaultNotesListBox.ItemIndex > -1 )
  and ( DefaultNotesListBox.ItemIndex < DefaultNotesListBox.Count - 1 ) then
    DefaultNotesListBox.Items.Exchange( DefaultNotesListBox.ItemIndex, DefaultNotesListBox.ItemIndex + 1 );
end;

end.
