{
Application:    Time Stamp, Copyright 1997 William Rouck
Module:         TMDropListBld.pas
Contact:        email: wrouck@syntap.com
Modifications:  JM Holloway, June 2004
                justin@schiznit.net


This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version. This
program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details. You should have
received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
02111-1307, USA.
}

unit TMDropListBld;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, System.ComponentModel, Borland.Vcl.Buttons, IniFiles;

type
  TDropListBuilderForm = class(TForm)
    SaveButton: TButton;
    CancelButton: TButton;
    DefaultNoteEdit: TEdit;
    Label1: TLabel;
    DefaultDropListBox: TListBox;
    AddToMasterListButton: TButton;
    DeleteButton: TButton;
    MoveDownButton: TSpeedButton;
    MoveUpButton: TSpeedButton;
    FieldNameLabel: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    RateEdit: TEdit;
    RateListBox: TListBox;
    procedure AddToMasterListButtonClick(Sender: TObject);
    procedure SaveButtonClick(Sender: TObject);
    procedure DeleteButtonClick(Sender: TObject);
    procedure MoveDownButtonClick(Sender: TObject);
    procedure MoveUpButtonClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    INISettings : TIniFile;
    SectionName : String;
    RateSection : String;
  public
    procedure Execute( FieldName, ColumnTitle: String );
  end;

implementation

uses TMUtils;

{$R *.NFM}

procedure TDropListBuilderForm.AddToMasterListButtonClick(Sender: TObject);
{This procedure adds the text in the DefaultNoteEdit box to the list of
 notes.}
begin
   DefaultDropListBox.Items.Add(DefaultNoteEdit.Text);
   DefaultNoteEdit.Clear;

   if ( RateEdit.Text = '' ) or ( StrToFloatDef( RateEdit.Text, -1.11 ) = -1.11 ) then
     RateListBox.Items.Add('')
   else
     RateListBox.Items.Add( FloatToStrF( StrToFloat( RateEdit.Text ), ffCurrency, 5, 2) );

   RateEdit.Clear;
end; {procedure TNoteBuilderForm.AddToMasterListButtonClick}

//*****************************************************************************

procedure TDropListBuilderForm.SaveButtonClick(Sender: TObject);
{This procedure writes the default note list to a text file where it can be
 recalled for use by all projects.}
var
  i : Integer;
begin
  IniSettings.EraseSection( SectionName );

  for i := 0 to DefaultDropListBox.Items.Count - 1 do
    IniSettings.WriteString( SectionName, DefaultDropListBox.Items[i], '' );
  INISettings.WriteString( RateSection, 'rates', RateListBox.Items.DelimitedText );
end; {procedure TNoteBuilderForm.SaveButtonClick}

//*****************************************************************************

procedure TDropListBuilderForm.DeleteButtonClick(Sender: TObject);
{This procedure deletes the selected Default Note from the list of
 default notes.}
var
  Count : word;
begin
  for Count := 0 to (DefaultDropListBox.Items.Count - 1) do
    if DefaultDropListBox.Selected[Count] then
    begin
      DefaultDropListBox.Items.Delete(Count);
      RateListBox.Items.Delete(Count);
      Break;
    end; {if DefaultDropListBox.Selected}
end; {procedure TNoteBuilderForm.DeleteButtonClick}

//*****************************************************************************

procedure TDropListBuilderForm.MoveUpButtonClick(Sender: TObject);
{ Move the selected item higher in the list
}
var
  i : Integer;
begin
  i :=  DefaultDropListBox.ItemIndex;
  if ( i > 0 ) then
  begin
    DefaultDropListBox.Items.Exchange( i, i - 1 );
    RateListBox.Items.Exchange( i, i - 1 );
  end;
end;

procedure TDropListBuilderForm.MoveDownButtonClick(Sender: TObject);
{ Move the selected item lower in the list
}
var
  i : Integer;
begin
  i :=  DefaultDropListBox.ItemIndex;
  if ( i > -1 ) and ( i < DefaultDropListBox.Count - 1 ) then
  begin
    DefaultDropListBox.Items.Exchange( i, i + 1 );
    RateListBox.Items.Exchange( i, i + 1 );
  end;
end;

procedure TDropListBuilderForm.Execute(FieldName, ColumnTitle: String);
{ Show the form populated with values for the field & column passed
}
begin
  FieldNameLabel.Caption := ColumnTitle + ' values:';
  SectionName            := FieldName + ' dropdown list';
  RateSection            := FieldName + ' rates';

  INISettings.ReadSection( SectionName, DefaultDropListBox.Items );
  RateListBox.Items.Delimiter     := ';';
  RateListBox.Items.DelimitedText := INISettings.ReadString( RateSection, 'rates', '' );

  { Sync the listboxes if they have become mismatched }
  if RateListBox.Items.Count > DefaultDropListBox.Items.Count then
    RateListBox.Clear;
  while RateListBox.Items.Count < DefaultDropListBox.Items.Count do
    RateListBox.Items.Add( '' );
  ShowModal;
end;

procedure TDropListBuilderForm.FormCreate(Sender: TObject);
begin
  INISettings := TINIFile.Create( TMUtils.GetINIFilePath );
end;

procedure TDropListBuilderForm.FormDestroy(Sender: TObject);
begin
  INISettings.Free;
end;

end.
