Time Stamp
Version 3.12.2003-0801
by William Rouck, Syntap Software

---------------------------

This release incorporates the following fixes / 
enhancements:


Bug Fixes

- Error message appeared when Windows 98 users clicked on the top fixed cell row of the grid when no tasks were present.  This would appear for other OS versions if all tasks in the grid were deleted and then the top fixed cell row was clicked.



Thanks, and enjoy!
-Bill

