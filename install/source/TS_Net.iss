; Installer script for Time Stamp .Net
; Includes support for .Net assembly registration
;
; Install script using InnoSetup 5.2.2 with ISPP

#define AppDir           "..\..\bin"
#define DataDir          "..\..\sample data"
#define SrcDir           "..\..\source"
#define ImgDir           "..\..\images"
#define AppVersion       GetFileVersion("..\..\bin\TimeStamp_Net.exe")
#define AssemblySource   "c:\program files\codegear\rad studio\5.0\bin"
#define GACUtilSource    "c:\windows\Microsoft.NET\Framework\v1.1.4322"

[Setup]

AppName=TimeStamp .NET
AppVerName=TimeStamp 4.0 for .NET
AppVersion={#AppVersion}
MinVersion=0,5.0.2195
DefaultDirName={pf}\TimeStamp.NET
DefaultGroupName=Time Stamp .Net
OutputDir=..\bin
OutputBaseFilename=TimeStamp_Setup
PrivilegesRequired=admin
UninstallDisplayName=TimeStamp for .NET
UninstallDisplayIcon={app}\TimeStamp_NET.exe
AppCopyright=� 2004
LicenseFile={#AppDir}\License.txt
ShowLanguageDialog=no
AppUpdatesURL=http://www.syntap.com
AppSupportURL=http://www.syntap.com
WizardImageBackColor=$6c0000
WizardImageFile=Install_Stamp.bmp

[Tasks]
Name: desktopicon; Description: Create a &desktop icon; GroupDescription: Additional icons:

[Files]
; Application and settings
Source: {#AppDir}\TimeStamp_NET.exe;                      DestDir: {app}
Source: {#AppDir}\TimeStamp_NET.exe.config;               DestDir: {app}
Source: {#AppDir}\TimeStamp_net.INI;                      DestDir: {app}
Source: {#AppDir}\gpl.txt;                                DestDir: {app}
Source: {#AppDir}\License.txt;                            DestDir: {app}
Source: {#AppDir}\Readme.txt;                             DestDir: {app}
Source: {#AppDir}\timestamp.dtd;                          DestDir: {app}
Source: {#AppDir}\Timestamp.hlp;                          DestDir: {app}
Source: {#AppDir}\Timestamp.hpt;                          DestDir: {app}
Source: {#AppDir}\Timestamp.not;                          DestDir: {app}
Source: {#AppDir}\TSReport.rav;                           DestDir: {app}

;Libraries - these should output to {app}\Assemblies, but they do not operate correctly
;Source: {#AssemblySource}\Borland.Delphi.dll;         DestDir: {app}
;Source: {#AssemblySource}\Borland.Vcl.dll;            DestDir: {app}
;Source: {#AssemblySource}\Borland.VclDbCtrls.dll;     DestDir: {app}
;Source: {#AssemblySource}\Borland.VclDbRtl.dll;       DestDir: {app}
;Source: {#AssemblySource}\Borland.VclDbxCds.dll;      DestDir: {app}

;Sample Data
Source: {#DataDir}\My Projects.tsg;                DestDir: {app}\Sample Data
Source: {#DataDir}\DE_Web.SA2;                     DestDir: {app}\Sample Data
Source: {#DataDir}\DE_Web.TS2;                     DestDir: {app}\Sample Data
Source: {#DataDir}\Inventory Manager.SA2;          DestDir: {app}\Sample Data
Source: {#DataDir}\Inventory Manager.TS2;          DestDir: {app}\Sample Data
Source: {#DataDir}\Orders.SA2;                     DestDir: {app}\Sample Data
Source: {#DataDir}\Orders.TS2;                     DestDir: {app}\Sample Data

;Source code
Source: {#SrcDir}\*;                                         DestDir: {app}\source;     Flags: recursesubdirs

;Images
Source: {#ImgDir}\*;                                         DestDir: {app}\images;     Flags: skipifsourcedoesntexist

;Installer
Source: *.iss;                                               DestDir: {app}\install\source;    Flags: recursesubdirs
Source: *.bmp;                                               DestDir: {app}\install\source;    Flags: recursesubdirs
Source: {#GACUtilSource}\gacutil.exe;                        DestDir: {tmp}
Source: {#GACUtilSource}\gacutil.exe.config;                 DestDir: {tmp}
Source: {#GACUtilSource}\msvcr71.dll;                        DestDir: {tmp}
Source: {#AppDir}\midas.dll;                                 DestDir: {app}

;Create output directories for the source code
[Dirs]
Name: "{app}\bin"
Name: "{app}\source"

[Run]
;The install of the files to the Assemblies folder does not work correctly
;Filename: "{code:GetGACUtil} "; Parameters: "/i ""{app}\Borland.Delphi.dll"""
;Filename: "{code:GetGACUtil} "; Parameters: "/i ""{app}\Borland.Vcl.dll"""
;Filename: "{code:GetGACUtil} "; Parameters: "/i ""{app}\Borland.VclDbCtrls.dll"""
;Filename: "{code:GetGACUtil} "; Parameters: "/i ""{app}\Borland.VclDbRtl.dll"""
;Filename: "{code:GetGACUtil} "; Parameters: "/i ""{app}\Borland.VclRtl.dll"""
;Filename: "{code:GetGACUtil} "; Parameters: "/i ""{app}\Borland.VclX.dll"""
Filename: "{app}\TimeStamp_NET.exe"; Parameters: """{app}\Sample Data\My Projects.tsg"""; Description: Launch TimeStamp .NET; Flags: nowait postinstall skipifsilent

[INI]
Filename: {app}\TSNet.url; Section: InternetShortcut; Key: URL; String: http://www.syntap.com

[Icons]
Name: {group}\Time Stamp .NET;            Filename: {app}\TimeStamp_NET.exe
Name: {group}\Time Stamp on the web;      Filename: {app}\TSNet.url
Name: {group}\Uninstall Time Stamp .NET;  Filename: {uninstallexe}
Name: {userdesktop}\Time Stamp .NET;      Filename: {app}\TimeStamp_NET.exe; Tasks: desktopicon

[Registry]
; Add a key so assemblies can be seen in VS .NET
Root: HKLM; Subkey: Software\Microsoft\.NETFramework\AssemblyFolders\TimeStamp.NET; ValueType: String; ValueData: {app}\Assemblies; Flags: deletekey uninsdeletekey

[Code]
const
  TimeStampVersion = '4.0';

function GetGACUtil(S: String): String;
var
  sPath: String;
begin
  Result := 'Path to framework or gacutil not found: This means the .NET framework is missing or the wrong version (not 1.0 or 1.1).';

  if RegQueryStringValue(HKLM, 'SOFTWARE\Microsoft\.NETFramework', 'sdkInstallRoot', sPath) then
    Result := sPath + 'Bin\gacutil.exe'
  else if RegQueryStringValue(HKLM, 'SOFTWARE\Microsoft\.NETFramework', 'sdkInstallRootv1.1', sPath) then
    Result := sPath + 'Bin\gacutil.exe'
  else
    Result := ExpandConstant( '{tmp}' ) + '\gacutil.exe';
end;

function InitializeSetup(): Boolean;
var
  ErrorCode: Integer;
  NetFrameWorkInstalled : Boolean;
  Result1 : Boolean;
begin
   NetFrameWorkInstalled := RegKeyExists(HKLM,'SOFTWARE\Microsoft\.NETFramework\policy\v1.0');
   if NetFrameWorkInstalled =true then
   begin
      Result := true;
   end;
   if NetFrameWorkInstalled = false then
   begin
     NetFrameWorkInstalled := RegKeyExists(HKLM,'SOFTWARE\Microsoft\.NETFramework\policy\v1.1');
     if NetFrameWorkInstalled =true then
     begin
        Result := true;
     end;
     if NetFrameWorkInstalled = false then
     begin
       Result1 := MsgBox('This setup requires the .NET Framework. Please download and install the .NET Framework and run this setup again. Do you want to download the framwork now?',
                    mbConfirmation, MB_YESNO) = idYes;
       if Result1 =false then
       begin
          Result:=false;
       end
       else
       begin
         Result:=false;
         ShellExec( '',
            'http://download.microsoft.com/download/a/a/c/aac39226-8825-44ce-90e3-bf8203e74006/dotnetfx.exe',
            '',
            '',
            SW_SHOWNORMAL,
            ewWaitUntilTerminated,
            ErrorCode
            );
      end;
    end;
  end;
end;

