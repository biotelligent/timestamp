program TimeStamp_NET_Test;
{

  Delphi DUnit Test Project
  -------------------------
  This project contains the DUnit test framework and the GUI/Console test runners.
  Add "CONSOLE_TESTRUNNER" to the conditional defines entry in the project options 
  to use the console test runner.  Otherwise the GUI test runner will be used by 
  default.

}

{$IFDEF CONSOLE_TESTRUNNER}
{$APPTYPE CONSOLE}
{$ENDIF}

uses
  Forms,
  TestFramework,
  NGUITestRunner,
  TextTestRunner,
  TestTMMain in 'source\TestTMMain.pas',
  TMMain in '..\Source\TMMain.pas',
  TMUtils in '..\Source\TMUtils.pas';

{$R *.RES}

begin
  Application.Initialize;
  if IsConsole then
    TextTestRunner.RunRegisteredTests
  else
    NGUITestRunner.RunRegisteredTests;
end.

