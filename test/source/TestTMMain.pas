unit TestTMMain;
{

  Delphi DUnit Test Case
  ----------------------
  This unit contains a skeleton test case class generated by the Test Case Wizard.
  Modify the generated code to correctly setup and call the methods from the unit 
  being tested.

}

interface

uses
  TestFramework, System.ComponentModel, ToolWin, DB, TMMain, SysUtils, Classes, TMGridFrme, 
  ComCtrls, Windows, TMUtils, StdCtrls, TMReport, Messages, Printers, Variants, Menus, Controls, 
  IniFiles, ActnList, Dialogs, Spin, System.Diagnostics, Grids, Forms, Calendar, Buttons, 
  ExtCtrls, Graphics, ImgList;

type
  // Test methods for class TTSMainForm
  
  TestTTSMainForm = class(TTestCase)
  strict private
    FTSMainForm: TTSMainForm;
  protected
    procedure SetUp; override;
    procedure TearDown; override;
  private
    [Test]
    procedure TestAboutActionExecute;
    [Test]
    procedure TestAlwaysOnTopActionExecute;
    [Test]
    procedure TestAppendProjectActionExecute;
    [Test]
    procedure TestApplicationMinimizeHandler;
    [Test]
    procedure TestApplicationRestoreHandler;
    [Test]
    procedure TestApplicationRxTrayIconClick;
    [Test]
    procedure TestAutoSaveTimerTimer;
    [Test]
    procedure TestClearNotesActionExecute;
    [Test]
    procedure TestCreateDefaultNotesListActionExecute;
    [Test]
    procedure TestCreateProjectActionExecute;
    [Test]
    procedure TestDeleteTaskActionExecute;
    [Test]
    procedure TestEditTaskActionExecute;
    [Test]
    procedure TestExitActionExecute;
    [Test]
    procedure TestExportASCIILogActionExecute;
    [Test]
    procedure TestExportXMLProjectActionExecute;
    [Test]
    procedure TestFormClose;
    [Test]
    procedure TestFormCloseQuery;
    [Test]
    procedure TestFormCreate;
    [Test]
    procedure TestFormShow;
    [Test]
    procedure TestGNUGPLActionExecute;
    [Test]
    procedure TestHelpContentsActionExecute;
    [Test]
    procedure TestHideToolbarActionExecute;
    [Test]
    procedure TestLicenseInfoActionExecute;
    [Test]
    procedure TestMinimizePopupMenuItemClick;
    [Test]
    procedure TestMRUManagerClick;
    [Test]
    procedure TestOpenProjectActionExecute;
    [Test]
    procedure TestPreferencesActionExecute;
    [Test]
    procedure TestPrintLogActionExecute;
    [Test]
    procedure TestPrintPreviewActionExecute;
    [Test]
    procedure TestPrintSetupActionExecute;
    [Test]
    procedure TestRemindTimerTimer;
    [Test]
    procedure TestReducedModeActionExecute;
    [Test]
    procedure TestRegistrationInfoHelpMenuItemClick;
    [Test]
    procedure TestResetRemindTimer;
    [Test]
    procedure TestRestorePopupMenuItemClick;
    [Test]
    procedure TestSaveProjectActionExecute;
    [Test]
    procedure TestSetDefaultHourlyRateActionExecute;
    [Test]
    procedure TestSetDefaultReportEndNotesActionExecute;
    [Test]
    procedure TestSetProjectTitleActionExecute;
    [Test]
    procedure TestSetThisProjectsReportEndNotesActionExecute;
    [Test]
    procedure TestStartSlackActionExecute;
    [Test]
    procedure TestStartTimerActionExecute;
    [Test]
    procedure TestStatusTimerTimer;
    [Test]
    procedure TestVersionHistoryActionExecute;
    [Test]
    procedure TestStopTimerActionExecute;
    [Test]
    procedure TestTSStatusBarDrawPanel;
    [Test]
    procedure TestRemindActionExecute;
    [Test]
    procedure TestRemindTimeSpinEditChange;
    [Test]
    procedure TestContinueTimeActionExecute;
    [Test]
    procedure TestGridPopupPopup;
    [Test]
    procedure TestPrintPreviewToolButtonClick;
    [Test]
    procedure TestCreateGroupActionExecute;
    [Test]
    procedure TestRowScrolled;
    [Test]
    procedure TestUpdateTitle;
    [Test]
    procedure TestUpdateStatusPanel;
  published
    [Test]
    procedure TestUpdateGUI;
  end;

implementation

procedure TestTTSMainForm.SetUp;
begin
  FTSMainForm := TTSMainForm.Create(nil);
  TSMainForm := FTSMainForm;
end;

procedure TestTTSMainForm.TearDown;
begin
  FTSMainForm := nil;
end;

procedure TestTTSMainForm.TestAboutActionExecute;
var
  Sender: TObject;
begin
  // TODO: Setup method call parameters
  FTSMainForm.AboutActionExecute(Sender);
  // TODO: Validate method results
end;

procedure TestTTSMainForm.TestAlwaysOnTopActionExecute;
var
  Sender: TObject;
begin
  // TODO: Setup method call parameters
  FTSMainForm.AlwaysOnTopActionExecute(Sender);
  // TODO: Validate method results
end;

procedure TestTTSMainForm.TestAppendProjectActionExecute;
var
  Sender: TObject;
begin
  // TODO: Setup method call parameters
  FTSMainForm.AppendProjectActionExecute(Sender);
  // TODO: Validate method results
end;

procedure TestTTSMainForm.TestApplicationMinimizeHandler;
var
  Sender: TObject;
begin
  // TODO: Setup method call parameters
  FTSMainForm.ApplicationMinimizeHandler(Sender);
  // TODO: Validate method results
end;

procedure TestTTSMainForm.TestApplicationRestoreHandler;
var
  Sender: TObject;
begin
  // TODO: Setup method call parameters
  FTSMainForm.ApplicationRestoreHandler(Sender);
  // TODO: Validate method results
end;

procedure TestTTSMainForm.TestApplicationRxTrayIconClick;
var
  Y: Integer;
  X: Integer;
  Shift: TShiftState;
  Button: TMouseButton;
  Sender: TObject;
begin
  // TODO: Setup method call parameters
  FTSMainForm.ApplicationRxTrayIconClick(Sender, Button, Shift, X, Y);
  // TODO: Validate method results
end;

procedure TestTTSMainForm.TestAutoSaveTimerTimer;
var
  Sender: TObject;
begin
  // TODO: Setup method call parameters
  FTSMainForm.AutoSaveTimerTimer(Sender);
  // TODO: Validate method results
end;

procedure TestTTSMainForm.TestClearNotesActionExecute;
var
  Sender: TObject;
begin
  // TODO: Setup method call parameters
  FTSMainForm.ClearNotesActionExecute(Sender);
  // TODO: Validate method results
end;

procedure TestTTSMainForm.TestCreateDefaultNotesListActionExecute;
var
  Sender: TObject;
begin
  // TODO: Setup method call parameters
  FTSMainForm.CreateDefaultNotesListActionExecute(Sender);
  // TODO: Validate method results
end;

procedure TestTTSMainForm.TestCreateProjectActionExecute;
var
  Sender: TObject;
begin
  // TODO: Setup method call parameters
  FTSMainForm.CreateProjectActionExecute(Sender);
  // TODO: Validate method results
end;

procedure TestTTSMainForm.TestDeleteTaskActionExecute;
var
  Sender: TObject;
begin
  // TODO: Setup method call parameters
  FTSMainForm.DeleteTaskActionExecute(Sender);
  // TODO: Validate method results
end;

procedure TestTTSMainForm.TestEditTaskActionExecute;
var
  Sender: TObject;
begin
  // TODO: Setup method call parameters
  FTSMainForm.EditTaskActionExecute(Sender);
  // TODO: Validate method results
end;

procedure TestTTSMainForm.TestExitActionExecute;
var
  Sender: TObject;
begin
  // TODO: Setup method call parameters
  FTSMainForm.ExitActionExecute(Sender);
  // TODO: Validate method results
end;

procedure TestTTSMainForm.TestExportASCIILogActionExecute;
var
  Sender: TObject;
begin
  // TODO: Setup method call parameters
  FTSMainForm.ExportASCIILogActionExecute(Sender);
  // TODO: Validate method results
end;

procedure TestTTSMainForm.TestExportXMLProjectActionExecute;
var
  Sender: TObject;
begin
  // TODO: Setup method call parameters
  FTSMainForm.ExportXMLProjectActionExecute(Sender);
  // TODO: Validate method results
end;

procedure TestTTSMainForm.TestFormClose;
var
  Action: TCloseAction;
  Sender: TObject;
begin
  // TODO: Setup method call parameters
  FTSMainForm.FormClose(Sender, Action);
  // TODO: Validate method results
end;

procedure TestTTSMainForm.TestFormCloseQuery;
var
  CanClose: Boolean;
  Sender: TObject;
begin
  // TODO: Setup method call parameters
  FTSMainForm.FormCloseQuery(Sender, CanClose);
  // TODO: Validate method results
end;

procedure TestTTSMainForm.TestFormCreate;
var
  Sender: TObject;
begin
  // TODO: Setup method call parameters
  FTSMainForm.FormCreate(Sender);
  // TODO: Validate method results
end;

procedure TestTTSMainForm.TestFormShow;
var
  Sender: TObject;
begin
  // TODO: Setup method call parameters
  FTSMainForm.FormShow(Sender);
  // TODO: Validate method results
end;

procedure TestTTSMainForm.TestGNUGPLActionExecute;
var
  Sender: TObject;
begin
  // TODO: Setup method call parameters
  FTSMainForm.GNUGPLActionExecute(Sender);
  // TODO: Validate method results
end;

procedure TestTTSMainForm.TestHelpContentsActionExecute;
var
  Sender: TObject;
begin
  // TODO: Setup method call parameters
  FTSMainForm.HelpContentsActionExecute(Sender);
  // TODO: Validate method results
end;

procedure TestTTSMainForm.TestHideToolbarActionExecute;
var
  Sender: TObject;
begin
  // TODO: Setup method call parameters
  FTSMainForm.HideToolbarActionExecute(Sender);
  // TODO: Validate method results
end;

procedure TestTTSMainForm.TestLicenseInfoActionExecute;
var
  Sender: TObject;
begin
  // TODO: Setup method call parameters
  FTSMainForm.LicenseInfoActionExecute(Sender);
  // TODO: Validate method results
end;

procedure TestTTSMainForm.TestMinimizePopupMenuItemClick;
var
  Sender: TObject;
begin
  // TODO: Setup method call parameters
  FTSMainForm.MinimizePopupMenuItemClick(Sender);
  // TODO: Validate method results
end;

procedure TestTTSMainForm.TestMRUManagerClick;
var
  UserData: Integer;
  Caption: string;
  RecentName: string;
  Sender: TObject;
begin
  // TODO: Setup method call parameters
  FTSMainForm.MRUManagerClick(Sender, RecentName, Caption, UserData);
  // TODO: Validate method results
end;

procedure TestTTSMainForm.TestOpenProjectActionExecute;
var
  Sender: TObject;
begin
  // TODO: Setup method call parameters
  FTSMainForm.OpenProjectActionExecute(Sender);
  // TODO: Validate method results
end;

procedure TestTTSMainForm.TestPreferencesActionExecute;
var
  Sender: TObject;
begin
  // TODO: Setup method call parameters
  FTSMainForm.PreferencesActionExecute(Sender);
  // TODO: Validate method results
end;

procedure TestTTSMainForm.TestPrintLogActionExecute;
var
  Sender: TObject;
begin
  // TODO: Setup method call parameters
  FTSMainForm.PrintLogActionExecute(Sender);
  // TODO: Validate method results
end;

procedure TestTTSMainForm.TestPrintPreviewActionExecute;
var
  Sender: TObject;
begin
  // TODO: Setup method call parameters
  FTSMainForm.PrintPreviewActionExecute(Sender);
  // TODO: Validate method results
end;

procedure TestTTSMainForm.TestPrintSetupActionExecute;
var
  Sender: TObject;
begin
  // TODO: Setup method call parameters
  FTSMainForm.PrintSetupActionExecute(Sender);
  // TODO: Validate method results
end;

procedure TestTTSMainForm.TestRemindTimerTimer;
var
  Sender: TObject;
begin
  // TODO: Setup method call parameters
  FTSMainForm.RemindTimerTimer(Sender);
  // TODO: Validate method results
end;

procedure TestTTSMainForm.TestReducedModeActionExecute;
var
  Sender: TObject;
begin
  // TODO: Setup method call parameters
  FTSMainForm.ReducedModeActionExecute(Sender);
  // TODO: Validate method results
end;

procedure TestTTSMainForm.TestRegistrationInfoHelpMenuItemClick;
var
  Sender: TObject;
begin
  // TODO: Setup method call parameters
  FTSMainForm.RegistrationInfoHelpMenuItemClick(Sender);
  // TODO: Validate method results
end;

procedure TestTTSMainForm.TestResetRemindTimer;
begin
  FTSMainForm.ResetRemindTimer;
  // TODO: Validate method results
end;

procedure TestTTSMainForm.TestRestorePopupMenuItemClick;
var
  Sender: TObject;
begin
  // TODO: Setup method call parameters
  FTSMainForm.RestorePopupMenuItemClick(Sender);
  // TODO: Validate method results
end;

procedure TestTTSMainForm.TestSaveProjectActionExecute;
var
  Sender: TObject;
begin
  // TODO: Setup method call parameters
  FTSMainForm.SaveProjectActionExecute(Sender);
  // TODO: Validate method results
end;

procedure TestTTSMainForm.TestSetDefaultHourlyRateActionExecute;
var
  Sender: TObject;
begin
  // TODO: Setup method call parameters
  FTSMainForm.SetDefaultHourlyRateActionExecute(Sender);
  // TODO: Validate method results
end;

procedure TestTTSMainForm.TestSetDefaultReportEndNotesActionExecute;
var
  Sender: TObject;
begin
  // TODO: Setup method call parameters
  FTSMainForm.SetDefaultReportEndNotesActionExecute(Sender);
  // TODO: Validate method results
end;

procedure TestTTSMainForm.TestSetProjectTitleActionExecute;
var
  Sender: TObject;
begin
  // TODO: Setup method call parameters
  FTSMainForm.SetProjectTitleActionExecute(Sender);
  // TODO: Validate method results
end;

procedure TestTTSMainForm.TestSetThisProjectsReportEndNotesActionExecute;
var
  Sender: TObject;
begin
  // TODO: Setup method call parameters
  FTSMainForm.SetThisProjectsReportEndNotesActionExecute(Sender);
  // TODO: Validate method results
end;

procedure TestTTSMainForm.TestStartSlackActionExecute;
var
  Sender: TObject;
begin
  // TODO: Setup method call parameters
  FTSMainForm.StartSlackActionExecute(Sender);
  // TODO: Validate method results
end;

procedure TestTTSMainForm.TestStartTimerActionExecute;
var
  Sender: TObject;
begin
  // TODO: Setup method call parameters
  FTSMainForm.StartTimerActionExecute(Sender);
  // TODO: Validate method results
end;

procedure TestTTSMainForm.TestStatusTimerTimer;
var
  Sender: TObject;
begin
  // TODO: Setup method call parameters
  FTSMainForm.StatusTimerTimer(Sender);
  // TODO: Validate method results
end;

procedure TestTTSMainForm.TestVersionHistoryActionExecute;
var
  Sender: TObject;
begin
  // TODO: Setup method call parameters
  FTSMainForm.VersionHistoryActionExecute(Sender);
  // TODO: Validate method results
end;

procedure TestTTSMainForm.TestStopTimerActionExecute;
var
  Sender: TObject;
begin
  // TODO: Setup method call parameters
  FTSMainForm.StopTimerActionExecute(Sender);
  // TODO: Validate method results
end;

procedure TestTTSMainForm.TestTSStatusBarDrawPanel;
var
  Rect: TRect;
  Panel: TStatusPanel;
  StatusBar: TStatusBar;
begin
  // TODO: Setup method call parameters
  FTSMainForm.TSStatusBarDrawPanel(StatusBar, Panel, Rect);
  // TODO: Validate method results
end;

procedure TestTTSMainForm.TestRemindActionExecute;
var
  Sender: TObject;
begin
  // TODO: Setup method call parameters
  FTSMainForm.RemindActionExecute(Sender);
  // TODO: Validate method results
end;

procedure TestTTSMainForm.TestRemindTimeSpinEditChange;
var
  Sender: TObject;
begin
  // TODO: Setup method call parameters
  FTSMainForm.RemindTimeSpinEditChange(Sender);
  // TODO: Validate method results
end;

procedure TestTTSMainForm.TestContinueTimeActionExecute;
var
  Sender: TObject;
begin
  // TODO: Setup method call parameters
  FTSMainForm.ContinueTimeActionExecute(Sender);
  // TODO: Validate method results
end;

procedure TestTTSMainForm.TestGridPopupPopup;
var
  Sender: TObject;
begin
  // TODO: Setup method call parameters
  FTSMainForm.GridPopupPopup(Sender);
  // TODO: Validate method results
end;

procedure TestTTSMainForm.TestPrintPreviewToolButtonClick;
var
  Sender: TObject;
begin
  // TODO: Setup method call parameters
  FTSMainForm.PrintPreviewToolButtonClick(Sender);
  // TODO: Validate method results
end;

procedure TestTTSMainForm.TestCreateGroupActionExecute;
var
  Sender: TObject;
begin
  // TODO: Setup method call parameters
  FTSMainForm.CreateGroupActionExecute(Sender);
  // TODO: Validate method results
end;

procedure TestTTSMainForm.TestRowScrolled;
var
  Sender: TObject;
begin
  // TODO: Setup method call parameters
  FTSMainForm.RowScrolled(Sender);
  // TODO: Validate method results
end;

procedure TestTTSMainForm.TestUpdateTitle;
begin
  FTSMainForm.UpdateTitle;
  // TODO: Validate method results
end;

procedure TestTTSMainForm.TestUpdateStatusPanel;
begin
  FTSMainForm.UpdateStatusPanel;
  // TODO: Validate method results
end;

procedure TestTTSMainForm.TestUpdateGUI;
begin
  TMUtils.ClearTimeRecord( TMUtils.TimingRecord.TimeRecord );
  TMUtils.TimingRecord.TimeRecord.StartTime := now;
  TMUtils.TimingRecord.SessionStartTime     := now;

  CurrentState := csTiming;
  FTSMainForm.UpdateGUI;

  Check( CurrentState = csTiming, 'CurrentState' );
  CheckFalse(  FTSMainForm.StartTimerAction.Enabled, 'StartTimerAction' );
  CheckTrue(   FTSMainForm.StartSlackAction.Enabled, 'StartSlackAction' );
  //to fail.. CheckFalse( FTSMainForm.StartSlackAction.Enabled, 'StartSlackAction' );
end;

initialization
  // Register any test cases with the test runner
  RegisterTest(TestTTSMainForm.Suite);
end.

