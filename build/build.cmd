@echo off
@cls
@REM Builds Timestamp, output only if there is a failure
@echo 1_checkout...
call 1_checkout
if errorlevel 1 goto failed

@echo 2_make...
call 2_make
if errorlevel 1 goto failed

@echo 3_test...
call 3_test
if errorlevel 1 goto failed

@echo 4_publish...
call 4_publish
if errorlevel 1 goto failed

goto done

:failed
set errorlevel=1 
goto end

:done
set errorlevel=0
goto end

:end
if errorlevel 1 echo BUILD FAILED else echo BUILD OK
