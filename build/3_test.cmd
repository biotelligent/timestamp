@echo off
REM Run console tests
SET COPYCMD=/Y
SETLOCAL EnableExtensions
SETLOCAL EnableDelayedExpansion

set TSTFILE=c:\Temp\Build\Timestamp\bin\Timestamp_Net_Test.exe

if not exist %TSTFILE% echo %TSTFILE% failed to build
if not exist %TSTFILE% goto failed

%TSTFILE%
if errorlevel==0 goto done

:failed
set errorlevel=1
@echo TEST FAILED 
goto end

:done
set errorlevel=0
@echo TEST OK

:end
ENDLOCAL