@echo off
REM Publish to demo server / nightly build

SET COPYCMD=/Y
SETLOCAL EnableExtensions
SETLOCAL EnableDelayedExpansion

set OUTDIR=c:\Temp\Build\Timestamp
set ISCC="c:\devtools\Inno Setup 5\iscc.exe"
set UPXEXE=c:\devtools\UPX\upx.exe
set INSTALLSCRIPT=%OUTDIR%\Install\Source\TS_Net.iss
set SETUPFILE=%OUTDIR%\bin\Timestamp_Setup.exe

if not exist %INSTALLSCRIPT% echo %INSTALLSCRIPT% is missing 
if not exist %INSTALLSCRIPT% goto failed
REM if not exist %UPXEXE% echo %UPXEXE% is missing 
REM if not exist %UPXEXE% goto failed
if not exist %ISCC% echo %ISCC% is missing 
if not exist %ISCC% goto failed

:clean
if exist %SETUPFILE% del %SETUPFILE%

:ISCC
REM %UPXEXE% %OUTDIR%\bin\Timestamp_Net.exe
%ISCC% %INSTALLSCRIPT% /q /o%OUTDIR%\bin
if errorlevel = 0 goto done

if exist %SETUPFILE% goto done

:failed
set errorlevel=1
@echo PUBLISH FAILED 
goto end

:done
set errorlevel=0
@echo PUBLISH OK

:end
ENDLOCAL
