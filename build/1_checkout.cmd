@echo off
@REM Check out Timestamp
SET COPYCMD=/Y
SETLOCAL EnableExtensions
SETLOCAL EnableDelayedExpansion

set SVNDIR=c:\DevTools\SVN
set OUTDIR=c:\Temp\Build\Timestamp

if exist %OUTDIR%\. rmdir %OUTDIR% /s /q > nul

if exist %SVNDIR%\svn.exe goto checkout
@echo FAILED: svn.exe does not exist
set errorlevel=1
goto end

:checkout
%SVNDIR%\svn.exe checkout -q http://devserver.my.local/svn/public/Timestamp/trunk %OUTDIR% --username jholloway --password goeshere --no-auth-cache

:done
@echo CHECKOUT OK
set errorlevel=0
REM cd %OUTDIR%

:end
ENDLOCAL