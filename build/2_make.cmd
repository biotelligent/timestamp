@echo off
@REM Builds Timestamp, output only if there is a failure
SET COPYCMD=/Y
SETLOCAL EnableExtensions
SETLOCAL EnableDelayedExpansion

set OUTDIR=c:\Temp\Build\Timestamp
set SRCDIR=%OUTDIR%\Source
set TSTDIR=%OUTDIR%\test 
set EXEDIR=%OUTDIR%\bin
set EXEFILE=%EXEDIR%\Timestamp_Net.exe
set TSTFILE=%EXEDIR%\Timestamp_Net_Test.exe
set LOGFILE=%OUTDIR%\build.log

if not exist %EXEFILE% goto build
del %EXEFILE%
del %TSTFILE%

:build
@cd %SRCDIR%
"c:\program files\codegear\rad studio\5.0\bin\dccil.exe" -E%EXEDIR% -B -DDEBUG --description:"Time Stamp .NET" -I..\dcuil;"c:\program files\codegear\rad studio\5.0\lib\Debug";C:\WINDOWS\Microsoft.NET\Framework\v2.0.50727;"C:\Program Files\Common Files\CodeGear Shared\Rad Studio\Shared Assemblies\5.0";"c:\program files\codegear\rad studio\5.0\LIB";"c:\program files\codegear\rad studio\5.0\lib\Indy10";"c:\program files\codegear\rad studio\5.0\RaveReports\Lib";;..\dcuil -LE"C:\Documents and Settings\All Users\Documents\RAD Studio\5.0\Bpl" -LN..\dcuil -LUC:\WINDOWS\Microsoft.NET\Framework\v2.0.50727\System.Drawing.dll;"C:\Program Files\Common Files\CodeGear Shared\Rad Studio\Shared Assemblies\5.0\Borland.Delphi.dll";"C:\Program Files\Common Files\CodeGear Shared\Rad Studio\Shared Assemblies\5.0\Borland.Vcl.dll";"C:\Program Files\Common Files\CodeGear Shared\Rad Studio\Shared Assemblies\5.0\Borland.VclDbCtrls.dll";"C:\Program Files\Common Files\CodeGear Shared\Rad Studio\Shared Assemblies\5.0\Borland.VclDbRtl.dll";"C:\Program Files\Common Files\CodeGear Shared\Rad Studio\Shared Assemblies\5.0\Borland.VclDbxCds.dll" -N0..\dcuil -NH..\dcuil -NO..\dcuil -NSBorland.Vcl;Nevrona.Rave -R..\dcuil;"c:\program files\codegear\rad studio\5.0\lib\Debug";C:\WINDOWS\Microsoft.NET\Framework\v2.0.50727;"C:\Program Files\Common Files\CodeGear Shared\Rad Studio\Shared Assemblies\5.0";"c:\program files\codegear\rad studio\5.0\LIB";"c:\program files\codegear\rad studio\5.0\lib\Indy10";"c:\program files\codegear\rad studio\5.0\RaveReports\Lib"; -U..\dcuil;"c:\program files\codegear\rad studio\5.0\lib\Debug";C:\WINDOWS\Microsoft.NET\Framework\v2.0.50727;"C:\Program Files\Common Files\CodeGear Shared\Rad Studio\Shared Assemblies\5.0";"c:\program files\codegear\rad studio\5.0\LIB";"c:\program files\codegear\rad studio\5.0\lib\Indy10";"c:\program files\codegear\rad studio\5.0\RaveReports\Lib";;..\dcuil -V -VN --no-config -W-SYMBOL_PLATFORM -W-UNIT_PLATFORM   TimeStamp_NET.dpr > %LOGFILE%

@cd %TSTDIR%
"c:\program files\codegear\rad studio\5.0\bin\dccil.exe" -E%EXEDIR% -B -DDEBUG;CONSOLE_TESTRUNNER -I..\..\source;..\source;D:\Develop\Common\Source\Units;..\Common;"c:\program files\codegear\rad studio\5.0\Source\DUnit\src";C:\WINDOWS\Microsoft.NET\Framework\v2.0.50727;"C:\Program Files\Common Files\CodeGear Shared\Rad Studio\Shared Assemblies\5.0";"c:\program files\codegear\rad studio\5.0\LIB";"c:\program files\codegear\rad studio\5.0\lib\Indy10";"c:\program files\codegear\rad studio\5.0\RaveReports\Lib";;bin -LE"C:\Documents and Settings\All Users\Documents\RAD Studio\5.0\Bpl" -LNbin -LUC:\WINDOWS\Microsoft.NET\Framework\v2.0.50727\System.Data.dll;C:\WINDOWS\Microsoft.NET\Framework\v2.0.50727\System.dll;C:\WINDOWS\Microsoft.NET\Framework\v2.0.50727\System.XML.dll;"C:\Program Files\Common Files\CodeGear Shared\Rad Studio\Shared Assemblies\5.0\Borland.Delphi.dll";"C:\Program Files\Common Files\CodeGear Shared\Rad Studio\Shared Assemblies\5.0\Borland.Vcl.dll" -N0dcuil -NHdcuil -NOdcuil -NSBorland.Vcl;Nevrona.Rave -R..\..\source;..\source;D:\Develop\Common\Source\Units;..\Common;"c:\program files\codegear\rad studio\5.0\Source\DUnit\src";C:\WINDOWS\Microsoft.NET\Framework\v2.0.50727;"C:\Program Files\Common Files\CodeGear Shared\Rad Studio\Shared Assemblies\5.0";"c:\program files\codegear\rad studio\5.0\LIB";"c:\program files\codegear\rad studio\5.0\lib\Indy10";"c:\program files\codegear\rad studio\5.0\RaveReports\Lib"; -U..\..\source;..\source;D:\Develop\Common\Source\Units;..\Common;"c:\program files\codegear\rad studio\5.0\Source\DUnit\src";C:\WINDOWS\Microsoft.NET\Framework\v2.0.50727;"C:\Program Files\Common Files\CodeGear Shared\Rad Studio\Shared Assemblies\5.0";"c:\program files\codegear\rad studio\5.0\LIB";"c:\program files\codegear\rad studio\5.0\lib\Indy10";"c:\program files\codegear\rad studio\5.0\RaveReports\Lib";;bin -V -VN --platform:x86 --no-config -W-   TimeStamp_NET_Test.dpr >> %LOGFILE%

:check
if not exist %EXEFILE% goto failed
REM if %TSTFILE% did not build, fail in the test stage instead
goto done

:failed
@type %LOGFILE% 
@echo MAKE FAILED
set errorlevel=1
goto end

:done
@echo MAKE OK
set errorlevel=0

:end
cd %EXEDIR%
ENDLOCAL