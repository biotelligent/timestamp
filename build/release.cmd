@echo off
@cls
@echo Create and verify a new project release from the checked in mainline, /? for help
@echo.
if /i "%1"=="?" goto showuse
if /i "%1"=="help" goto showuse
if /i "%1"=="" goto showuse
if /i "%2"=="" goto showuse
goto start

:showuse
@echo Use:
@echo   %0 PROJECT NEW_RELEASE_VERSION "[SWITCH WORKING COPY TO NEW RELEASE Y/N]"
@echo.
@echo.  eg. %0 Timestamp 1.1 "[Y|N]"
@echo.
goto end

:start
REM Steps:
REM   1. Checkout, build and test the mainline using the RELEASE compiler directive
REM   2. Create the release branch
REM   3. Export, build, test, and publish the release branch using the RELEASE compiler directive

SET COPYCMD=/Y
SETLOCAL EnableExtensions
SETLOCAL EnableDelayedExpansion

REM Verify SVN
set SVNDIR=c:\DevTools\SVN
if not exist %SVNDIR%\svn.exe echo FAILED: svn.exe does not exist
if not exist %SVNDIR%\svn.exe goto failed

REM default SWITCHWORKINGCOPY to No
set SWITCHWORKINGCOPY=N
if /i "%3"=="y" set SWITCHWORKINGCOPY=Y

set PROJECT=%1
set VERSION=%2
set RELEASE=%PROJECT%-%VERSION%_release
set URLBASE=http://devserver.aseltd.local/svn/public
set URLTRUNK=%URLBASE%/%PROJECT%/trunk
set URLBRANCH=%URLBASE%/%PROJECT%/branches/%RELEASE%
set LOGMSG=%PROJECT% release %VERSION% by %USERNAME% on %COMPUTERNAME%

:createrelease
%SVNDIR%\svn.exe delete %URLBRANCH% --quiet --message "LOGMSG" --username jholloway --password goeshere --no-auth-cache
%SVNDIR%\svn.exe copy %URLTRUNK% %URLBRANCH% --message "%LOGMSG%" --username jholloway --password goeshere --no-auth-cache
if errorlevel 1 goto failed

:export 
@echo 1_export...
set TMPDIR=c:\Temp\Build\%PROJECT%
if exist %TMPDIR%\. rmdir %TMPDIR% /s /q > nul
%SVNDIR%\svn.exe export -q %URLBRANCH% %TMPDIR% --username jholloway --password goeshere --no-auth-cache

@echo 2_make...
call 2_make
if errorlevel 1 goto failed

@echo 3_test...
call 3_test
if errorlevel 1 goto failed

@echo 4_publish...
call 4_publish
if errorlevel 1 goto failed

goto done

:failed 
@echo RELEASE FAILED
set errorlevel=1
goto end

:done
@echo RELEASE OK
set errorlevel=0 
goto end

:end
ENDLOCAL